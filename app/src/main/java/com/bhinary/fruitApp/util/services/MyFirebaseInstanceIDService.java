package com.bhinary.fruitApp.util.services;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;

/**
 * Created by wpinango on 9/1/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        Global.refreshedToken = token;
        new UpdateTokenFB().execute(Global.URL_UPDATE_FB_TOKEN);
    }

    private class UpdateTokenFB extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(5000);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(MyFirebaseInstanceIDService.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(s,BooleanResponse.class);

                } else {
                    Global.Toaster.get().showToast(MyFirebaseInstanceIDService.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
