package com.bhinary.fruitApp.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wpinango on 8/29/17.
 */

public class SharedPreferenceConstants {

    public static final String KEY_SESSION_TIME = "sesionTime";
    public static final String KEY_NOTIFICATION = "notification1";
    public static final String KEY_NOTIFICATION_SOUND = "sound";
    public static final String KEY_GENERAL_NOTIFICATION = "notification";
    public static final String KEY_SHOW_RAFFE = "roulette";
    public static final String KEY_ROULETTE_SOUND = "rouletteSound";
    public static final String MUTE_ROULETTE = "muteSound";
    public static final String ID_SAVED = "user_id";
    public static final String KEY_TIME = "timeSynchro";
    private static final String KEY_DATA = "data";

    public static void saveTimestamp(Context context, long value) {
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong("timestamp", value);
        edit.apply();
    }

    public static long getSavedTimestamp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_TIME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong("timestamp", 0);
    }

    public static void saveData(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("data", value);
        edit.apply();
    }

    public static String getSavedData(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString("data", "");
    }

    public static void saveGeneralData(Context context, String data, String TAG) {
        SharedPreferences preferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("data", data);
        edit.apply();
    }

    public static String getGeneralData(Context context, String TAG) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPreferences.getString("data", "");
    }

}
