package com.bhinary.fruitApp.util.chart;

import android.graphics.Color;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.bhinary.fruitApp.common.CustomFormatter;
import com.bhinary.fruitApp.common.MyXAxisValueFormatter;
import com.bhinary.fruitApp.models.Feature;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/10/17.
 */

public class Chart {

    public static void paintChart(ArrayList<Feature> comments, ArrayList<BarEntry> arrayList, HorizontalBarChart chart ) {
        BarDataSet set1;
        set1 = new BarDataSet(setData(comments), "The year 2017");
        set1.setValueFormatter(new CustomFormatter());
        set1.setValueTextSize(9f);
        set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
        set1.setColors(Color.parseColor("#7cb5ec"), Color.parseColor("#7cb5ec"), Color.parseColor("#7cb5ec"), Color.parseColor("#7cb5ec"), Color.parseColor("#7cb5ec"));
        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);
        BarData data = new BarData(dataSets);
        YAxis left = chart.getAxisLeft();
        left.setDrawLabels(false);
        String[] values = new String[] { "1 star", comments.get(0).getName(), comments.get(1).getName(),
                comments.get(2).getName(), comments.get(3).getName(), comments.get(4).getName(), "2", "3"};
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextSize(9f);
        xAxis.setAxisMinimum(1f);
        xAxis.setAxisMaximum(6f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setLabelCount(5);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setAxisMaximum(5f);
        chart.getAxisRight().setAxisMinimum(0f);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisRight().setDrawZeroLine(true);
        chart.setData(data);
        Description description = new Description();
        description.setText("");
        chart.setDescription(description);
        chart.getLegend().setEnabled(false);
        chart.animateY(1000);
        chart.invalidate();
    }

    public static ArrayList<BarEntry> getDataSet() {
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e2 = new BarEntry(1.5f, 1.5f); //Responsabilidad
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(2.5f, 2.0f); // Eficiencia
        valueSet1.add(v1e3);
        BarEntry v1e4 = new BarEntry(3.5f, 4.5f); // Honestidad
        valueSet1.add(v1e4);
        BarEntry v1e5 = new BarEntry(4.5f, 2.5f); // Comunicacion
        valueSet1.add(v1e5);
        BarEntry v1e6 = new BarEntry(5.5f, 4.5f); // Asesoria
        valueSet1.add(v1e6);
        return valueSet1;
    }

    public static ArrayList<BarEntry> setData(ArrayList<Feature> comments) {
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e2 = new BarEntry(1.5f, comments.get(0).getScore()); //Responsabilidad
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(2.5f, comments.get(1).getScore()); // Eficiencia
        valueSet1.add(v1e3);
        BarEntry v1e4 = new BarEntry(3.5f, comments.get(2).getScore()); // Honestidad
        valueSet1.add(v1e4);
        BarEntry v1e5 = new BarEntry(4.5f, comments.get(3).getScore()); // Comunicacion
        valueSet1.add(v1e5);
        BarEntry v1e6 = new BarEntry(5.5f, comments.get(4).getScore()); // Asesoria
        valueSet1.add(v1e6);
        return valueSet1;
    }
}
