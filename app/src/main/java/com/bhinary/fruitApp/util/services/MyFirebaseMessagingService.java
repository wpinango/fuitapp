package com.bhinary.fruitApp.util.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.common.LoginState;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.fragments.NotificationsFragment;
import com.bhinary.fruitApp.fragments.PaymentFragment;
import com.bhinary.fruitApp.fragments.RechargeFragment;
import com.bhinary.fruitApp.fragments.ResultFragment;
import com.bhinary.fruitApp.models.FragmentPosition;
import com.bhinary.fruitApp.models.MessageType;
import com.bhinary.fruitApp.models.Notification;
import com.bhinary.fruitApp.models.Settings;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.util.notification.NotificationsConfig;
import com.bhinary.fruitApp.util.SharedPreferenceConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wpinango on 8/31/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static String TAG = "MyFirebaseMsgService";
    private NotificationsConfig notificationsConfig = new NotificationsConfig();
    private NotificationID notificationID = new NotificationID();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Log.d(TAG, "From: " + remoteMessage.getFrom());

            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());

                SharedPreferences sharedPreferences = getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
                String value = sharedPreferences.getString("configNotification", "");

                if (value.equals("")) {
                    setSettings();
                } else {
                    getSettings();
                }

                for (Settings settings : Global.settings) {
                    switch (settings.getTAG()) {
                        case SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION:
                            if (settings.isStatus() && !Global.isNotificationShow && LoginState.isLogin(this)) {
                                sendNotification(remoteMessage);
                            }
                            break;
                        case SharedPreferenceConstants.KEY_NOTIFICATION_SOUND:
                            if (settings.isStatus() && LoginState.isLogin(this)) {
                                makeANotificationSound();
                            }
                            break;
                    }
                }

                saveIncomingNotification(remoteMessage.getData());
                if (Global.isNotificationShow && LoginState.isLogin(this)) {
                    sendNotificationToFragmentsView(new Gson().toJson(remoteMessage.getData()));
                } else if (!Global.isNotificationShow && LoginState.isLogin(this)) {
                    checkNotificationMessageType(remoteMessage.getData());
                }
            }

            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }

    private void saveIncomingNotification(Map<String,String >data ) {
        try {
            Notification notification = new Notification();
            if (NotificationsFragment.getSavedNotifications(this) != "") {
                ArrayList<Notification> a = new ArrayList<>(Arrays.asList(new Gson().fromJson(NotificationsFragment.getSavedNotifications(this), Notification[].class)));
                if (data.containsKey("title") && !data.get("title").equals("")) {
                    notification.setTitle(data.get("title"));
                    notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                    a.add(0, notification);
                }
                NotificationsFragment.saveNotifications(this, new Gson().toJson(a));
            } else {
                ArrayList<Notification> notifications = new ArrayList<>();
                if (data.containsKey("title") && !data.get("title").equals("")) {
                    notification.setTitle(data.get("title"));
                    notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                    notifications.add(notification);
                }
                NotificationsFragment.saveNotifications(this, new Gson().toJson(notifications));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void checkNotificationMessageType(Map<String, String> data) {
        try {
            if (data.containsKey(MessageType.KEY_MESSAGE)) {
                String dataContains = data.get(MessageType.KEY_MESSAGE);
                if (dataContains.equals(MessageType.KEY_PAYMENT)) {
                    PaymentFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RECHARGE)) {
                    RechargeFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RESULT)) {
                    ResultFragment.refresh = "refresh";
                }
                updateFragmentViewFromNotification(data.get(MessageType.KEY_MESSAGE));
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    protected void sendNotificationToFragmentsView(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateFragmentViewFromNotification(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY2",value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    /*private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().containsKey("title") && !remoteMessage.getData().get("title").equals("")) {
            Intent intent = new Intent(this, MainActivity.class);
            if (remoteMessage.getData().containsKey(MessageType.KEY_MESSAGE)){
                String value = remoteMessage.getData().get(MessageType.KEY_MESSAGE);
                if (value.equals(MessageType.KEY_PAYMENT)){
                    intent.putExtra(MessageType.KEY_MESSAGE, FragmentPosition.paymentFragment);
                } else if (value.equals(MessageType.KEY_RECHARGE)){
                    intent.putExtra(MessageType.KEY_MESSAGE,FragmentPosition.rechargeFragment);
                } else if (value.equals(MessageType.KEY_SYSTEM_MESSAGE)){
                    intent.putExtra(MessageType.KEY_SYSTEM_MESSAGE,FragmentPosition.notificationFragment);
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(remoteMessage.getData().get("title"))
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(notificationID.getID(), notificationBuilder.build());
        }
    }

    private void makeANotificationSound() {
        try {
            Vibrator vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            vb.vibrate(600);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSettings() {
        ArrayList<Settings> settings = new ArrayList<>();
        Global.settings.clear();
        Settings[] s = new Gson().fromJson(notificationsConfig.getNotification(this), Settings[].class);
        for (Settings a : s) {
            settings.add(a);
        }
        Global.settings.addAll(settings);
        return new Gson().toJson(settings);
    }

    private void setSettings() {
        ArrayList<Settings> settings = new ArrayList<>();
        Global.settings.clear();
        Settings sound = new Settings();
        sound.setTitle(Global.NAME_NOTIFICATION_SOUND);
        sound.setTAG(SharedPreferenceConstants.KEY_NOTIFICATION_SOUND);
        sound.setStatus(true);
        Settings notification = new Settings();
        notification.setStatus(true);
        notification.setTAG(SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION);
        notification.setTitle(Global.NAME_NOTIFICATION);
        settings.add(notification);
        settings.add(sound);
        notificationsConfig.setNotificationConfig(this, new Gson().toJson(settings));
        Global.settings.addAll(settings);
    }

    private class NotificationID {
        private final AtomicInteger c = new AtomicInteger(0);
        private int getID() {
            return c.incrementAndGet();
        }
    }
}

