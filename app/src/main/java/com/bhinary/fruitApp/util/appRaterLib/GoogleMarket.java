package com.bhinary.fruitApp.util.appRaterLib;

/**
 * Created by JoseAntonio on 31/5/2017.
 */

import android.content.Context;

import android.net.Uri;



public class GoogleMarket extends Market {

    private static String marketLink = "market://details?id=";



    @Override

    public Uri getMarketURI(Context context) {

        return Uri.parse(marketLink + Market.getPackageName(context));

    }

}