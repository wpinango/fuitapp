package com.bhinary.fruitApp.util.notification;

import android.content.Context;
import android.content.SharedPreferences;

import com.bhinary.fruitApp.util.SharedPreferenceConstants;

/**
 * Created by wpinango on 9/5/17.
 */

public class NotificationsConfig {

    public void setNotificationConfig(Context context,String value){
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("configNotification", value);
        edit.apply();
    }

    public String getNotification(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
        return sharedPreferences.getString("configNotification","");
    }

}
