package com.bhinary.fruitApp.util.timeout;

import android.content.Context;
import android.content.SharedPreferences;

import com.bhinary.fruitApp.util.SharedPreferenceConstants;

/**
 * Created by wpinango on 8/28/17.
 */

public class TimerMethod {

    public static final long time = 300;
    public static final int timeToRaffe = 10;

    public static long getLastActivityTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_SESSION_TIME, Context.MODE_PRIVATE);
        return sharedPreferences.getLong("time",0);
    }

    public static void setLastActivityTime(Context context, long timestamp) {
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_SESSION_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong("time", timestamp);
        edit.apply();
    }

    public int checkTimeForNextRaffle(int raffle, int currentTime) {
        System.out.println("valores 2 " + raffle + " " + currentTime);
        if ((raffle - currentTime) <= timeToRaffe ) {
            return 2;
        } else if ((raffle - currentTime) > timeToRaffe ){
            return 0;
        }
        return 0;
    }


}
