package com.bhinary.fruitApp;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.widget.Toast;

import com.bhinary.fruitApp.models.Chip;
import com.bhinary.fruitApp.models.Details;
import com.bhinary.fruitApp.models.Feature;
import com.bhinary.fruitApp.models.Game;
import com.bhinary.fruitApp.models.RaffleLabel;
import com.bhinary.fruitApp.models.Settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class Global {
    public static final String KEY_TOKEN = "x-session-token";
    public static final String KEY_ID_USER = "x-identity";
    public static final String KEY_AGENCY_ID = "x-agency-id";
    public static final String KEY_SECURITY = "x-security";
    public static final String KEY_PRODUCER_ID = "x-pid";
    public static final String KEY_AUTHORIZATION = "authorization";
    public static final String KEY_SCHEMA = "x-schema";
    public static final String NEW_KEY = "x-new-key";
    public static final String KEY_MOVIL = "movilx";
    public static final String KEY_TOKEN_FB = "x-msg-token";
    public static final String KEY_TIMESTAMP = "x-ts-sync";
    public static final String KEY_HASH_BALANCE = "x-hash-balance";
    public static final String KEY_HASH_RESULT = "x-hash-result";
    public static final String TAG = "chatbubbles";
    public static final String KEY_DENIED_RESPONSE = "Problema de autorizacion";
    public static final String NAME_NOTIFICATION_SOUND = "Sonido de notificacion";
    public static final String NAME_NOTIFICATION = "Notificaciones";
    public static final String NAME_RAFFLE = "Mostrar sorteo en vivo";
    public static ArrayList<String> name = new ArrayList<>();
    public static ArrayList<String> phone = new ArrayList<>();
    public static ArrayList<String> id = new ArrayList<>();
    public static ArrayList<String> email = new ArrayList<>();
    public static ArrayList<String> alias = new ArrayList<>();
    public static ArrayList<RaffleLabel> rafflesByGames = new ArrayList<>();
    public static String token = "";
    public static String userId;
    public static String agencyId;
    public static String userName;
    public static String cash;
    public static String userLogin;
    public static String footerTicket;
    public static String password;
    public static long timestamp = -1 ;
    public static int amount;
    public static int locked;
    public static int deferred;
    public static String HOST;
    public static String URL_ONE_TICKET;
    public static String URL_ALL_TICKET;
    public static String ENDPOINT_SELL;
    public static String URL_LOGIN_MOBILE;
    public static String URL_SYNCRO_MOBILE;
    public static String URL_GET_RESULT;
    public static String URL_DISCARD_TICKET;
    public static String URL_REQUEST_CASH;
    public static String URL_REQUEST_BALANCE;
    public static String URL_GET_PRODUCER;
    public static String URL_GET_REGISTER_NEW_USERS;
    public static String URL_CLOSE_TOKEN;
    public static String URL_GET_PENDING_RECHARGE;
    public static String URL_GET_PAYMENT;
    public static String URL_GET_PENDING_PAYMENT;
    public static String URL_DELETE_PENDING_TRANSACTION;
    public static String URL_GET_PRODUCER_RATING;
    public static String URL_FIND_USER;
    public static String URL_SEND_TRANSFER;
    public static String URL_RATE_PRODUCER;
    public static String URL_GET_PRODUCER_FEATURES;
    public static String URL_CHANGE_PRODUCER;
    public static String URL_GET_BANKS;
    public static String URL_REGISTER_ACCOUNT;
    public static String URL_GET_PLAYER_ACCOUNTS;
    public static String URL_UPDATE_PLAYER_ACCOUNTS;
    public static String URL_DELETE_PLAYER_ACCOUNTS;
    public static String URL_GET_USER_PROFILE;
    public static String URL_UPDATE_USER_PROFILE;
    public static String URL_UPDATE_PASSWORD;
    public static String URL_REQUEST_PASSWORD;
    public static String URL_UPDATE_FB_TOKEN;
    public static String URL_GET_TUTORIAL;
    public static String refreshedToken;
    public static boolean isLogin = false;
    public static int selectedPosition;
    public static HashMap<Integer, Boolean> mycheked = new HashMap<>();
    public static Details details;
    public static LinkedHashMap<String, Game> currentGames;
    public static final String KEY_FA = "ext-fa";
    public static final String KEY_RA = "ext-ra";
    public static final String KEY_LA = "ext-la";
    public static String producerName;
    public static int producerId;
    public static ArrayList<Feature> features = new ArrayList<>();
    public static ArrayList<Settings> settings = new ArrayList<>();
    public static float producerRating;
    public static long lastActivityTime = 0;
    public static boolean isNotificationShow = false;
    public static final String KEY_PINEAPPLE = "Piña";
    public static boolean activityVisible;
    public static final String KEY_VALUE_ACCOUNT = "-";
    public static final int httpRequestTimeout = 20000;
    public static ArrayList<Chip> chipArrayList = new ArrayList<>();



    public static void selectURL() {
        //HOST = "http://172.16.29.134:8090";
        //HOST = "http://172.16.10.175:8090";
        HOST = "http://movil.lotosuite.com:8095";
        URL_ONE_TICKET = HOST + "/mobile/getOneTicket";
        URL_ALL_TICKET = HOST + "/mobile/getAllTickets";
        ENDPOINT_SELL = HOST + "/mobile/sell";
        URL_LOGIN_MOBILE = HOST + "/mobile/doAnonymousLogin";
        URL_SYNCRO_MOBILE = HOST + "/mobile/synchronization";
        URL_GET_RESULT = HOST + "/mobile/resultsMobile";
        URL_DISCARD_TICKET = HOST + "/mobile/discardItemsGroup";
        URL_REQUEST_CASH = HOST + "/mobile/requestCash";
        URL_REQUEST_BALANCE = HOST + "/mobile/getBalanceHistory";
        URL_GET_PRODUCER = HOST + "/mobile/getProducerList";
        URL_GET_REGISTER_NEW_USERS = HOST + "/mobile/registerNewUser";
        URL_CLOSE_TOKEN = HOST + "/mobile/destroySession";
        URL_GET_PENDING_RECHARGE = HOST + "/mobile/getPendingRecharges";
        URL_GET_PAYMENT = HOST + "/mobile/requestPayment";
        URL_GET_PENDING_PAYMENT = HOST + "/mobile/getPaymentPending";
        URL_DELETE_PENDING_TRANSACTION = HOST + "/mobile/deletePendingTransaction";
        URL_GET_PRODUCER_RATING = HOST + "/mobile/getProducerRating";
        URL_FIND_USER = HOST + "/mobile/findUser";
        URL_SEND_TRANSFER = HOST + "/mobile/transfer";
        URL_RATE_PRODUCER = HOST + "/mobile/rateProducer";
        URL_GET_PRODUCER_FEATURES = HOST + "/mobile/getProducerFeatures";
        URL_CHANGE_PRODUCER = HOST + "/mobile/changeProducer";
        URL_GET_BANKS = HOST + "/mobile/getBanks";
        URL_REGISTER_ACCOUNT = HOST + "/mobile/registerAccountNumber";
        URL_GET_PLAYER_ACCOUNTS = HOST + "/mobile/getPlayerAccounts";
        URL_UPDATE_PLAYER_ACCOUNTS = HOST + "/mobile/updatePlayerAccounts";
        URL_DELETE_PLAYER_ACCOUNTS = HOST + "/mobile/deletePlayerAccounts";
        URL_GET_USER_PROFILE = HOST + "/mobile/getUserInformation";
        URL_UPDATE_USER_PROFILE = HOST + "/mobile/updateUserProfile";
        URL_UPDATE_PASSWORD = HOST + "/mobile/updatePassword";
        URL_REQUEST_PASSWORD = HOST + "/mobile/requestAccountUnlock";
        URL_UPDATE_FB_TOKEN = HOST + "/mobile/updateFBaseToken";
        URL_GET_TUTORIAL = HOST + "/mobile/getVideos";
    }

    public static int[] lottGame = {R.drawable.lotto, R.drawable.tufrutactiva, R.drawable.ruleta,};

    public enum Toaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> Toast.makeText(context, message, length).show()
                );
            }
        }

        public static Toaster get() {
            return INSTANCE;
        }
    }

    public enum CenterToaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> {
                            Toast toast = Toast.makeText(context, message, length);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                );
            }
        }

        public static CenterToaster get() {
            return INSTANCE;
        }
    }

    public static boolean playsIsSelected() {
        for (Map.Entry<Integer, Boolean> a : mycheked.entrySet()) {
            if (a.getValue().equals(true)) {
                return true;
            }
        }
        return false;
    }

    public static void setBalanceResponse(int cash, int deferred, int locked) {
        Global.deferred = deferred;
        Global.locked = locked;
        Global.cash = String.valueOf(cash);
    }
}