package com.bhinary.fruitApp.models;

import com.bhinary.fruitApp.common.Time;

import java.util.ArrayList;

public class SellItem {

    public String game;
    public int raffle;
    public String chip;
    public int amount;
    public int gameId;
    public int raffleId;
    public int chipId;
    public int effectiveSell;
    public String codeName;
    public String description;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getRaffleId() {
        return raffleId;
    }

    public void setRaffleId(int raffleId) {
        this.raffleId = raffleId;
    }

    public int getChipId() {
        return chipId;
    }

    public void setChipId(int chipId) {
        this.chipId = chipId;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public int getRaffle() {
        return raffle;
    }

    public void setRaffle(int raffle) {
        this.raffle = raffle;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public int getEffectiveSell() {
        return effectiveSell;
    }

    public void setEffectiveSell(int effectiveSell) {
        this.effectiveSell = effectiveSell;
    }


    @Override
    public String toString() {
        return String.format("gameId: %d,raffleId : %d, amount: %d, chipId: %d", gameId,raffleId,amount,chipId);
    }

    public static int getCountSellItem(ArrayList<SellItem> sellItems, boolean status) {
        SellItem sellItem;
        ArrayList<Integer> okValue = new ArrayList<>();
        ArrayList<Integer> noValue = new ArrayList<>();
        for (int i = 0; i < sellItems.size(); i++) {
            sellItem = sellItems.get(i);
            if (sellItem.effectiveSell == sellItem.amount) {
                okValue.add(i);
            }else if (sellItem.raffle <= Time.getCurrentTime() || sellItem.effectiveSell == 0) {
                noValue.add(i);
            }
        }
        if (status) {
            return okValue.size();
        } else {
            return noValue.size();
        }
    }
}
