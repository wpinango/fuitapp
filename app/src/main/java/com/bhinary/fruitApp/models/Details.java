package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/9/17.
 */

public class Details {
    private ArrayList<SimpleDetail> items;
    private int totalAmount;
    private String ticketDate;
    private int status;
    private int award;
    private String ticketTime;

    public int getAward() {
        return award;
    }

    public void setAward(int award) {
        this.award = award;
    }

    public ArrayList<SimpleDetail> getItems() {
        return items;
    }

    public void setItems(ArrayList<SimpleDetail> items) {
        this.items = items;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTicketDate() {
        return ticketDate;
    }

    public void setTicketDate(String ticketDate) {
        this.ticketDate = ticketDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTicketTime() {
        return ticketTime;
    }

    public void setTicketTime(String ticketTime) {
        this.ticketTime = ticketTime;
    }

}
