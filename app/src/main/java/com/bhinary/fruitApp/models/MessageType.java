package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 10/3/17.
 */

public class MessageType {

    public static String KEY_MESSAGE = "messageType";
    public static String KEY_RECHARGE = "recharge";
    public static String KEY_PAYMENT = "payment";
    public static String KEY_RESULT = "result";
    public static String KEY_WINNER = "winnner";
    public static String KEY_3P = "3p";
    public static String KEY_SYSTEM_MESSAGE = "systemMessage";
}
