package com.bhinary.fruitApp.models;

import android.support.annotation.NonNull;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class Raffle implements Comparable<Raffle> {
    private int id;
    private boolean selected;
    private int hour;
    private boolean active;
    private int gameId;
    private String codeName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Raffle(int id) {
        this.id = id;
    }
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    @Override
    public int compareTo(@NonNull Raffle o) {
        return this.hour > o.hour ? 1 : -1;
    }


}

