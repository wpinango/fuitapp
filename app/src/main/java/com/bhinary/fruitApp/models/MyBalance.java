package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/27/17.
 */

public class MyBalance {
    private int amount;
    private int locked;
    private int deferred;
    private ArrayList<Balance> balance;
    private String hashBalance;

    public String getHashBalance() {
        return hashBalance;
    }

    public void setHashBalance(String hashBalance) {
        this.hashBalance = hashBalance;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public int getDeferred() {
        return deferred;
    }

    public void setDeferred(int deferred) {
        this.deferred = deferred;
    }

    public ArrayList<Balance> getBalances() {
        return balance;
    }

    public void setBalances(ArrayList<Balance> balances) {
        this.balance = balances;
    }


}
