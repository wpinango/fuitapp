package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 7/18/17.
 */

public class TransactionType {
    public static int TRANSFER_RECHARGE = 1;
    public static int TRANSFER_WITHDRAWAL = 2;
    public static int CASH_WITHDRAWAL = 3;
    public static int CASH_RECHARGE = 4;
}
