package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 10/3/17.
 */

public class FragmentPosition {

    public static int primaryFragment = 0;
    public static int ticketFragment = 1;
    public static int resultFragment = 2;
    public static int balanceFragment = 3;
    public static int rechargeFragment = 4;
    public static int paymentFragment = 5;
    public static int notificationFragment = 6;
}
