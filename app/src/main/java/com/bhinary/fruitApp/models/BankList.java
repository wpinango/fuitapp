package com.bhinary.fruitApp.models;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/11/17.
 */

public class BankList {

    public static int getBankImages(Context context,String bankCode){
        switch (bankCode) {
            case "0171":
                return context.getResources().getIdentifier("activo","drawable",context.getPackageName());
            case "0175":
                return context.getResources().getIdentifier("bicentenario","drawable",context.getPackageName());
            case "0128":
                return context.getResources().getIdentifier("caroni","drawable",context.getPackageName());
            case "0102":
                return context.getResources().getIdentifier("devenezuela","drawable",context.getPackageName());
            case "0114":
                return context.getResources().getIdentifier("bancaribe","drawable",context.getPackageName());
            case "0163":
                return context.getResources().getIdentifier("deltesoro","drawable",context.getPackageName());
            case "0115":
                return context.getResources().getIdentifier("exterior","drawable",context.getPackageName());
            case "0105":
                return context.getResources().getIdentifier("mercantil","drawable",context.getPackageName());
            case "0191":
                return context.getResources().getIdentifier("bnc","drawable",context.getPackageName());
            case "0108":
                return context.getResources().getIdentifier("provincial","drawable",context.getPackageName());
            case "0134":
                return context.getResources().getIdentifier("banesco","drawable",context.getPackageName());
            case "0174":
                return context.getResources().getIdentifier("banplus","drawable",context.getPackageName());
            case "0116":
                return context.getResources().getIdentifier("bod","drawable",context.getPackageName());
            case "0157":
                return context.getResources().getIdentifier("delsur","drawable",context.getPackageName());
            case "0151":
                return context.getResources().getIdentifier("bfc","drawable",context.getPackageName());
            case "0156":
                return context.getResources().getIdentifier("cienporciento","drawable",context.getPackageName());
            case "0172":
                return context.getResources().getIdentifier("bancamiga","drawable",context.getPackageName());
            case "0168":
                return context.getResources().getIdentifier("bancrecer","drawable",context.getPackageName());
            case "0177":
                return context.getResources().getIdentifier("banfanb","drawable",context.getPackageName());
            case "0146":
                return context.getResources().getIdentifier("bangente","drawable",context.getPackageName());
            case "0190":
                return context.getResources().getIdentifier("citibank","drawable",context.getPackageName());
            case "0169":
                return context.getResources().getIdentifier("mibanco","drawable",context.getPackageName());
            case "0138":
                return context.getResources().getIdentifier("plaza","drawable",context.getPackageName());
            case "0137":
                return context.getResources().getIdentifier("sofitasa","drawable",context.getPackageName());
        }
        return 0;
    }

    public static boolean isBankExist(Context context,String code){
        int bankImage = getBankImages(context,code);
        if (bankImage != 0){
            return true;
        }
        return false;
    }

    /*public static String getBankName(String bankCode) {
        switch (bankCode) {
            case "0171":
                return "Banco Activo";
            case "0175":
                return "Banco Bicentenario";
            case "0128":
                return "Banco Caroni";
            case "0102":
                return "Banco de Venezuela";
            case "0114":
                return "Banco del Caribe";
            case "0163":
                return "Banco del Tesoro";
            case "0115":
                return "Banco Exterior";
            case "0105":
                return "Banco Mercantil";
            case "0191":
                return "Banco Nacional de Credito";
            case "0108":
                return "Banco Provincial";
            case "0134":
                return "Banesco";
            case "0174":
                return "Banplus Banco Comercial";
            case "0116":
                return "Banco Occidental de Descuento";
            case "0157":
                return "Banco Del Sur";
            case "0151":
                return "BFC";
            case "0172":
                return "Bancamiga";
            case "0168":
                return "Bancrecer";
            case "0177":
                return "Banfanb";
            case "0146":
                return "Bangente";
            case "0190":
                return "Citi Bank";
            case "0169":
                return "Mi Banco";
            case "0138":
                return "Banco Plaza";
            case "0137":
                return "Sofitasa";
            case "0156":
                return "100% Banco";
        }
        return "";
    }*/

    public static String findBankName(ArrayList<Bank> banks, String code) {
        for (Bank bank: banks) {
            if (code.trim().equals(bank.getIcon().trim())){
                return bank.getName();
            }
        }
        return "";
    }

    public static int findBankId(ArrayList<Bank>banks,String code){
        for (Bank bank:banks) {
            if (code.trim().equals(bank.getIcon().trim())){
                return bank.getId();
            }
        }
        return 0;
    }
}
