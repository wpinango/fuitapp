package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/15/17.
 */

public class RechargePending extends RechargeItemPending {
    private ArrayList<Bank> banks;

    public void setBanks(ArrayList<Bank>banks) {
        this.banks = banks;
    }

    public ArrayList<Bank> getBanks(){
        return banks;
    }
}
