package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 6/30/17.
 */

public class BooleanResponse {
    public boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


}
