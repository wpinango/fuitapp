package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/23/17.
 */

public class PaymentPending extends RechargeItemPending {
    private ArrayList<AccountNumber> accounts;

    public ArrayList<AccountNumber> getAccountNumbers() {
        return accounts;
    }

    public void setAccountNumbers(ArrayList<AccountNumber> accountNumbers) {
        this.accounts = accountNumbers;
    }
}
