package com.bhinary.fruitApp.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class LoginMessage {
    public String footer;
    public String agencyname;
    public String username;
    public String userlogin;
    public LinkedHashMap<String, Game> games;
    public String producerName;
    public int producerId;
    public ArrayList<Feature> producerFeatures = new ArrayList<>();
    public float producerRating;
    public boolean synchronization;
}

