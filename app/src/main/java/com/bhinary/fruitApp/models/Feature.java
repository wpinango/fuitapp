package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 8/7/17.
 */

public class Feature {
    private int featureId;
    private String name;
    private float score;

    public int getFeatureId() {
        return featureId;
    }

    public void setFeatureId(int featureId) {
        this.featureId = featureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }


}
