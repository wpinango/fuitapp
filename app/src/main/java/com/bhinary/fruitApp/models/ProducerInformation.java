package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/7/17.
 */

public class ProducerInformation {
    private int producerId;
    private String producerName;
    private float producerRating;
    private String ced;
    private String email;
    private String location;
    private ArrayList<Bank> banks;
    private ArrayList<Feature> features;
    private ArrayList<Comment> comments;
    private int totalUsers;

    public void setProducerRating(float producerRating) {
        this.producerRating = producerRating;
    }

    public float getProducerRating() {
        return producerRating;
    }

    public int getProducerId() {
        return producerId;
    }

    public void setProducerId(int producerId) {
        this.producerId = producerId;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    public String getCed() {
        return ced;
    }

    public void setCed(String ced) {
        this.ced = ced;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<Bank> banks) {
        this.banks = banks;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }
}
