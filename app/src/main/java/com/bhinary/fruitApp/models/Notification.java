package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 9/4/17.
 */

public class Notification {
    private String title;
    private String body;
    private String hour;
    private ResultNotification result;
    private boolean selected;

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public boolean isSelected(){
        return selected;
    }

    public void setResult(ResultNotification result){
        this.result = result;
    }

    public ResultNotification getResult(){
        return result;
    }

    public void setHour(String hour){
        this.hour = hour;
    }

    public String getHour(){
        return hour;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
