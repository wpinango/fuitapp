package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/12/17.
 */

public class RechargeItemPending {
    public boolean status;
    public ArrayList<ItemPending> pending = new ArrayList<>();

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ArrayList<ItemPending> getPending() {
        return pending;
    }

    public void setPending(ArrayList<ItemPending> pending) {
        this.pending = pending;
    }

    public boolean isItemSelected(ArrayList<ItemPending> pendings) {
        for (ItemPending itemPending: pendings) {
            if(itemPending.isSelected()){
                return true;
            }
        }
        return false;
    }

    public ItemPending getSelectedItem(ArrayList<ItemPending> pending) {
        for (ItemPending itemPending : pending) {
            if (itemPending.isSelected()){
                return itemPending;
            }
        }
        return null;
    }

    public int getItemIdSelected(ArrayList<ItemPending> pending) {
        for (int i = 0; i < pending.size(); i++) {
            if (pending.get(i).isSelected()) {
                return i;
            }
        }
        return 0;
    }
}
