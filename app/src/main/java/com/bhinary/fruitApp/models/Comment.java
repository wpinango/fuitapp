package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 8/7/17.
 */

public class Comment {
    private int playerId;
    private String nickName;
    private float rating;
    private String comment;
    private String date;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return nickName;
    }

    public void setPlayerName(String playerName) {
        this.nickName = playerName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
