package com.bhinary.fruitApp.models;

import android.support.annotation.NonNull;

import com.bhinary.fruitApp.Global;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by jcabrera on 05/06/17.
 */

public class Chip implements Comparable<Chip>{
    public String chipLabel;
    public int id;
    public String description;
    public String quota;
    public int gameId;
    public String codeName;
    public boolean selected;
    public boolean active = false;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int compareTo(@NonNull Chip chip) {
        return Integer.valueOf(this.chipLabel).compareTo(Integer.valueOf(chip.chipLabel));
    }

    public static boolean isChipSelected() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    return true;
                }
            }
        }
        return false;
    }

    public static void clearChipSelection() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                chip.setSelected(false);
                //Global.chipSelect.put(Integer.valueOf(chipLabel.chipLabel),false);
            }
        }
    }

    public static String getChipSelected() {
        Game game;
        for (Map.Entry<String, Game> a : Global.currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    return chip.chipLabel;
                }
            }
        }
        return null;
    }

    public static Chip getChipByCurrentsGame (LinkedHashMap<String,Game> games, int position) {
        Game game;
        for (Map.Entry<String, Game> a :games.entrySet()) {
            game = a.getValue();
            switch (game.getCodeName()) {
                case Global.KEY_FA:
                    return game.getChips().get(String.valueOf(position));
                case Global.KEY_LA:
                    return game.getChips().get(String.valueOf(position));
                case Global.KEY_RA:
                    return game.getChips().get(String.valueOf(position));
            }
        }
        return null;
    }

    public static int getChipByGameMaxPosition(LinkedHashMap<String, Game> currentGames) {
        int temp = 0;
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            //temp++;
            if (a.getValue().getChips().size() > temp) {
                temp = a.getValue().getChips().size();
            }
            //Math.max(a.getValue().getChips().size(), temp);
        }
        return temp;
    }

    public static int getNumberChipsSelected (LinkedHashMap<String, Game> currentGames) {
        Game game;
        int count = 0;
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            game = a.getValue();
            for (Chip chip : game.getChips().values()){
                if (chip.isSelected()){
                    count++;
                }
            }
        }
        return count;
    }

    public static int getChipGrid(LinkedHashMap<String, Game> currentGames){
        for (Map.Entry<String, Game> a : currentGames.entrySet()) {
            return a.getValue().getChips().size();
        }
        return 0;
    }
}
