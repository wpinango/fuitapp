package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 8/4/17.
 */

public class Bank {
    private String name;
    private String icon;
    private String account; //to producer banks
    private int accountId;
    private int id; // to gamer banks

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAccountId(){
        return accountId;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setIcon (String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }


}
