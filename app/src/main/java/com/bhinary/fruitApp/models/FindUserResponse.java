package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 8/14/17.
 */

public class FindUserResponse extends BooleanResponse {
    private String name;
    private int targetId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

}
