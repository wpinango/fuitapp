package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/18/17.
 */

public class AccountNumber {
    private int bankId;
    private String accountNumber;
    private String type;
    private int userId;
    private int accountId;
    private boolean status;
    private boolean selected;
    private String bank;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setStatus(boolean status){
        this.status = status;
    }

    public boolean getStatus(){
        return status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static boolean isAccountNumberSelected(ArrayList<AccountNumber> accountNumbers){
        for (AccountNumber a:accountNumbers ) {
            if (a.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static AccountNumber getAccountNumberSelected(ArrayList<AccountNumber> accountNumbers){
        for (AccountNumber a: accountNumbers) {
            if (a.isSelected()){
                return a;
            }
        }
        return null;
    }

}
