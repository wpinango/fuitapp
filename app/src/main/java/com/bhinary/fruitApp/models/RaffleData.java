/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bhinary.fruitApp.models;

import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class RaffleData {
    public String date;
    public String raffleWinSerial;
    public String raffleName;
    public LinkedHashMap<String , GameData> results;
}
