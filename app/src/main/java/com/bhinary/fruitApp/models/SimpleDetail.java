package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 6/9/17.
 */

public class SimpleDetail {
    private int chipId;
    private String chip;
    private int raffleId;
    private int gameId;
    private int effectiveSell;
    private String label;
    private int status;
    private String codeName;
    private int serial;
    private int processed;
    private String chipDescription;
    private int tp;

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public void setChipDescription(String chipDescription){
        this.chipDescription = chipDescription;
    }

    public String getChipDescription(){
        return chipDescription;
    }

    public void setProcessed(int processed){
        this.processed = processed;
    }

    public int getProcessed(){
        return processed;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getChipId() {
        return chipId;
    }

    public void setChipId(int chipId) {
        this.chipId = chipId;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public int getRaffleId() {
        return raffleId;
    }

    public void setRaffleId(int raffleId) {
        this.raffleId = raffleId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getEffectiveSell() {
        return effectiveSell;
    }

    public void setEffectiveSell(int effectiveSell) {
        this.effectiveSell = effectiveSell;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

}
