package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 9/5/17.
 */

public class Settings {
    private String title;
    private boolean status;
    private String TAG;

    public void setTAG(String TAG){
        this.TAG = TAG;
    }

    public String getTAG(){
        return TAG;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
