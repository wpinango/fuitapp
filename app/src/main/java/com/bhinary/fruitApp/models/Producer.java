package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/4/17.
 */

public class Producer {
    private String name;
    private int id;
    private float score;
    private ArrayList<Bank> banks;
    private boolean selected;
    private int status;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<Bank> banks) {
        this.banks = banks;
    }

    public static Producer getProducerSelected(ArrayList<Producer> producers) {
        for (Producer p : producers) {
            if (p.isSelected()) {
                return p;
            }
        }
        return null;
    }

    public static boolean isProducerSelected(ArrayList<Producer> producers) {
        for (Producer p : producers) {
            if (p.isSelected()) {
                return true;
            }
        }
        return false;
    }

    public static void selectProducer(ArrayList<Producer> producers, int i) {
        for (int j = 0; j < producers.size(); j ++ ) {
            if (j == i) {
                producers.get(i).setSelected(!producers.get(i).isSelected());
            }else {
                producers.get(j).setSelected(false);
            }
        }
    }

    public static void deselectProducer(ArrayList<Producer> producers){
        for (Producer p : producers){
            p.setSelected(false);
        }
    }
}
