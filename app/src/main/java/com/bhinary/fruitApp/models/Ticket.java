package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 6/23/17.
 */

public class Ticket {
    public String label;
    public Details details;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }


    public Ticket(String label, Details details) {
        this.label = label;
        this.details = details;
    }
}
