package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/3/17.
 */

public class Messages {
    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }

    private ArrayList<ChatMessage> messages;

}
