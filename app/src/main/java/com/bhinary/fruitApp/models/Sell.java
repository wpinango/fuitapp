package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/7/17.
 */

public class Sell {
    public String userId;
    public String xSerial;
    public ArrayList<SellItem> sellItems;

    public String getUserId() {
        return userId;
    }

    public String getxSerial() {
        return xSerial;
    }

    public void setxSerial(String xSerial) {
        this.xSerial = xSerial;
    }

    public ArrayList<SellItem> getSellItems() {
        return sellItems;
    }

    public void setSellItems(ArrayList<SellItem> sellItems) {
        this.sellItems = sellItems;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

