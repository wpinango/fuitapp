package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 6/27/17.
 */

public class Purchase {
    public String label;
    public SellItem sellItem;

    public Purchase(String label, SellItem sellItem) {
        this.label = label;
        this.sellItem = sellItem;
    }
}
