package com.bhinary.fruitApp.models;

import com.bhinary.fruitApp.common.Format;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/5/17.
 */

public class ResultNotification {
    private String serial;
    private int raffle;
    private ArrayList<SimpleDetail> results;
    private String rotationTime;

    public String getRotationTime() {
        return rotationTime;
    }

    public void setRotationTime(String rotationTime) {
        this.rotationTime = rotationTime;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getRaffle() {
        return Format.getRaffleHour(raffle);
    }

    public void setRaffle(int raffle) {
        this.raffle = raffle;
    }

    public ArrayList<SimpleDetail> getResults() {
        return results;
    }

    public void setResults(ArrayList<SimpleDetail> results) {
        this.results = results;
    }
}
