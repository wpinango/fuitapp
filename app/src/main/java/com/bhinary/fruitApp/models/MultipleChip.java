package com.bhinary.fruitApp.models;

/**
 * Created by wpinango on 6/27/17.
 */

public class MultipleChip {
    public String chip;
    public boolean selected;
    public int gameId;
    public String game;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int status;

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public MultipleChip(String chip, boolean selected) {
        this.chip = chip;
        this.selected = selected;
    }
}
