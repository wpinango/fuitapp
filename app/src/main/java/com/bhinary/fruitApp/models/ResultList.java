package com.bhinary.fruitApp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/22/17.
 */

public class ResultList  {
    public ArrayList<SimpleDetail> simpleDetails;
    public String hour;
    public String date;

    public ResultList(String key, ArrayList<SimpleDetail> value, String date) {
        this.hour = key;
        this.simpleDetails = value;
        this.date = date;
    }

    public ArrayList<SimpleDetail> getSimpleDetails() {
        return simpleDetails;
    }

    public void setSimpleDetails(ArrayList<SimpleDetail> simpleDetails) {
        this.simpleDetails = simpleDetails;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
