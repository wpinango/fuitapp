package com.bhinary.fruitApp.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public class Game {

    private LinkedHashMap<String, Chip> chips;
    private LinkedHashMap<Integer, Raffle> raffles;
    private final int id;
    private String codeName;

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }



    public Game(LinkedHashMap<String, Chip> chips, LinkedHashMap<Integer, Raffle> raffles, int id) {
        this.chips = chips;
        this.raffles = raffles;
        this.id = id;
    }

    public LinkedHashMap<String, Chip> getChips() {
        return chips;
    }

    public void setChips(LinkedHashMap<String, Chip> chips) {
        this.chips = chips;
    }

    public LinkedHashMap<Integer, Raffle> getRaffles() {
        return raffles;
    }

    public void setRaffles(LinkedHashMap<Integer, Raffle> raffles) {
        this.raffles = raffles;
    }

    public int getId() {
        return id;
    }


    public ArrayList<Chip> getSelectedChip() {
        ArrayList<Chip> tempChip = new ArrayList<>();
        for (Chip a : chips.values()) {
            if (a.isSelected()) {
                tempChip.add(a);
            }
        }
        return tempChip;
    }
}
