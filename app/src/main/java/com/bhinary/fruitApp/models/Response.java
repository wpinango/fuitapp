package com.bhinary.fruitApp.models;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class Response {
    public long timestamp;
    public int status;
    public String error;
    public String exception;
    public String message;
    public String path;
    public int cash;
    public String username;
    public int locked;
    public int deferred;
    private String hashResult;
    private String hashBalance;

    public String getHashResult() {
        return hashResult;
    }

    public void setHashResult(String hashResult) {
        this.hashResult = hashResult;
    }

    public String getHashBalance() {
        return hashBalance;
    }

    public void setHashBalance(String hashBalance) {
        this.hashBalance = hashBalance;
    }
}
