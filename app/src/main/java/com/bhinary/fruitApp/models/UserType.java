package com.bhinary.fruitApp.models;

/**
 * Created by madhur on 17/01/15.
 */
public enum UserType {
    OTHER, SELF
}
