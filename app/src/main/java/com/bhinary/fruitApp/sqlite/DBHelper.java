package com.bhinary.fruitApp.sqlite;
//2a$12$3fatS7oM7EEDhDRy4rU3/OD8Of8m0Eys2YQBrpch.teZswIoQaoM6


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bhinary.fruitApp.Global;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class DBHelper  extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "MyContacts.db";
    public static String TABLE = "tabla";
    public static String HASH = "hash";
    public static String DATA = "data";
    /*public static final String CONTACTS_TABLE_NAME = "contactos";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";
    public static final String CONTACTS_COLUMN_EMAIL = "email";;
    public static final String CONTACTS_COLUMN_PHONE = "phone";*/
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here
        db.execSQL("create table userData (id integer primary key, username text, token text, agency text, userid text, time text)");
        db.execSQL("create table " + TABLE + " (id integer primary key, hash integer, json text);");
        //db.execSQL("create table time (id integer primary key, date numeric)");
        //String CREATE_TABLE_CONTACT = // "CREATE TABLE " + Person.TABLE + "("
        //"create table contactos " +
         //       "(id integer primary key, name text, email text, idc text, phonne text)";
                /*//+ Person.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Person.KEY_name + " TEXT, "
                + Person.KEY_email + " TEXT, "
                + Person.KEY_ID + " TEXT, "
                + Person.KEY_phone + " TEXT )";*/
       // db.execSQL(CREATE_TABLE_CONTACT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Person.TABLE);
        db.execSQL("DROP TABLE IF EXISTS userData" );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        //db.execSQL("DROP TABLE IF EXISTS time");
        // Create tables again
        onCreate(db);
    }

    public boolean insertDataRefresh(String data){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA, data);
        db.insert(TABLE, null,contentValues);
        db.close();
        return true;
    }

    public void updateDataRefresh(String data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATA, data);
        db.update(TABLE,contentValues,"id = ?", new String[] {Integer.toString(1)});
    }

    public Cursor getDataRefresh() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE, null);
        return res;
    }

    public boolean insertToken (String token, String userName, String agency, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", userName);
        contentValues.put("token", token);
        contentValues.put("agency", agency);
        contentValues.put("userid",userId);
        db.insert("userData", null, contentValues);
        db.close();
        return true;
    }

    public Cursor getToken() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from userData", null);
        return res;
    }

    public void deleteToken () {

    }

    public void updateToken(Integer id, String token, String userName, String agency, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("token",token);
        contentValues.put("username", userName);
        contentValues.put("agency", agency);
        contentValues.put("userid", userId);
        db.update("userData", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
    }

    public boolean insertContact (String name, String phone, String id, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("email", email);
        contentValues.put("idc",id);
        contentValues.put("phonne", phone);
        db.insert(Person.TABLE, null, contentValues);
        db.close();
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+Person.TABLE+" where id="+id+"", null);
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, Person.TABLE);
        return numRows;
    }

    public boolean updateContact (Integer id, String name, String phone, String email, String street, String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("email", email);
        contentValues.put("phone", phone);
        db.update(Person.TABLE, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteContact (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(Person.TABLE, "id = ? ", new String[] { Integer.toString(id) });
    }

    public void  getAllCotacts() {
        clearArrays();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+ Person.TABLE, null );
        res.moveToFirst();
        while(!res.isAfterLast()){
            Global.name.add(res.getString(res.getColumnIndex(Person.KEY_name)));
            Global.phone.add(res.getString(res.getColumnIndex(Person.KEY_phone)));
            Global.id.add(res.getString(res.getColumnIndex(Person.KEY_ID)));
            res.moveToNext();
        }
        if (!res.moveToFirst()) {
            res.close();
        }
    }

    public Person getPersonById(int Id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery =  "SELECT " +
                Person.KEY_ID + "," +
                Person.KEY_name + "," +
                Person.KEY_email +
                " FROM " + Person.TABLE
                + " WHERE " +
                Person.KEY_ID + "=?";// It's a good practice to use parameter ?, instead of concatenate string
        int iCount =0;
        Person person = new Person();
        Cursor res = db.rawQuery(selectQuery, new String[] { String.valueOf(Id) } );
        if (res.moveToFirst()) {
            do {
                person.name = res.getString(res.getColumnIndex(Person.KEY_name));
                person.phone = res.getString(res.getColumnIndex(Person.KEY_phone));
                person.id = res.getString(res.getColumnIndex(Person.KEY_ID));
            } while (res.moveToNext());
        }
        res.close();
        db.close();
        return person;
    }

    public void updateArrays(){

    }

    private void clearArrays() {
        Global.name.clear();
        Global.phone.clear();
        Global.id.clear();
        Global.email.clear();
        Global.alias.clear();
    }
}
