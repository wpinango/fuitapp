package com.bhinary.fruitApp.sqlite;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class Person {
    // Labels table name
    public static final String TABLE = "contactos";

    // Labels Table Columns names
    public static final String KEY_ROWID = "_id";
    public static final String KEY_ID = "idc";
    public static final String KEY_name = "name";
    public static final String KEY_email = "email";
    public static final String KEY_phone = "phonne";
    public static final String KEY_alias = "alias";


    // property help us to keep data
    public String name;
    public String email;
    public String id;
    public String phone;
    public String alias;
}
