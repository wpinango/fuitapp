package com.bhinary.fruitApp.activities;

import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.bhinary.fruitApp.adapters.GameListAdapters.GameListAdapter;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.adapters.GamesChipGridAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class ListGameActivity extends BaseActivity {

    private SwipeMenuListView gameList;
    private GamesChipGridAdapter adapter;
    private AlertDialog iconDialog;
    private String[] lottoChips, fruitChips, rouletteChips;
    private GameListAdapter listAdapter2;
    private ArrayList<String> lotoName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_list_game);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Juegos disponible");
        lotoName = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.LottoNames)));
        listAdapter2 = new GameListAdapter(this,lotoName,Global.lottGame);
        gameList = (SwipeMenuListView)findViewById(R.id.gameList);
        /*SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item backgrounda
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item backgrounda
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.trash_white);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        gameList.setMenuCreator(creator);

        gameList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        break;
                    case 1:
                        // delete
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });*/
        gameList.setAdapter(listAdapter2);
        gameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MosAlerta(position);
            }
        });
        lottoChips = getResources().getStringArray(R.array.lotto_chips_name);
        fruitChips = getResources().getStringArray(R.array.fruit_chips_name);
        rouletteChips = getResources().getStringArray(R.array.roulette_chips_name);
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_list_game;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MosAlerta(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.grid_view_general, null);
        GridView xGrilla = (GridView) ViewAlerta.findViewById(R.id.grdGrilla);
        TextView xTitulo = (TextView) ViewAlerta.findViewById(R.id.lblTitu);

        switch (position){
            case 0:
                adapter = new GamesChipGridAdapter(this, lottoChips, Global.KEY_LA);
                xTitulo.setText("LottoActivo");
                break;
            case 1:
                adapter = new GamesChipGridAdapter(this, fruitChips, Global.KEY_FA);
                xTitulo.setText("Tu Fruta Activa");
                break;
            case 2:
                adapter = new GamesChipGridAdapter(this, rouletteChips, Global.KEY_RA);
                xTitulo.setText("Ruleta Animal");
                break;
        }

        xGrilla.setAdapter(adapter);
        builder.setNegativeButton("Cerrar", null);
        builder.setView(ViewAlerta);
        iconDialog = builder.create();
        iconDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

}
