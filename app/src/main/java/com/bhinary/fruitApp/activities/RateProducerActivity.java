package com.bhinary.fruitApp.activities;

import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 7/25/17.
 */

public class RateProducerActivity extends BaseActivity {
    private ProgressBar progressBar;
    private EditText etComment;
    private RatingBar ratingBar, ratingBar1, ratingBar2, ratingBar3, ratingBar4;
    private JsonArray jsonArray = new JsonArray();
    private String comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_rate_producer);
        setRatingBarColor();
        ImageButton btnSend = (ImageButton) findViewById(R.id.btn_send_rate);
        progressBar = (ProgressBar) findViewById(R.id.progressBar5);
        progressBar.setVisibility(View.INVISIBLE);
        EditText etProducerName = (EditText) findViewById(R.id.et_producer_name_rate);
        EditText etProducerRating = (EditText) findViewById(R.id.et_producer_rating_rate);
        etComment = (EditText)findViewById(R.id.et_comment);
        TextView tvResp = (TextView) findViewById(R.id.tv_resp);
        TextView tvEfic = (TextView) findViewById(R.id.tv_efi);
        TextView tvHones = (TextView) findViewById(R.id.tv_hones);
        TextView tvComun = (TextView) findViewById(R.id.tv_comun);
        TextView tvAse = (TextView) findViewById(R.id.tv_ases);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Califique a su asesor");
        try {
            etProducerName.setText(Global.producerName);
            tvResp.setText(Global.features.get(0).getName());
            tvEfic.setText(Global.features.get(1).getName());
            tvHones.setText(Global.features.get(2).getName());
            tvComun.setText(Global.features.get(3).getName());
            tvAse.setText(Global.features.get(4).getName());
            etProducerRating.setText(Format.getFormattedStringOneDecimal(Global.producerRating));
        } catch (Exception e) {
            e.getMessage();
        }
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingValidation()) {
                    populateRatingArray();
                    showConfirmDialog();
                }
            }
        });
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_rate_producer;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(RateProducerActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea realizar esta calificacion a su asesor?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new RateProducer().execute(Global.URL_RATE_PRODUCER);
            }
        });
        dialog.setNegativeButton("Cancelar", null);
        dialog.show();
    }

    private class RateProducer extends AsyncTask<String, Void, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("targetProducerId",Global.producerId);
            jsonObject.add("features",jsonArray);
            jsonObject.addProperty("comment", comment);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                System.out.println("Error1 http : " + e.getMessage());
                Global.Toaster.get().showToast(RateProducerActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message,BooleanResponse.class);
                    if (booleanResponse.status){
                        new SweetAlertDialog(RateProducerActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Gracias!")
                                .setContentText("por la calificacion a su asesor")
                                .show();
                        ratingBar.setRating(0);ratingBar1.setRating(0);ratingBar2.setRating(0);
                        ratingBar3.setRating(0);ratingBar4.setRating(0);etComment.setText("");
                    } else {
                        new SweetAlertDialog(RateProducerActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!")
                                .setContentText("No se pudo realizar la calificacion")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(RateProducerActivity.this, "Falla de red", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setRatingBarColor() {
        ratingBar = (RatingBar) findViewById(R.id.rb_rate_respon);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        //stars.getDrawable(0).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        ratingBar1 = (RatingBar) findViewById(R.id.rb_rate_efic);
        LayerDrawable stars1 = (LayerDrawable) ratingBar1.getProgressDrawable();
        //stars1.getDrawable(0).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars1.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars1.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        ratingBar2 = (RatingBar) findViewById(R.id.rb_rate_hones);
        LayerDrawable stars2 = (LayerDrawable) ratingBar2.getProgressDrawable();
        //stars2.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars2.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars2.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        ratingBar3 = (RatingBar) findViewById(R.id.rb_rate_comu);
        LayerDrawable stars3 = (LayerDrawable) ratingBar3.getProgressDrawable();
        //stars3.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars3.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars3.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
        ratingBar4 = (RatingBar) findViewById(R.id.rb_rate_aseso);
        LayerDrawable star4 = (LayerDrawable) ratingBar4.getProgressDrawable();
        //star4.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        star4.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        star4.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorAward), PorterDuff.Mode.SRC_ATOP);
    }

    private boolean ratingValidation() {
        boolean valid = true;

        if (ratingBar.getRating() == 0.0 || ratingBar1.getRating() == 0.0 || ratingBar2.getRating() == 0.0
                || ratingBar3.getRating() == 0.0 || ratingBar4.getRating() == 0.0) {
            Toast.makeText(RateProducerActivity.this, "Debe evaluar todas las caracteristicas", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (etComment.getText().length() == 0) {
            Toast.makeText(RateProducerActivity.this,"Debe ingresar un comentario",Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    private void populateRatingArray() {

        float[] ratings = new float[5];
        ratings[0] = ratingBar.getRating();
        ratings[1] = ratingBar1.getRating();
        ratings[2] = ratingBar2.getRating();
        ratings[3] = ratingBar3.getRating();
        ratings[4] = ratingBar4.getRating();

        for (int i = 0 ; i < ratings.length; i++){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("featureId",i + 1);
            jsonObject.addProperty("value", ratings[i]);
            jsonArray.add(jsonObject);
        }

        comment = etComment.getText().toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
