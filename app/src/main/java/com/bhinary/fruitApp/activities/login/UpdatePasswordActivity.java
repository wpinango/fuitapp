package com.bhinary.fruitApp.activities.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.common.LoginState;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 8/25/17.
 */

public class UpdatePasswordActivity extends AppCompatActivity {
    private String oldPassword, newPassword;
    private EditText etOldPassword, etNewPassword, etRepeatNewPassword;
    private ImageButton btnSend;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Cambie su contraseña");
        progressBar = (ProgressBar)findViewById(R.id.progressBar9);
        progressBar.setVisibility(View.INVISIBLE);
        etOldPassword = (EditText)findViewById(R.id.et_actual_password);
        etNewPassword = (EditText)findViewById(R.id.et_new_password);
        etRepeatNewPassword = (EditText)findViewById(R.id.et_repeat_new_password);
        btnSend = (ImageButton)findViewById(R.id.btn_send_update);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    confirmUpdatePassword();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void confirmUpdatePassword() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(UpdatePasswordActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea cambiar su contraseña?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                newPassword = etNewPassword.getText().toString();
                oldPassword = etOldPassword.getText().toString();
                new UpdatePassword().execute(Global.URL_UPDATE_PASSWORD);
                btnSend.setEnabled(true);
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btnSend.setEnabled(true);
            }
        });
        dialog.show();
    }

    private boolean validate(){
        boolean valid = true;
        String oldPAssword = etOldPassword.getText().toString();
        String password = etNewPassword.getText().toString();
        String reEnterPassword = etRepeatNewPassword.getText().toString();
        if (oldPAssword.isEmpty()) {
            etOldPassword.setError("Ingrese su contraseña actual");
            valid = false;
        } else {
            etOldPassword.setError(null);
        }
        if (password.isEmpty() || password.length() < 6 || password.length() > 20) {
            etNewPassword.setError("Contraseña minimo 6, maximo 20 caracteres");
            valid = false;
        } else {
            etNewPassword.setError(null);
        }
        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 6 || reEnterPassword.length() > 20 || !(reEnterPassword.equals(password))) {
            etRepeatNewPassword.setError("No coincien las contraseñas");
            valid = false;
        } else {
            etRepeatNewPassword.setError(null);
        }
        return valid;
    }

    private class UpdatePassword extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();
        HttpRequest req;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .basic(Global.userLogin, oldPassword)
                        .header(Global.NEW_KEY, newPassword)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(UpdatePasswordActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    Global.token = req.header(Global.KEY_TOKEN);
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(UpdatePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("Cambio de contraseña exitoso")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent intent = new Intent(UpdatePasswordActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                        LoginState.setLoginState(UpdatePasswordActivity.this,false);
                                        TransitionAnimation.setOutActivityTransition(UpdatePasswordActivity.this);
                                    }
                                }).show();
                    } else {
                        new SweetAlertDialog(UpdatePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Datos incorrectos")
                                .setContentText("Fallo en cambio de contraseña")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(UpdatePasswordActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
