package com.bhinary.fruitApp.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.common.LoginState;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.fragments.NotificationsFragment;
import com.bhinary.fruitApp.models.LoginMessage;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.sqlite.DBHelper;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.util.SharedPreferenceConstants;


/**
 * Created by willianpinango on 01/05/2017.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private TextView signupLink, forgotLink;
    private EditText userLogin, passwordText;
    private Button loginButton;
    private final int REQUEST_SIGNUP = 0;
    private String status = "";
    private ProgressBar progressBar;
    private HttpRequest request;
    private Gson gson = new Gson();
    private DBHelper dbHelper;
    private boolean isTokenExist = false;
    public static boolean isRegisterSuccess = false;

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(this);
        setContentView(R.layout.activity_login);
        userLogin = (EditText) findViewById(R.id.input_user);
        forgotLink = (TextView) findViewById(R.id.tv_forgot_link);
        passwordText = (EditText) findViewById(R.id.input_pass);
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        userLogin.setText(sharedPreferences.getString("user", ""));
        Cursor res = dbHelper.getToken();
        Global.selectURL();
        Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        progressBar = (ProgressBar) findViewById(R.id.pb_login);
        progressBar.setVisibility(View.INVISIBLE);
        if (res.moveToFirst()) {
            if (!res.getString(res.getColumnIndex("token")).equals(null)) {
                isTokenExist = true;
            }
        }
        loginButton = (Button) findViewById(R.id.btn_login);
        signupLink = (TextView) findViewById(R.id.link_signup);
        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        forgotLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                login();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRegisterSuccess){
            new LoginAsyncTask().execute(Global.userName,Global.password);
            isRegisterSuccess = false;
        }
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private class LoginAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String user = params[0];
            String password = params[1];
            try {
                request = HttpRequest.post(Global.URL_LOGIN_MOBILE)
                        .accept("application/json")
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .header(Global.KEY_TOKEN_FB,Global.refreshedToken)
                        .basic(user, password)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                setStatus("fail");
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                if (!s.isEmpty()) {
                    try {
                        Response response = new Gson().fromJson(s, Response.class);
                        if (response.error.isEmpty()) {
                            Global.token = request.header(Global.KEY_TOKEN);
                            Global.agencyId = request.header(Global.KEY_AGENCY_ID);
                            Global.userId = request.header(Global.KEY_ID_USER);
                            Global.footerTicket = request.header("x-footer");
                            Global.cash = String.valueOf(response.cash);
                            Global.timestamp = response.timestamp;
                            SharedPreferenceConstants.saveTimestamp(LoginActivity.this,Global.timestamp);
                            SharedPreferenceConstants.saveData(LoginActivity.this,s);
                            LoginMessage message = gson.fromJson(response.message, LoginMessage.class);
                            //dbHelper.insertDataRefresh(new Gson().toJson(message.games));
                            Global.currentGames = message.games;
                            Global.producerId = message.producerId;
                            Global.producerName = message.producerName;
                            Global.producerRating = message.producerRating;
                            Global.features = message.producerFeatures;
                            Global.userName = message.username;
                            Global.userLogin = message.userlogin;
                            setStatus("ok");
                        } else {
                            setStatus("fail");
                            Global.Toaster.get().showToast(LoginActivity.this, response.message, Toast.LENGTH_SHORT);
                        }
                    } catch (HttpRequest.HttpRequestException ex) {
                        setStatus("fail");
                        progressBar.setVisibility(View.GONE);
                        Global.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                    }
                    successLogin();
                } else {
                    showToast("Falla de red");
                }
                loginButton.setEnabled(true);
            }catch (Exception e){
                e.getMessage();
            }
        }
    }

    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        loginButton.setEnabled(false);
        String user = userLogin.getText().toString().toUpperCase();
        String password = passwordText.getText().toString();
        new LoginAsyncTask().execute(user, password);
    }

    public boolean validate() {
        boolean valid = true;
        String user = userLogin.getText().toString();
        String password = passwordText.getText().toString();
        TextInputLayout textInputLayout = (TextInputLayout)findViewById(R.id.textInputLayout9);
        if (user.isEmpty()) {
            userLogin.setError("Introduzca un usuario valido");
            valid = false;
        } else {
            userLogin.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            textInputLayout.setError("Introduzca una contrasena valida");
            valid = false;
        } else {
            textInputLayout.setError(null);
        }
        return valid;
    }

    private void successLogin() {
        new android.os.Handler().postDelayed(() -> {
            try {
                if (status.equals("fail")) {
                    onLoginFailed();
                } else {
                    onLoginSuccess();
                    if (isTokenExist) {
                        dbHelper.updateToken(1, Global.token, Global.userLogin, Global.agencyId, Global.userId);
                    } else {
                        NotificationsFragment.saveNotifications(this,"");
                        dbHelper.insertToken(Global.token, Global.userLogin, Global.agencyId, Global.userId);
                    }
                    SharedPreferences preferences = getSharedPreferences("data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putString("user", Global.userLogin);
                    edit.apply();
                    finish();
                }
            } catch (Exception e) {
                onLoginFailed();
            }
        }, 100);
    }



    public void onLoginSuccess() {
        LoginState.setLoginState(this, true);
        Global.isLogin = true;
        progressBar.setVisibility(View.INVISIBLE);
        loginButton.setEnabled(true);
        Global.lastActivityTime = Time.getCurrentTimeInSeconds();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Fallo en Login", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
    }

}