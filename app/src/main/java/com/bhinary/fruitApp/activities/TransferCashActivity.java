package com.bhinary.fruitApp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.FindUserResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import java.io.UnsupportedEncodingException;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 7/25/17.
 */

public class TransferCashActivity extends BaseActivity {
    private EditText etCash, etUser, etPassword, etUserName, etAmount;
    private String userName, pass;
    private ProgressBar progressBar;
    private int targetId, amount;
    private ImageView imageIsFinded;
    private ImageButton btnSend, btnBack;
    private final String ACTION_INTENT = "com.ruletadigital.ruletaplay.fragments.cash.action.UI_UPDATE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_transfer_cash);
        etUser = (EditText) findViewById(R.id.et_user_transfer);
        etUser.setSelection(etUser.getText().length());
        progressBar = (ProgressBar) findViewById(R.id.progressBar6);
        progressBar.setVisibility(View.INVISIBLE);
        etUserName = (EditText) findViewById(R.id.et_user_name);
        etAmount = (EditText) findViewById(R.id.et_amount_transfer);
        etPassword = (EditText) findViewById(R.id.et_password_transfer);
        imageIsFinded = (ImageView) findViewById(R.id.iv_user_is_find);
        btnSend = (ImageButton) findViewById(R.id.btn_send_transfer);
        btnBack = (ImageButton) findViewById(R.id.imageButton3);
        etCash = (EditText) findViewById(R.id.et_cash_transfer);
        IntentFilter filter = new IntentFilter(ACTION_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        ImageButton btnFindUser = (ImageButton) findViewById(R.id.btn_find_user);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Transferir saldo");
        if (Global.cash != null) {
            etCash.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
        }
        etUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (userValidation()) {
                        userName = etUser.getText().toString();
                        new FindUser().execute(Global.URL_FIND_USER);
                    }
                    return true;
                }
                return false;
            }
        });
        btnFindUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userValidation()) {
                    userName = etUser.getText().toString();
                    new FindUser().execute(Global.URL_FIND_USER);
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    btnSend.setEnabled(false);
                    showConfirmDialog();
                }
            }
        });
        etUser.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    imageIsFinded.setImageResource(R.color.transparent);
                    etUserName.setText("");
                }
                return false;
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                TransitionAnimation.setOutActivityTransition(TransferCashActivity.this);
            }
        });
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_transfer_cash;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY3");
                updateCurrentFragmentFromNotification(value);
            }
        }
    };

    private void updateCurrentFragmentFromNotification(String value) {
        etCash.setText(Format.getCashFormat(Integer.valueOf(value)));
    }

    private void showConfirmDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(TransferCashActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea realizar esta transferencia?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (amountValidation()) {
                    amount = Integer.valueOf(etAmount.getText().toString());
                    if (amount <= (Integer.valueOf(Global.cash) - 1000)) {
                        try {
                            pass = Format.getPEncrypted(etPassword.getText().toString());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        new SendTransf().execute(Global.URL_SEND_TRANSFER);
                    } else {
                        Toast.makeText(TransferCashActivity.this, "Debe ingresar un monto permitido", Toast.LENGTH_SHORT).show();
                    }
                }
                btnSend.setEnabled(true);
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btnSend.setEnabled(true);
            }
        });
        dialog.show();
    }

    private boolean validation() {
        boolean valid = true;
        String userName = etUser.getText().toString();
        String cash = etCash.getText().toString();
        String amount = etAmount.getText().toString();
        String pass = etPassword.getText().toString();
        String user = etUser.getText().toString();
        if (userName.isEmpty() || cash.isEmpty() || pass.isEmpty() || user.isEmpty()) {
            Toast.makeText(TransferCashActivity.this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (amount.isEmpty()) {
            Toast.makeText(TransferCashActivity.this, "Debe actualizar su saldo disponible", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (Integer.valueOf(Global.cash) < Integer.valueOf(amount)) {
            Toast.makeText(TransferCashActivity.this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    private boolean userValidation() {
        boolean valid = true;
        String userName = etUser.getText().toString();
        if (userName.isEmpty() || userName.length() < 7 || userName.length() > 8) {
            etUser.setError("Introduzca un usuario valido");
            valid = false;
        } else {
            etUser.setError(null);
        }
        return valid;
    }

    private boolean amountValidation() {
        boolean valid = true;
        String cash = etCash.getText().toString();
        if (cash.isEmpty()) {
            etCash.setError("Introduzca un monto valido");
            valid = false;
        } else {
            etCash.setError(null);
        }
        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class FindUser extends AsyncTask<String, Void, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userName", userName);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                System.out.println("Error1 http : " + e.getMessage());
                Global.Toaster.get().showToast(TransferCashActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                etUserName.setText("");
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    FindUserResponse f = gson.fromJson(response.message, FindUserResponse.class);
                    if (f.status) {
                        targetId = f.getTargetId();
                        etUserName.setText(f.getName());
                        imageIsFinded.setImageResource(R.drawable.ok);
                    }
                } else if (response.error.equals("not-found")) {
                    Global.Toaster.get().showToast(TransferCashActivity.this, "Usuario no encontrado", Toast.LENGTH_SHORT);
                    imageIsFinded.setImageResource(R.drawable.no);
                } else {
                    imageIsFinded.setImageResource(R.color.transparent);
                    Global.Toaster.get().showToast(TransferCashActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SendTransf extends AsyncTask<String, Void, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("targetId", targetId);
            jsonObject.addProperty("amount", amount);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SECURITY, pass.trim())
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                System.out.println("Error1 http : " + e.getMessage());
                Global.Toaster.get().showToast(TransferCashActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    FindUserResponse f = gson.fromJson(response.message, FindUserResponse.class);
                    if (f.status) {
                        new SweetAlertDialog(TransferCashActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La transferencia exitosa")
                                .show();
                        Global.cash = String.valueOf(response.cash);
                        etCash.setText(Format.getCashFormat(response.cash));
                        etAmount.setText("");
                        etUser.setText("");
                        etUserName.setText("");
                        etPassword.setText("");
                        imageIsFinded.setImageResource(R.color.transparent);
                    } else {
                        new SweetAlertDialog(TransferCashActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Datos incorrectos")
                                .setContentText("no se pudo procesar la transferencia")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(TransferCashActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
