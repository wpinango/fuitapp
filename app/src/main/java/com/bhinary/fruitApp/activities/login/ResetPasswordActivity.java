package com.bhinary.fruitApp.activities.login;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 8/3/17.
 */

public class ResetPasswordActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private EditText etEmail;
    private String email;
    private Button btnRequestNewPassword;
    private TextView tvLinkForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        btnRequestNewPassword = (Button)findViewById(R.id.btn_reset_password);
        etEmail = (EditText)findViewById(R.id.input_email_forgot);
        tvLinkForgot = (TextView)findViewById(R.id.link_forgot);
        progressBar = (ProgressBar)findViewById(R.id.pb_forgot);
        progressBar.setVisibility(View.INVISIBLE);
        btnRequestNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailValidation()) {
                    email = etEmail.getText().toString();
                    new RequestResetPassword().execute(Global.URL_REQUEST_PASSWORD);
                }
            }
        });
        tvLinkForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                TransitionAnimation.setOutActivityTransition(ResetPasswordActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private boolean emailValidation(){
        boolean valid = true;
        String email = etEmail.getText().toString();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Introduzca una direccion de correo valida");
            valid = false;
        } else {
            etEmail.setError(null);
        }
        return valid;
    }

    private class RequestResetPassword extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header("x-email",email)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(ResetPasswordActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(ResetPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Email correcto")
                                .setContentText("Revise su bandeja de correo.")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        ResetPasswordActivity.this.finish();
                                        TransitionAnimation.setOutActivityTransition(ResetPasswordActivity.this);
                                    }
                                })
                                .show();
                    } else {
                        new SweetAlertDialog(ResetPasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Email Incorrecto")
                                .setContentText("hubo un problema.")
                                .show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
