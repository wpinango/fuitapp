package com.bhinary.fruitApp.activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Patterns;
import android.view.MenuItem;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.models.UserProfile;
import com.bhinary.fruitApp.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class UserActivity extends BaseActivity {
    private EditText etUser, etEmail, etPhone, etNick;
    private ProgressBar progressBar;
    private ImageButton btnSend, btnRefresh;
    private UserProfile userProfile = new UserProfile();
    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_user);
        progressBar = (ProgressBar)findViewById(R.id.progressBar10);
        progressBar.setVisibility(View.INVISIBLE);
        constraintLayout = (ConstraintLayout)findViewById(R.id.contraint_layout_user);
        etUser = (EditText)findViewById(R.id.et_user_name_user);
        etEmail = (EditText) findViewById(R.id.et_email_user);
        etNick = (EditText)findViewById(R.id.et_nick_name_user);
        etPhone = (EditText)findViewById(R.id.et_phone_user);
        btnSend = (ImageButton)findViewById(R.id.btn_send_user);
        btnRefresh = (ImageButton)findViewById(R.id.btn_refresh_user);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Datos del usuario");
        new GetUserProfile().execute(Global.URL_GET_USER_PROFILE);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    btnSend.setEnabled(false);
                    updateUserProfile();
                }
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetUserProfile().execute(Global.URL_GET_USER_PROFILE);
            }
        });
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_user;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUserProfile() {
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("¿Desea actualizar los datos de usuario?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                userProfile.setEmail(etEmail.getText().toString());
                userProfile.setName(etUser.getText().toString());
                userProfile.setPhone(etPhone.getText().toString());
                userProfile.setNick(etNick.getText().toString());
                new UpdateUserProfile().execute(Global.URL_UPDATE_USER_PROFILE);
                btnSend.setEnabled(true);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btnSend.setEnabled(true);
            }
        });
        dialogo1.show();
    }

    private boolean validate(){
        boolean valid = true;
        String name = etUser.getText().toString();
        String email = etEmail.getText().toString();
        String mobile = etPhone.getText().toString();
        String nickname = etNick.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            etUser.setError("Introduzca al menos 3 caracteres");
            valid = false;
        } else {
            etUser.setError(null);
        }
        if(nickname.isEmpty()){
            etNick.setError("Introduzca un apodo valido");
            valid = false;
        } else {
            etNick.setError(null);
        }
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Introduzca una direccion de correo valida");
            valid = false;
        } else {
            etEmail.setError(null);
        }
        if (mobile.isEmpty() || mobile.length() != 11) {
            etPhone.setError("Introduzca un numero de telefono valido");
            valid = false;
        } else {
            etPhone.setError(null);
        }
        return valid;
    }

    private class GetUserProfile extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(UserActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    UserProfile userProfile = gson.fromJson(response.message,UserProfile.class);
                    etNick.setText(userProfile.getNick());
                    etUser.setText(userProfile.getName());
                    etEmail.setText(userProfile.getEmail());
                    etPhone.setText(userProfile.getPhone());
                } else {
                    Global.Toaster.get().showToast(UserActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdateUserProfile extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(userProfile);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(UserActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message,BooleanResponse.class);
                    if (booleanResponse.status){
                        new SweetAlertDialog(UserActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .show();
                        constraintLayout.requestFocus();
                        new GetUserProfile().execute(Global.URL_GET_USER_PROFILE);
                    }
                } else {
                    Global.Toaster.get().showToast(UserActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
