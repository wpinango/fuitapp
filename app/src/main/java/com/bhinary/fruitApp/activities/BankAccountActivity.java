package com.bhinary.fruitApp.activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bhinary.fruitApp.adapters.AccountListAdapter;
import com.bhinary.fruitApp.common.FourDigitBankAccountFormatWatcher;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.AccountNumber;
import com.bhinary.fruitApp.models.Bank;
import com.bhinary.fruitApp.models.BankList;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 7/27/17.
 */

public class BankAccountActivity extends BaseActivity {
    private ListView lvBanks;
    private ImageButton btnAddAccount, btnEliminate, btnEdit;
    private ProgressBar progressBar;
    private TextView tvAddAccount, tvEliminate, tvEdit;
    private ArrayList<Bank> banks = new ArrayList<>();
    private AccountNumber accountNumber = new AccountNumber();
    private ArrayList<AccountNumber> accountNumbers = new ArrayList<>();
    private AccountListAdapter accountListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_bank_accounts);
        btnAddAccount = (ImageButton) findViewById(R.id.btn_add_account);
        lvBanks = (ListView) findViewById(R.id.lv_banks_accounts);
        tvEliminate = (TextView) findViewById(R.id.textView38);
        progressBar = (ProgressBar) findViewById(R.id.progressBar8);
        progressBar.setVisibility(View.INVISIBLE);
        tvEdit = (TextView) findViewById(R.id.textView42);
        btnEdit = (ImageButton) findViewById(R.id.btn_edit);
        btnEliminate = (ImageButton) findViewById(R.id.btn_delete_user_account);
        tvAddAccount = (TextView) findViewById(R.id.tv_add_bank_account);
        accountListAdapter = new AccountListAdapter(this, accountNumbers);
        lvBanks.setAdapter(accountListAdapter);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Mis cuentas bancarias");
        btnAddAccount.setOnClickListener(view -> AddBankAccount(true));
        disableAddButton();
        new GetBanks().execute(Global.URL_GET_BANKS);
        lvBanks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AccountNumber accountNumber = accountNumbers.get(i);
                for (int j = 0; j < accountNumbers.size(); j++) {
                    if (j == i) {
                        accountNumbers.get(i).setSelected(!accountNumbers.get(i).isSelected());
                    } else {
                        accountNumbers.get(j).setSelected(false);
                    }
                }
                accountListAdapter.notifyDataSetChanged();
                if (accountNumber.isSelected()) {
                    enableChangeButton();
                } else {
                    disableChangeButton();
                }
            }
        });
        disableChangeButton();
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccountNumber.isAccountNumberSelected(accountNumbers)){
                    AddBankAccount(false);
                }
            }
        });
        btnEliminate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccountNumber.isAccountNumberSelected(accountNumbers)) {
                    deleteAccount();
                }
            }
        });
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_bank_accounts;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void AddBankAccount(boolean isAddAccount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_bank_account, null);
        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_account);
        final EditText etAccountNumber = (EditText) view.findViewById(R.id.et_account_number);
        etAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        if (isAddAccount) {
            builder.setTitle("Agregue numero de cuenta");
            builder.setPositiveButton("Agregar", (dialog, which) -> {
            });
        } else {
            AccountNumber a = AccountNumber.getAccountNumberSelected(accountNumbers);
            builder.setTitle("Modifique cuenta");
            etAccountNumber.setText(a.getAccountNumber());
            accountNumber.setBankId(a.getBankId());
            accountNumber.setUserId(a.getUserId());
            accountNumber.setAccountId(a.getAccountId());
            if (a.getType().equals("Corriente")) {
                radioGroup.check(R.id.rb_current);
            } else if (a.getType().equals("Ahorro")) {
                radioGroup.check(R.id.rb_savings);
            }
            builder.setPositiveButton("Editar", (dialog, which) -> {
            });
        }
        builder.setCancelable(false);
        builder.setNegativeButton("Cerrar", null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(BankAccountActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
            if (etAccountNumber.getText().length() != 0 && etAccountNumber.getText().length() == 24 &&
                    radioGroup.getCheckedRadioButtonId() != -1) {
                accountNumber.setAccountNumber(etAccountNumber.getText().toString());
                String code = accountNumber.getAccountNumber().substring(0, 4);
                int value = radioGroup.getCheckedRadioButtonId();
                if (value == R.id.rb_current) {
                    accountNumber.setType("Corriente");
                } else if (value == R.id.rb_savings) {
                    accountNumber.setType("Ahorro");
                }
                if (BankList.isBankExist(BankAccountActivity.this, code)) {
                    accountNumber.setBankId(BankList.findBankId(banks, code));
                    alertDialog.dismiss();
                    showBankFound(code, isAddAccount);
                } else {
                    Global.CenterToaster.get().showToast(BankAccountActivity.this, "Banco no disponible", Toast.LENGTH_SHORT);
                }
            } else if (etAccountNumber.getText().length() != 24) {
                Global.CenterToaster.get().showToast(BankAccountActivity.this, "Ingrese un numero de cuenta valido", Toast.LENGTH_SHORT);
            } else if (radioGroup.getCheckedRadioButtonId() == -1) {
                Global.CenterToaster.get().showToast(BankAccountActivity.this, "Debe seleccionar un tipo de cuenta", Toast.LENGTH_SHORT);
            }
        });
    }

    private void showBankFound(String code, boolean isAddAccount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_confirm_bank_account, null);
        TextView tvAccountBank = (TextView) view.findViewById(R.id.tv_bank_name_confirm);
        TextView tvTypeAccount = (TextView) view.findViewById(R.id.tv_type_account);
        TextView tvAccountNumber = (TextView) view.findViewById(R.id.tv_account_number_confirm);
        ImageView ivBank = (ImageView) view.findViewById(R.id.iv_bank_account);
        ivBank.setImageResource(BankList.getBankImages(BankAccountActivity.this, code));
        tvAccountBank.setText(BankList.findBankName(banks, code));
        tvTypeAccount.setText(accountNumber.getType());
        tvAccountNumber.setText((accountNumber.getAccountNumber()));
        builder.setTitle("Registro de cuenta");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", (dialog, which) -> {
            accountNumber.setAccountNumber(accountNumber.getAccountNumber().replace(Global.KEY_VALUE_ACCOUNT, ""));
            if (isAddAccount) {
                new RegisterNewAccount().execute(Global.URL_REGISTER_ACCOUNT);
            } else {
                new UpdatePlayerAccount().execute(Global.URL_UPDATE_PLAYER_ACCOUNTS);
            }
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {

        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void disableAddButton() {
        btnAddAccount.setEnabled(false);
        btnAddAccount.setImageResource(R.drawable.plus_circle_dis);
        tvAddAccount.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableAddButton() {
        btnAddAccount.setEnabled(true);
        btnAddAccount.setImageResource(R.drawable.plus_circle_white);
        tvAddAccount.setTextColor(Color.WHITE);
    }

    private void disableChangeButton() {
        btnEdit.setEnabled(false);
        btnEliminate.setEnabled(false);
        btnEdit.setImageResource(R.drawable.edit_dis);
        tvEdit.setTextColor(getResources().getColor(R.color.colorDisable));
        btnEliminate.setImageResource(R.drawable.trash_dis);
        tvEliminate.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableChangeButton() {
        btnEdit.setEnabled(true);
        btnEliminate.setEnabled(true);
        btnEdit.setImageResource(R.drawable.edit_white);
        tvEdit.setTextColor(getResources().getColor(R.color.white));
        btnEliminate.setImageResource(R.drawable.trash_white);
        tvEliminate.setTextColor(getResources().getColor(R.color.white));
    }

    private void deleteAccount() {
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("¿Desea eliminar este numero de cuenta?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                new DeletePlayerAccount().execute(Global.URL_DELETE_PLAYER_ACCOUNTS);
            }
        });
        dialogo1.setNegativeButton("Cancelar", null);
        dialogo1.show();
    }

    private class GetBanks extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(BankAccountActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    enableAddButton();
                    Type custom = new TypeToken<ArrayList<Bank>>() {
                    }.getType();
                    banks = gson.fromJson(response.message, custom);
                    new GetUserAccounts().execute(Global.URL_GET_PLAYER_ACCOUNTS);
                } else {
                    Global.Toaster.get().showToast(BankAccountActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RegisterNewAccount extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(accountNumber);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(BankAccountActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(BankAccountActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .show();
                        new GetUserAccounts().execute(Global.URL_GET_PLAYER_ACCOUNTS);
                    }
                } else {
                    Global.Toaster.get().showToast(BankAccountActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetUserAccounts extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(BankAccountActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            accountNumbers.clear();
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    AccountNumber[] a = gson.fromJson(response.message, AccountNumber[].class);
                    for (int i = 0; i < a.length; i++) {
                        accountNumbers.add(a[i]);
                    }
                    accountListAdapter.notifyDataSetChanged();
                } else {
                    Global.Toaster.get().showToast(BankAccountActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdatePlayerAccount extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            ArrayList<AccountNumber>a = new ArrayList<>();
            a.add(accountNumber);
            String jsonData = gson.toJson(a);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(BankAccountActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    AccountNumber[] a = gson.fromJson(response.message, AccountNumber[].class);
                    if (a[0].getStatus()) {
                        new SweetAlertDialog(BankAccountActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("Cambios guardados")
                                .show();
                        new GetUserAccounts().execute(Global.URL_GET_PLAYER_ACCOUNTS);
                    }
                } else {
                    Global.Toaster.get().showToast(BankAccountActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DeletePlayerAccount extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            ArrayList<AccountNumber> accountNumbers1 = new ArrayList<>();
            accountNumbers1.add(AccountNumber.getAccountNumberSelected(accountNumbers));
            String jsonData = gson.toJson(accountNumbers1);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(BankAccountActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    AccountNumber[] a = gson.fromJson(response.message, AccountNumber[].class);
                    if (a[0].getStatus()) {
                        accountNumbers.clear();
                        accountListAdapter.notifyDataSetChanged();
                        new GetUserAccounts().execute(Global.URL_GET_PLAYER_ACCOUNTS);
                    }
                } else {
                    Global.Toaster.get().showToast(BankAccountActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
