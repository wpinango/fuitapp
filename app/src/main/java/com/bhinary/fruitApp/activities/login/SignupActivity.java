package com.bhinary.fruitApp.activities.login;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.bhinary.fruitApp.adapters.ProducerListAdapter;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.NewUser;
import com.bhinary.fruitApp.models.Producer;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by willianpinango on 01/05/2017.
 */

public class SignupActivity extends AppCompatActivity {

    private TextView linkLogin, linkAddProducer;
    private Button btnCreateUser;
    private EditText etName, etDni, etPhone, etEmail, etPassword, etRepeatPassword, etNickname;
    private SweetAlertDialog progressDialog;
    private String producerId;
    private NewUser newUser;
    private ArrayList<Producer> producers = new ArrayList<>();
    private ProducerListAdapter producerListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        etName = (EditText) findViewById(R.id.input_name);
        etDni = (EditText) findViewById(R.id.input_dni);
        etEmail = (EditText) findViewById(R.id.input_email);
        etPassword = (EditText) findViewById(R.id.input_password);
        etRepeatPassword = (EditText) findViewById(R.id.input_reEnterPassword);
        etPhone = (EditText) findViewById(R.id.input_phone);
        btnCreateUser = (Button) findViewById(R.id.btn_signup);
        linkLogin = (TextView) findViewById(R.id.link_login);
        etNickname = (EditText) findViewById(R.id.input_nickname);
        producerListAdapter = new ProducerListAdapter(this, producers);
        new GetProducers().execute(Global.URL_GET_PRODUCER);
        linkAddProducer = (TextView) findViewById(R.id.tv_add_producer);
        linkAddProducer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProducerDialog();
                linkAddProducer.setEnabled(false);
            }
        });
        linkLogin.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                finishSignupActivity();
            }
        });
        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCreateUser.setEnabled(false);
                createUser();
            }
        });
    }

    private void finishSignupActivity(){
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void finishActivityAndLogin(){
        LoginActivity.isRegisterSuccess = true;
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void selectProducerDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.list_view_general, null);
        ListView xGrilla = (ListView) ViewAlerta.findViewById(R.id.listView1);
        TextView xTitulo = (TextView) ViewAlerta.findViewById(R.id.lblTit);
        xTitulo.setText("Seleccione un asesor de juego");
        xGrilla.setAdapter(producerListAdapter);
        builder.setPositiveButton("Ok", null);
        builder.setNegativeButton("Actualizar", null);
        builder.setNeutralButton("Cerrar", null);
        builder.setView(ViewAlerta);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnRefresh = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btnRefresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new GetProducers().execute(Global.URL_GET_PRODUCER);
                    }
                });
                Button btnConfirm = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btnConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Producer.isProducerSelected(producers)) {
                            linkAddProducer.setText(Producer.getProducerSelected(producers).getName() + " " +
                                    Producer.getProducerSelected(producers).getScore());
                            alertDialog.dismiss();
                            linkAddProducer.setEnabled(true);
                        } else {
                            Toast.makeText(SignupActivity.this, "Debe seleccionar un asesor de juego", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                Button btnClose = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        linkAddProducer.setEnabled(true);
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.show();
    }

    private String getProducerId() {
        return String.valueOf(Producer.getProducerSelected(producers).getId());
    }

    private void createUser() {
        if (validate()) {
            if (etPhone.length() != 0 && etName.length() != 0 && etDni.length() != 0 && etEmail.length() != 0
                    && etPassword.length() != 0 && etRepeatPassword.length() != 0 && !producers.isEmpty() &&
                    Producer.isProducerSelected(producers)) {
                newUser = new NewUser();
                newUser.setName(etName.getText().toString());
                newUser.setCedula(Integer.parseInt(etDni.getText().toString()));
                newUser.setEmail(etEmail.getText().toString());
                newUser.setPhone(etPhone.getText().toString());
                newUser.setPassword(etPassword.getText().toString());
                newUser.setUserName("M" + etDni.getText());
                newUser.setNick(etNickname.getText().toString());
                producerId = getProducerId();
                progressDialog = new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                progressDialog.setTitleText("Enviando datos de registro");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new SendNewRegister().execute(Global.URL_GET_REGISTER_NEW_USERS);
                btnCreateUser.setEnabled(true);
            } else if (producers.isEmpty()) {
                btnCreateUser.setEnabled(true);
                Toast.makeText(SignupActivity.this, "Debe tener conexion con internet para registrarse", Toast.LENGTH_SHORT).show();
            } else if (!Producer.isProducerSelected(producers)) {
                Toast.makeText(SignupActivity.this, "Debe seleccionar un asesor de juego", Toast.LENGTH_SHORT).show();
            } else {
                btnCreateUser.setEnabled(true);
                Toast.makeText(SignupActivity.this, "Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(SignupActivity.this, "Fallo en registro de usuario", Toast.LENGTH_SHORT).show();
            btnCreateUser.setEnabled(true);
        }
    }

    public boolean validate() {
        boolean valid = true;
        btnCreateUser.setEnabled(true);
        TextInputLayout textInputLayout1 = (TextInputLayout)findViewById(R.id.textInputLayout37);
        TextInputLayout textInputLayout2 = (TextInputLayout)findViewById(R.id.textInputLayout31);
        String dni = etDni.getText().toString();
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String mobile = etPhone.getText().toString();
        String password = etPassword.getText().toString();
        String nickname = etNickname.getText().toString();
        String reEnterPassword = etRepeatPassword.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            etName.setError("Introduzca al menos 3 caracteres");
            valid = false;
        } else {
            etName.setError(null);
        }
        if (nickname.isEmpty()) {
            etNickname.setError("Introduzca un apodo valido");
            valid = false;
        } else {
            etNickname.setError(null);
        }
        if (dni.isEmpty() || dni.length() < 7) {
            etDni.setError("Introduzca un numero de cedula valido");
            valid = false;
        } else {
            etDni.setError(null);
        }
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Introduzca una direccion de correo valida");
            valid = false;
        } else {
            etEmail.setError(null);
        }
        if (mobile.isEmpty() || mobile.length() != 11) {
            etPhone.setError("Introduzca un numero de telefono valido");
            valid = false;
        } else {
            etPhone.setError(null);
        }
        if (password.isEmpty() || password.length() < 6 || password.length() > 20) {
            textInputLayout1.setError("Contraseña minimo 6, maximo 20 caracteres");
            valid = false;
        } else {
            textInputLayout1.setError(null);
        }
        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 6 || reEnterPassword.length() > 20 || !(reEnterPassword.equals(password))) {
            textInputLayout2.setError("No coincien las contraseñas");
            valid = false;
        } else {
            textInputLayout2.setError(null);
        }
        return valid;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(SignupActivity.this);
    }

    private class GetProducers extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl).accept("application/json")
                        .header(Global.KEY_PRODUCER_ID, 0)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(SignupActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    producers.clear();
                    Type custom = new TypeToken<ArrayList<Producer>>() {
                    }.getType();
                    ArrayList<Producer> p = gson.fromJson(response.message, custom);
                    producers.addAll(p);
                    producerListAdapter.notifyDataSetChanged();
                } else {
                    Global.Toaster.get().showToast(SignupActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SendNewRegister extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(newUser);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_PRODUCER_ID, producerId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(SignupActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("Su usuario es " + newUser.getUserName() +
                                ", presione OK para entrar a la aplicacion.")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Global.userName = newUser.getUserName();
                                        Global.password = newUser.getPassword();
                                        etPhone.setText("");
                                        etName.setText("");
                                        etEmail.setText("");
                                        etRepeatPassword.setText("");
                                        etPassword.setText("");
                                        etDni.setText("");
                                        etNickname.setText("");
                                        finishActivityAndLogin();
                                    }
                                })
                                .show();

                    } else {
                        new SweetAlertDialog(SignupActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Datos Incorrectos")
                                .setContentText("Hubo un problema en la comprobacion de los datos")
                                .show();
                    }
                } else {
                    Global.Toaster.get().showToast(SignupActivity.this, response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                System.out.println("balance2 : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }


}
