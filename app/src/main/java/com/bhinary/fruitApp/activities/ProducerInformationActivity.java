package com.bhinary.fruitApp.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.bhinary.fruitApp.adapters.BankListAdapter;
import com.bhinary.fruitApp.adapters.CommentsListAdapter;
import com.bhinary.fruitApp.util.chart.Chart;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.common.TransitionAnimation;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.Bank;
import com.bhinary.fruitApp.models.Comment;
import com.bhinary.fruitApp.models.ProducerInformation;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/25/17.
 */

public class ProducerInformationActivity extends BaseActivity {
    private ListView lvComment, lvBanks;
    private CommentsListAdapter commentsListAdapter;
    private ArrayList<Comment>comments = new ArrayList<>();
    private ArrayList<Bank> banks = new ArrayList<>();
    private BankListAdapter bankListAdapter;
    private ProgressBar progressBar;
    private android.support.v7.app.ActionBar actionBar;
    private EditText etCed, etProducerName, etProducerEmail,etProducerLocation;
    private TextView tvRating, tvTotalUser;
    private RatingBar ratingBar;
    private HorizontalBarChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_producer_information);
        ratingBar = (RatingBar) findViewById(R.id.rb_producer);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        ratingBar.setRating(4);
        etCed = (EditText)findViewById(R.id.et_ced);
        etProducerName = (EditText)findViewById(R.id.et_producer_name);
        etProducerEmail = (EditText)findViewById(R.id.et_producer_email);
        etProducerLocation = (EditText)findViewById(R.id.et_producer_location);
        tvRating = (TextView) findViewById(R.id.tv_producer_rating);
        tvTotalUser = (TextView)findViewById(R.id.tv_total_user);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Informacion de su asesor");
        progressBar = (ProgressBar)findViewById(R.id.progressBar4);
        progressBar.setVisibility(View.INVISIBLE);
        lvComment = (ListView)findViewById(R.id.lv_comment);
        commentsListAdapter = new CommentsListAdapter(this, comments);
        lvComment.setAdapter(commentsListAdapter);
        lvBanks = (ListView)findViewById(R.id.lv_bank_account);
        bankListAdapter = new BankListAdapter(this,banks);
        lvBanks.setAdapter(bankListAdapter);
        chart = (HorizontalBarChart)findViewById(R.id.chart);
        chart.setDoubleTapToZoomEnabled(false);
        new GetProducerRating().execute(Global.URL_GET_PRODUCER_RATING);
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_producer_information;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private class GetProducerRating extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject producerId = new JsonObject();
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(producerId.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(ProducerInformationActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    ProducerInformation p = gson.fromJson(response.message,ProducerInformation.class);
                    etProducerName.setText(p.getProducerName());
                    etCed.setText(Format.getCashFormat(Integer.valueOf(p.getCed())));
                    etProducerEmail.setText(p.getEmail());
                    etProducerLocation.setText(p.getLocation());
                    tvRating.setText(Format.getFormattedStringOneDecimal(p.getProducerRating()));
                    ratingBar.setRating(p.getProducerRating());
                    tvTotalUser.setText(String.valueOf(p.getTotalUsers()));
                    banks.addAll(p.getBanks());
                    bankListAdapter.notifyDataSetChanged();
                    comments.addAll(p.getComments());
                    commentsListAdapter.notifyDataSetChanged();
                    Chart.paintChart(p.getFeatures(),Chart.setData(p.getFeatures()),chart);
                } else {
                    Global.Toaster.get().showToast(ProducerInformationActivity.this, "Falla de red", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
