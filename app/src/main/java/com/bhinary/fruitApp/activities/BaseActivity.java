package com.bhinary.fruitApp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bhinary.fruitApp.Global;

/**
 * Created by wpinango on 9/13/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResoursesId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityPaused();
    }

    protected abstract int getLayoutResoursesId();

    public static void activityResumed() {
        Global.activityVisible = true;
    }

    public static void activityPaused() {
        Global.activityVisible = false;
    }

}
