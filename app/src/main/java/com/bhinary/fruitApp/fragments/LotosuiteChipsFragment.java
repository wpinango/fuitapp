package com.bhinary.fruitApp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.adapters.GameListAdapters.ChipsPlayGridAdapter;
import com.bhinary.fruitApp.models.Chip;

/**
 * Created by wpinango on 11/13/17.
 */

public class LotosuiteChipsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grid_play2, container, false);
        final TextView tvNumberOfPlays = (TextView) rootView.findViewById(R.id.tv_play_number);
        ImageView img1 = (ImageView) rootView.findViewById(R.id.imageView90);
        ImageView img2 = (ImageView) rootView.findViewById(R.id.imageView10);
        ImageView img3 = (ImageView) rootView.findViewById(R.id.imageView110);
        ImageButton btnCancel = (ImageButton)rootView.findViewById(R.id.imageButton10);
        ImageButton btnAdd = (ImageButton) rootView.findViewById(R.id.imageButton9);
        ImageButton plus = (ImageButton) rootView.findViewById(R.id.imageButton8);
        ImageButton sus = (ImageButton) rootView.findViewById(R.id.imageButton6);
        EditText etAmount = (EditText) rootView.findViewById(R.id.etAmount);
        etAmount.setGravity(Gravity.CENTER);
        etAmount.setHorizontallyScrolling(false);
        etAmount.setText("100");
        etAmount.setGravity(Gravity.CENTER);
        //img1.setImageResource(getResources().getIdentifier(gamesNameLogo.get(0), "drawable", getActivity().getPackageName()));
        //img2.setImageResource(getResources().getIdentifier(gamesNameLogo.get(1), "drawable", getActivity().getPackageName()));
        //img3.setImageResource(getResources().getIdentifier(gamesNameLogo.get(2), "drawable", getActivity().getPackageName()));
        TextView tvRaffle = (TextView) rootView.findViewById(R.id.tv_raffle_chips);
        //tvRaffle.setText("Sorteo: " + Format.getRaffleHour(RaffleLabel.getSelectedRaffle(rafflesList).getHour()));
        ChipsPlayGridAdapter chipsGameGridAdapter = new ChipsPlayGridAdapter(getActivity(), Global.chipArrayList);
        GridView gridView = (GridView) rootView.findViewById(R.id.grid_play);
        gridView.setAdapter(chipsGameGridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Global.chipArrayList.get(i).setSelected(!Global.chipArrayList.get(i).isSelected());
                chipsGameGridAdapter.notifyDataSetChanged();
                tvNumberOfPlays.setText(String.valueOf(Chip.getNumberChipsSelected(Global.currentGames)));
            }
        });
        etAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //playsListView.setFocusable(false);
                    return true;
                }
                return false;
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) + 100));
            }
        });
        sus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etAmount.getText().toString().equals("0") && !etAmount.getText().toString().equals("")) {
                    etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) - 100));
                    if (Integer.parseInt(etAmount.getText().toString()) <= 0 && !etAmount.getText().toString().equals("")) {
                        etAmount.setText("0");
                    }
                }
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Global.currentGames != null && Chip.isChipSelected() && Integer.valueOf(etAmount.getText().toString()) >= 50) {
                        //amount = Integer.valueOf(etAmount.getText().toString());
                        //addPlay();
                        Chip.clearChipSelection();
                        //raffleGameListView.setEnabled(true);
                    } else if (!Chip.isChipSelected()){
                        Toast.makeText(getActivity(),"Debe seleccionar una ficha", Toast.LENGTH_SHORT).show();
                    } else if (Integer.valueOf(etAmount.getText().toString()) < 50) {
                        Toast.makeText(getActivity(),"Monto minimo 50",Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chip.clearChipSelection();
                //raffleGameListView.setEnabled(true);
            }
        });

        return rootView;
    }
}
