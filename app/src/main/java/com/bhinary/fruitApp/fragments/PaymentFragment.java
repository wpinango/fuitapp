package com.bhinary.fruitApp.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.adapters.PaymentListAdapter;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.common.FourDigitBankAccountFormatWatcher;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.AccountNumber;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.ItemPending;
import com.bhinary.fruitApp.models.Payment;
import com.bhinary.fruitApp.models.PaymentPending;
import com.bhinary.fruitApp.models.RechargeItemPending;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.models.TransactionType;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class PaymentFragment extends Fragment {
    private EditText etAmount, etCode, etAmountPermitted;
    private RadioButton rbEfective, rbTrans;
    private TextView tvDelete,tvInfo;
    private Payment payment;
    private ImageButton btnSend, btnDelete, btnInfo;
    private SweetAlertDialog progressDialog;
    private PaymentListAdapter paymentListAdapter;
    private RechargeItemPending paymentItemPending = new RechargeItemPending();
    private ProgressBar progressBar;
    private Long ts;
    private Spinner spinner;
    private ArrayList<String>accounts = new ArrayList<>();
    private ArrayList<AccountNumber> accountNumbers = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    public static String refresh = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment, null);
        etAmount = (EditText)rootView.findViewById(R.id.et_amount_payment);
        spinner = (Spinner)rootView.findViewById(R.id.sp_account);
        arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item2, accounts);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        etCode = (EditText) rootView.findViewById(R.id.et_code_payment);
        btnSend = (ImageButton)rootView.findViewById(R.id.btn_sent_payment);
        etAmountPermitted = (EditText)rootView.findViewById(R.id.et_amount_payment_request);
        ts = System.currentTimeMillis() / 1000;
        etCode.setText(Format.getFormatValidationCode(String.valueOf(ts)));
        final InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        tvDelete = (TextView)rootView.findViewById(R.id.tv_delete_label_payment);
        tvInfo = (TextView)rootView.findViewById(R.id.tv_info_label);
        btnInfo = (ImageButton)rootView.findViewById(R.id.btn_info_payment);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        btnDelete = (ImageButton)rootView.findViewById(R.id.btn_delete_payment);
        ImageButton btnRefresh = (ImageButton) rootView.findViewById(R.id.btn_refresh_payment);
        paymentListAdapter = new PaymentListAdapter(getActivity(),paymentItemPending);
        ListView listView = (ListView)rootView.findViewById(R.id.lv_payment);
        listView.setAdapter(paymentListAdapter);
        //showBetaDialog();
        rbEfective = (RadioButton)rootView.findViewById(R.id.rb_savings);
        rbTrans = (RadioButton)rootView.findViewById(R.id.rb_current);
        rbTrans.setChecked(true);
        try {
            etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
        } catch ( Exception e ) {
            e.getMessage();
        }
        etAmount.addTextChangedListener(new TextWatcher() {
            String afterTextChanged = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                afterTextChanged = etAmount.getText().toString();
                try {
                    if (Integer.parseInt(afterTextChanged) > 10000) {
                        rbTrans.setChecked(true);
                        rbEfective.setEnabled(false);
                        rbTrans.toggle();
                    }else if (Integer.parseInt(afterTextChanged) < 10000)  {
                        rbEfective.setEnabled(true);
                    }
                } catch (Exception e ){
                    e.getMessage();
                }
            }
        });
        rbEfective.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rbEfective.isChecked()) {
                    imm.hideSoftInputFromWindow(rbEfective.getWindowToken(), 0);
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSend.setEnabled(false);
                //Toast.makeText(getActivity(),"No se puede realizar cobro al ser una version de prueba ",Toast.LENGTH_SHORT).show();
                sendRequest();
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPendingPayments();
                disableInfoButton();
                disableDeleteButton();
            }
        });
        if (paymentListAdapter.isEmpty()){
            getPendingPayments();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ItemPending itemPending = paymentItemPending.getPending().get(i);
                for (int j = 0; j < paymentItemPending.getPending().size(); j ++ ) {
                    if (j == i) {
                        paymentItemPending.getPending().get(i).setSelected(!paymentItemPending.getPending().get(i).isSelected());
                    }else {
                        paymentItemPending.getPending().get(j).setSelected(false);
                    }
                }
                paymentListAdapter.notifyDataSetChanged();
                if (itemPending.isSelected() && itemPending.getStatus() == 0 ){
                    enableDeleteButton();
                }else {
                    disableDeleteButton();
                }
                if (itemPending.isSelected()) {
                    enableInfoButton();
                } else {
                    disableInfoButton();
                }
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDelete.setEnabled(false);
                elminateItem();
            }
        });
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInformation();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (refresh != null) {
                requestPendingTransaction();
                refresh = null;
            }
            setNewCash();
        } catch ( Exception e ) {
            e.getMessage();
        }
    }

    public void setNewCash(){
        etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
    }

    public void requestPendingTransaction() {
        new GetPendingPayments().execute(Global.URL_GET_PENDING_PAYMENT);
    }

    private void showInformation(){
        if (paymentItemPending.isItemSelected(paymentItemPending.getPending())) {
            ItemPending itemPending = paymentItemPending.getSelectedItem(paymentItemPending.getPending());
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View View = inflater.inflate(R.layout.dialog_pending_information, null);
            TextView tvAmoutn = (TextView) View.findViewById(R.id.tv_amount_information);
            TextView tvDate = (TextView) View.findViewById(R.id.tv_date_information);
            TextView tvType = (TextView) View.findViewById(R.id.tv_type_information);
            TextView tvObervation = (TextView) View.findViewById(R.id.tv_observation_information);
            TextView tvCode = (TextView) View.findViewById(R.id.tv_ref_information);
            TextView tvStatus = (TextView)View.findViewById(R.id.textView55);
            tvDate.setText((itemPending.getOperationDate()));
            tvObervation.setText(itemPending.getObservations());
            tvCode.setText(Format.getFormatValidationCode(itemPending.getRefNumber()));
            tvAmoutn.setText(Format.getCashFormat(itemPending.getAmount()));
            if (itemPending.getStatus() == 0 ) {
                tvStatus.setText("En espera por aprobacion");
            }  else  if (itemPending.getStatus() == 2) {
                tvStatus.setText("Rechazado por el asesor");
            } else  if (itemPending.getStatus() == 3) {
                tvStatus.setText("Anulado por el jugador");
            }
            if (itemPending.getType() == TransactionType.CASH_WITHDRAWAL){
                tvType.setText("Efectivo");
            } else if (itemPending.getType() == TransactionType.TRANSFER_WITHDRAWAL) {
                tvType.setText("Transferencia");
            }
            builder.setTitle("Informacion");
            builder.setCancelable(false);
            builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    btnSend.setEnabled(true);
                }
            });
            builder.setView(View);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private void elminateItem() {
        if (paymentItemPending.isItemSelected(paymentItemPending.getPending())) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular esta(s) solicitud de cobro?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    btnDelete.setEnabled(true);
                    disableDeleteButton();
                    progressBar.setVisibility(View.VISIBLE);
                    new DeletePendingTransaction().execute(Global.URL_DELETE_PENDING_TRANSACTION);
                }
            });
            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    btnDelete.setEnabled(true);
                }
            });
            dialogo1.show();
        } else  {
            btnDelete.setEnabled(true);
        }
    }

    private void disableInfoButton() {
        btnInfo.setImageResource(R.drawable.info_dis);
        tvInfo.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableInfoButton() {
        btnInfo.setImageResource(R.drawable.info);
        tvInfo.setTextColor(Color.WHITE);
    }

    private void disableDeleteButton() {
        btnDelete.setImageResource(R.drawable.nul_dis);
        tvDelete.setTextColor(getResources().getColor(R.color.colorDisable));
        //btnDelete.setEnabled(true);
    }

    private void enableDeleteButton(){
        btnDelete.setImageResource(R.drawable.nul_white);
        tvDelete.setTextColor(Color.WHITE);
        //btnDelete.setEnabled(true);
    }

    private void getPendingPayments() {
        progressBar.setVisibility(View.VISIBLE);
        new GetPendingPayments().execute(Global.URL_GET_PENDING_PAYMENT);
    }

    private void requestPayment() {
        try {
            progressDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
            progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            progressDialog.setTitleText("Validando cobro");
            progressDialog.setCancelable(false);
            progressDialog.show();
            new RequestPayment().execute(Global.URL_GET_PAYMENT);
        }catch (Exception e){
            e.getMessage();
        }
    }

    private int getAmountAllowed() {
        if (Integer.valueOf(Global.cash) <= 0){
            return 0;
        }
        return Integer.valueOf(Global.cash) - 1000;
    }

    private void sendRequest() {
        if (etAmount.getText().length() != 0 && etCode.getText().length() != 0
                && (rbEfective.isChecked() || rbTrans.isChecked())){
            payment = new Payment();
            if ((getAmountAllowed()) >= Integer.valueOf(etAmount.getText().toString()) ){
                if (rbTrans.isChecked()){
                    if (!accountNumbers.isEmpty()) {
                        AccountNumber accountNumber = accountNumbers.get(spinner.getSelectedItemPosition());
                        payment.setType(TransactionType.TRANSFER_WITHDRAWAL);
                        payment.setTypeName("Transferencia");
                        payment.setAccountNumber(accountNumber.getAccountNumber());
                        payment.setAmount(Integer.parseInt(etAmount.getText().toString()));
                        payment.setOperationDate(Time.getCurrentDate());
                        payment.setRefNumber(String.valueOf(ts));
                        payment.setUserId(Global.userId);
                        payment.setAccountId(accountNumber.getAccountId());
                        confirmDialog();
                    } else {
                        btnSend.setEnabled(true);
                        Toast.makeText(getActivity(),"Debe tener al menos una cuenta registrada en Mis Cuentas Bancarias",Toast.LENGTH_SHORT).show();
                    }
                } else if (rbEfective.isChecked()) {
                    payment.setType(TransactionType.CASH_WITHDRAWAL);
                    payment.setTypeName("Efectivo");
                    payment.setAccountNumber("No Aplica");
                    payment.setAmount(Integer.parseInt(etAmount.getText().toString()));
                    payment.setOperationDate(Time.getCurrentDate());
                    payment.setRefNumber(String.valueOf(ts));
                    payment.setUserId(Global.userId);
                    //payment.setAccountId(accountNumber.getAccountId());
                    confirmDialog();
                } else {
                    Toast.makeText(getActivity(), "Debe introducir un numero de cuenta valido", Toast.LENGTH_SHORT).show();
                    btnSend.setEnabled(true);
                }
            } else  {
                btnSend.setEnabled(true);
                Toast.makeText(getActivity(), "Monto excede de su saldo disponible",Toast.LENGTH_SHORT).show();
            }
        } else {
            btnSend.setEnabled(true);
            Toast.makeText(getActivity(),"Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void confirmDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View View = inflater.inflate(R.layout.dialog_confirm_payment, null);
        TextView tvAmount = (TextView)View.findViewById(R.id.tv_amount_payment);
        TextView tvType = (TextView)View.findViewById(R.id.tv_type_payment);
        TextView tvAcoount = (TextView)View.findViewById(R.id.tv_account_payment);
        TextView tvCode = (TextView)View.findViewById(R.id.tv_code_payment);
        tvAcoount.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        tvAmount.setText(Format.getCashFormat(payment.getAmount()));
        tvType.setText(String.valueOf(payment.getTypeName()));
        tvAcoount.setText(payment.getAccountNumber());
        tvCode.setText(Format.getFormatValidationCode(payment.getRefNumber()));
        builder.setTitle("¿Desea realizar este cobro?");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPayment();
                btnSend.setEnabled(true);
            }
        });
        builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnSend.setEnabled(true);
            }
        });
        builder.setView(View);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private class RequestPayment extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            jsonData = gson.toJson(payment);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    RechargeItemPending a = gson.fromJson(response.message, RechargeItemPending.class);
                    if (a.isStatus()){
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La peticion de cobro fue enviada, pendiente por aprobacion")
                                //.setContentText("***Por los momentos esta operacion no genera ningun beneficio real al ser" +
                                //        "una version beta***")
                                .show();
                        new GetPendingPayments().execute(Global.URL_GET_PENDING_PAYMENT);
                        etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                        Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                        ((MainActivity)getActivity()).setNewRechargeCash();
                    }
                    ts = System.currentTimeMillis() / 1000;
                    etCode.setText(Format.getFormatValidationCode(String.valueOf(ts)));
                    etAmount.setText("");
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetPendingPayments extends AsyncTask<String,Void,String>{
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            jsonData = gson.toJson(payment);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    accounts.clear();
                    accountNumbers.clear();
                    PaymentPending a = gson.fromJson(response.message,PaymentPending.class);
                    paymentItemPending.setPending(a.getPending());
                    paymentItemPending.setStatus(a.isStatus());
                    accountNumbers.addAll(a.getAccountNumbers());
                    for (int i = 0; i < accountNumbers.size(); i ++){
                        accounts.add(accountNumbers.get(i).getBank() + " ..." +
                                accountNumbers.get(i).getAccountNumber().substring(16,20));
                    }
                    arrayAdapter.notifyDataSetChanged();
                    paymentListAdapter.notifyDataSetChanged();
                    Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                    etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DeletePendingTransaction extends AsyncTask<String,Void,String>{
        Gson gson = new Gson();
        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(paymentItemPending.getSelectedItem(paymentItemPending.getPending()));
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.isStatus()) {
                        paymentItemPending.getPending().get(paymentItemPending.getItemIdSelected(paymentItemPending.getPending())).setStatus(3);
                        paymentListAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Se anulo transacion", Toast.LENGTH_SHORT).show();
                        Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                        etAmountPermitted.setText(Format.getCashFormat(getAmountAllowed()));
                        ((MainActivity)getActivity()).setNewRechargeCash();
                    } else {
                        Toast.makeText(getActivity(), "No se pudo anular transaccion", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
