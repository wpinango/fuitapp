package com.bhinary.fruitApp.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.adapters.RechargeListAdapter;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.Bank;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.ItemPending;
import com.bhinary.fruitApp.models.Recharge;
import com.bhinary.fruitApp.models.RechargeItemPending;
import com.bhinary.fruitApp.models.RechargePending;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.models.TransactionType;
import com.bhinary.fruitApp.R;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by wpinango on 6/14/17.
 */

public class RechargeFragment extends android.support.v4.app.Fragment {
    private EditText etCalendar, etReference, etAmount, etCash;
    private Spinner spBank;
    private ArrayList<String> bank = new ArrayList<>();
    private Recharge recharge;
    private String dateRecharge;
    private SweetAlertDialog progressDialog;
    private RadioButton rbDeposit, rbTransf;
    private RechargeListAdapter rechargeListAdapter;
    private ImageButton btnSend;
    private ImageButton btnCancel;
    private ImageButton btnInfo;
    private RechargeItemPending rechargeItemPending = new RechargeItemPending();
    private ProgressBar progressBar;
    private TextView tvDelete,tvInfo;
    private ArrayList<Bank> banks = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    public static String refresh = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_recharge, null);
        etCalendar = (EditText) rootView.findViewById(R.id.et_date_recharge);
        etReference = (EditText)rootView.findViewById(R.id.et_reference_recharge);
        ImageButton btnRefresh = (ImageButton) rootView.findViewById(R.id.btn_refresh_recharge);
        btnCancel = (ImageButton)rootView.findViewById(R.id.btn_cancel_recharge);
        etAmount = (EditText)rootView.findViewById(R.id.et_amount_recharge);
        spBank = (Spinner)rootView.findViewById(R.id.sp_bank);
        rbDeposit = (RadioButton)rootView.findViewById(R.id.rb_deposit);
        rbTransf = (RadioButton)rootView.findViewById(R.id.rb_transf);
        rbTransf.setChecked(true);
        etCash = (EditText)rootView.findViewById(R.id.et_cash_recharge);
        try{
            etCash.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
        } catch (Exception e) {
            e.getMessage();
        }
        tvInfo = (TextView)rootView.findViewById(R.id.tv_info_label_recharge);
        btnInfo = (ImageButton)rootView.findViewById(R.id.btn_info_recharge);
        tvDelete = (TextView)rootView.findViewById(R.id.tv_delete_label_recharge);
        progressBar = (ProgressBar)rootView.findViewById(R.id.pb_refresh_recharge);
        progressBar.setVisibility(View.INVISIBLE);
        rechargeListAdapter = new RechargeListAdapter(getActivity(),rechargeItemPending);
        ListView lvRecharge = (ListView) rootView.findViewById(R.id.lv_recharge);
        lvRecharge.setAdapter(rechargeListAdapter);
        etCalendar.setText(com.bhinary.fruitApp.common.Time.getCurrentDate());
        btnSend = (ImageButton) rootView.findViewById(R.id.btn_sent_recharge);
        arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item2, bank);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spBank.setAdapter(arrayAdapter);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCancel.setEnabled(false);
                elminateItem();
            }
        });
        etCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDatePicker();
            }
        });
        spBank.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etReference.getWindowToken(), 0);
                return false;
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(),"No se puede realizar recargas al ser una version de prueba",Toast.LENGTH_SHORT).show();
                btnSend.setEnabled(false);
                sendRequest();
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPendingRecharge();
                disableInfoButton();
                disableDeleteButton();
            }
        });
        if (rechargeListAdapter.isEmpty()){
            getPendingRecharge();
        }
        lvRecharge.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ItemPending itemPending = rechargeItemPending.getPending().get(i);
                for (int j = 0; j < rechargeItemPending.getPending().size(); j ++ ) {
                    if (j == i) {
                        rechargeItemPending.getPending().get(i).setSelected(!rechargeItemPending.getPending().get(i).isSelected());
                    }else {
                        rechargeItemPending.getPending().get(j).setSelected(false);
                    }
                }
                rechargeListAdapter.notifyDataSetChanged();
                if (itemPending.isSelected() && itemPending.getStatus() == 0){
                    enableDeleteButton();
                }else {
                    disableDeleteButton();
                }
                if (itemPending.isSelected()) {
                    enableInfoButton();
                } else {
                    disableInfoButton();
                }
            }
        });
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInformation();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (refresh != null) {
                getPendingRecharge();
                refresh = null;
            }
            setNewCash();
        }catch (Exception e) {
            e.getMessage();
        }
    }

    public void setNewCash(){
        try {
            etCash.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showInformation(){
        if (rechargeItemPending.isItemSelected(rechargeItemPending.getPending())) {
            ItemPending itemPending = rechargeItemPending.getSelectedItem(rechargeItemPending.getPending());
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View View = inflater.inflate(R.layout.dialog_pending_information, null);
            TextView tvAmoutn = (TextView) View.findViewById(R.id.tv_amount_information);
            TextView tvDate = (TextView) View.findViewById(R.id.tv_date_information);
            TextView tvType = (TextView) View.findViewById(R.id.tv_type_information);
            TextView tvObervation = (TextView) View.findViewById(R.id.tv_observation_information);
            TextView tvCode = (TextView) View.findViewById(R.id.tv_ref_information);
            TextView tvStatus = (TextView)View.findViewById(R.id.textView55);
            tvDate.setText((itemPending.getOperationDate()));
            tvObervation.setText(itemPending.getObservations());
            tvCode.setText((itemPending.getRefNumber()));
            tvAmoutn.setText(Format.getCashFormat(itemPending.getAmount()));
            tvType.setText("Recarga bancaria");
            if (itemPending.getStatus() == 0 ) {
                tvStatus.setText("En espera por aprobacion");
            }  else  if (itemPending.getStatus() == 2) {
                tvStatus.setText("Rechazado por el asesor");
            } else  if (itemPending.getStatus() == 3) {
                tvStatus.setText("Anulado por el jugador");
            }
            builder.setTitle("Informacion");
            builder.setCancelable(false);
            builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    btnSend.setEnabled(true);
                }
            });
            builder.setView(View);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private void elminateItem() {
        if (rechargeItemPending.isItemSelected(rechargeItemPending.getPending())) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular esta(s) solicitud de recarga?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    btnCancel.setEnabled(true);
                    disableDeleteButton();
                    progressBar.setVisibility(View.VISIBLE);
                    new DeletePendingTransaction().execute(Global.URL_DELETE_PENDING_TRANSACTION);
                }
            });
            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    btnCancel.setEnabled(true);
                }
            });
            dialogo1.show();
        } else  {
            btnCancel.setEnabled(true);
        }
    }

    private void disableInfoButton() {
        btnInfo.setImageResource(R.drawable.info_dis);
        tvInfo.setTextColor(getResources().getColor(R.color.colorDisable));
    }

    private void enableInfoButton() {
        btnInfo.setImageResource(R.drawable.info);
        tvInfo.setTextColor(Color.WHITE);
    }

    private void disableDeleteButton() {
        btnCancel.setImageResource(R.drawable.nul_dis);
        tvDelete.setTextColor(getResources().getColor(R.color.colorDisable));
        //btnDelete.setEnabled(true);
    }

    private void enableDeleteButton(){
        btnCancel.setImageResource(R.drawable.nul_white);
        tvDelete.setTextColor(Color.WHITE);
        //btnDelete.setEnabled(true);
    }

    public void getPendingRecharge(){
        progressBar.setVisibility(View.VISIBLE);
        new GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
    }

    public void requestPendingTransaction(){
        new GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
    }

    private void sendRequest() {
        if (etAmount.getText().length() != 0 && etReference.getText().length() != 0 && etCalendar.getText().length() != 0
                && (rbDeposit.isChecked() || rbTransf.isChecked()) && !banks.isEmpty()) {
            int type = 0;
            if(rbDeposit.isChecked()) {
                type = TransactionType.TRANSFER_RECHARGE;
            }else if (rbTransf.isChecked()) {
                type = TransactionType.TRANSFER_RECHARGE;
            }
            try {
                recharge = new Recharge();
                recharge.setUserId(Global.userId);
                recharge.setAmount(Integer.parseInt(etAmount.getText().toString()));
                recharge.setOperationDate(etCalendar.getText().toString());
                recharge.setRefNumber(etReference.getText().toString());
                recharge.setType(type);
                recharge.setAccountId(banks.get(spBank.getSelectedItemPosition()).getAccountId());
                confirmDialog();
            }catch (Exception e ) {
                System.out.println("error : " + e.getMessage());
            }
        } else if (banks.isEmpty()) {
            btnSend.setEnabled(true);
            Toast.makeText(getActivity(),"Su productor no posee cuentas asociadas", Toast.LENGTH_SHORT).show();
        } else {
            btnSend.setEnabled(true);
            Toast.makeText(getActivity(),"Debe rellenar todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = (DatePicker) view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -6);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(new Time(System.currentTimeMillis()).getTime());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int month = myDatePicker.getMonth() + 1;
                        int day = myDatePicker.getDayOfMonth();
                        int year = myDatePicker.getYear();
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + day;
                        if (month < 10) {
                            formattedMonth = "0" + month;
                        }
                        if (day < 10) {
                            formattedDayOfMonth = "0" + day;
                        }
                        dateRecharge = String.valueOf(formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                        etCalendar.setText(dateRecharge);
                        dialog.cancel();
                    }
                }).show();
    }

    private void confirmDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.dialog_confirm_recharge, null);
        TextView tvAmount = (TextView)ViewAlerta.findViewById(R.id.tv_amount_recharge);
        TextView tvReference = (TextView)ViewAlerta.findViewById(R.id.tv_reference_recharge);
        TextView tvDate = (TextView)ViewAlerta.findViewById(R.id.tv_date_recharge);
        TextView tvBank = (TextView)ViewAlerta.findViewById(R.id.tv_bank_recharge);
        tvAmount.setText(Format.getCashFormat(recharge.getAmount()));
        tvReference.setText(String.valueOf(recharge.getRefNumber()));
        tvDate.setText(recharge.getOperationDate());
        tvBank.setText(banks.get(spBank.getSelectedItemPosition()).getName());
        builder.setTitle("¿Desea realizar la recarga?");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                progressDialog.setTitleText("Validando recarga");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new RequestRecharge().execute(Global.URL_REQUEST_CASH);
                btnSend.setEnabled(true);
            }
        });
        builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnSend.setEnabled(true);
            }
        });
        builder.setView(ViewAlerta);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void onTaskExecuted(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.cash.action.UI_UPDATE");
        intent.putExtra("UI_KEY3", value);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private class RequestRecharge extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            jsonData = gson.toJson(recharge);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    RechargeItemPending a = gson.fromJson(response.message, RechargeItemPending.class);
                    if (a.isStatus()){
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("La peticion de recarga fue enviada, pendiente por aprobacion")
                                //.setContentText("***Actualmente las recargas son simuladas***")
                                .show();
                        new GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
                        Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                        ((MainActivity)getActivity()).updateBalanceAmounts();
                    }
                    etAmount.setText("");
                    etCalendar.setText(com.bhinary.fruitApp.common.Time.getCurrentDate());
                    etReference.setText("");
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetPendingRecharge extends AsyncTask<String,Void,String>{
        Gson gson = new Gson();
        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            jsonData = gson.toJson(recharge);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    banks.clear();
                    bank.clear();
                    RechargePending a = gson.fromJson(response.message,RechargePending.class);
                    rechargeItemPending.setPending(a.getPending());
                    rechargeItemPending.setStatus(a.isStatus());
                    rechargeListAdapter.notifyDataSetChanged();
                    banks.addAll(a.getBanks());
                    for (Bank b : banks) {
                        bank.add(b.getName() + "..." + b.getAccount().substring(16,20));
                    }
                    arrayAdapter.notifyDataSetChanged();
                    Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                    ((MainActivity)getActivity()).updateBalanceAmounts();
                    setNewCash();
                    onTaskExecuted(String.valueOf(response.cash));
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DeletePendingTransaction extends AsyncTask<String,Void,String>{
        Gson gson = new Gson();
        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData = gson.toJson(rechargeItemPending.getSelectedItem(rechargeItemPending.getPending()));
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }


        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.isStatus()) {
                        new GetPendingRecharge().execute(Global.URL_GET_PENDING_RECHARGE);
                        Toast.makeText(getActivity(), "Se anulo transacion", Toast.LENGTH_SHORT).show();
                        Global.setBalanceResponse(response.cash,response.deferred,response.locked);
                        ((MainActivity)getActivity()).updateBalanceAmounts();
                    } else {
                        Toast.makeText(getActivity(), "No se pudo anular transaccion", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
