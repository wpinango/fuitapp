package com.bhinary.fruitApp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhinary.fruitApp.R;

/**
 * Created by wpinango on 11/13/17.
 */

public class CustomFragment extends Fragment {
    private String mText = "";

    public static CustomFragment createInstance(String txt) {
        CustomFragment fragment = new CustomFragment();
        fragment.mText = txt;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sample,container,false);
        ((TextView) v.findViewById(R.id.textView)).setText(mText);
        return v;
    }
}