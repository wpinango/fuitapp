package com.bhinary.fruitApp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bhinary.fruitApp.adapters.GameListAdapters.ChipPlayListAdapter;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.activities.login.LoginActivity;
import com.bhinary.fruitApp.adapters.ListConfirmAdapter;
import com.bhinary.fruitApp.adapters.PlaysListAdapter;
import com.bhinary.fruitApp.adapters.RaffleListAdapter;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.common.LoginState;
import com.bhinary.fruitApp.common.OrderArrayListWithZeros;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.models.Chip;
import com.bhinary.fruitApp.models.Game;
import com.bhinary.fruitApp.models.LoginMessage;
import com.bhinary.fruitApp.models.Raffle;
import com.bhinary.fruitApp.models.RaffleLabel;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.models.Sell;
import com.bhinary.fruitApp.models.SellItem;
import com.bhinary.fruitApp.sqlite.DBHelper;
import com.bhinary.fruitApp.util.SharedPreferenceConstants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by wpinango on 10/9/17.
 */

public class PrimaryFragment extends Fragment implements PlaysListAdapter.AdapterCallback{
    private EditText etTotal;
    private TextView etBalance;
    private ListView playsListView;
    private ListView raffleGameListView;
    private ImageButton btnPlay, btnCancel;
    private int total = 0;
    static int cash;
    private DecimalFormat formatter;
    private LinkedHashMap<String, SellItem> myGames = new LinkedHashMap<>();
    private SweetAlertDialog progressDialog;
    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<SellItem> sellItems = new ArrayList<>();
    private PlaysListAdapter playsListAdapter;
    private boolean isPlayAdded = false;
    private RaffleListAdapter raffleListAdapter;
    private ArrayList<RaffleLabel> rafflesList = new ArrayList<>();
    private ArrayList<Chip> chipArrayList = new ArrayList<>();
    private ArrayList<String> gamesNameLogo = new ArrayList<>();
    private int amount;
    private ArrayList<Chip> lotto = new ArrayList<>();
    private ArrayList<Chip> roulette = new ArrayList<>();
    private ArrayList<Chip> fruit = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_primary, container, false);
        init(rootView);
        try {
            cash = Integer.valueOf(Global.cash);
        } catch (Exception e) {
            e.getMessage();
        }
        etBalance.setText(Format.getCashFormat(cash));
        if (savedInstanceState != null) {
            cash = savedInstanceState.getInt("cash");
        }
        raffleGameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                RaffleLabel.clearRaffleSelection(rafflesList);
                RaffleLabel.checkRaffleTime(rafflesList);
                rafflesList.get(i).setSelected(true);
                raffleListAdapter.notifyDataSetChanged();
                if (RaffleLabel.isRaffleSelected(rafflesList)) {
                    RaffleLabel.setSelectedRaffle(rafflesList);
                    isPlayAdded = true;
                    raffleGameListView.setEnabled(false);
                    showChipsAvailable();
                }
            }
        });
        playsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playsListAdapter.toggleChecked(position);
                if (playsListAdapter.isSelected()) {
                    playsListView.setEnabled(false);
                    editAmount(position);
                }
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnPlay.setEnabled(false);
                makePlay();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    refreshContent();
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        return rootView;
    }

    private void editAmount(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setTitle("Modifique jugada");
        View ViewAlerta = inflater.inflate(R.layout.dialog_plays_edit, null);
        ImageButton btnConfirm = (ImageButton) ViewAlerta.findViewById(R.id.imageButton4);
        ImageButton btnClose = (ImageButton) ViewAlerta.findViewById(R.id.imageButton7);
        ImageButton btnDelete = (ImageButton) ViewAlerta.findViewById(R.id.imageButton5);
        ImageButton btnPlus = (ImageButton) ViewAlerta.findViewById(R.id.imageButton2);
        ImageButton btnMinus = (ImageButton) ViewAlerta.findViewById(R.id.imageButton);
        EditText tvAmount = (EditText) ViewAlerta.findViewById(R.id.et_amount_edit);
        btnPlus.setOnClickListener(v -> tvAmount.setText(String.valueOf(Integer.parseInt(tvAmount.getText().toString()) + 100)));
        btnMinus.setOnClickListener(v -> {
            if (!tvAmount.getText().toString().equals("0") && !tvAmount.getText().toString().equals("")) {
                tvAmount.setText(String.valueOf(Integer.parseInt(tvAmount.getText().toString()) - 100));
                if (tvAmount.getText().toString().equals("")) {
                    tvAmount.setText("0");
                }
            }
        });
        SellItem sellItem = myGames.get(playsListAdapter.getString(position));
        tvAmount.setText(String.valueOf(sellItem.amount));
        builder.setCancelable(false);
        builder.setView(ViewAlerta);
        AlertDialog alertDialog = builder.create();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                playsListView.setEnabled(true);
                playsListAdapter.updateDelete();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePlay();
                alertDialog.dismiss();
                playsListView.setEnabled(true);
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.valueOf(tvAmount.getText().toString()) >= 50) {
                    sellItem.amount = Integer.valueOf(tvAmount.getText().toString());
                    myGames.put(playsListAdapter.getString(position), sellItem);
                    addHashMapToAdapter(true);
                    setEditAmount();
                    alertDialog.dismiss();
                    playsListAdapter.updateDelete();
                    playsListView.setEnabled(true);
                } else {
                    Toast.makeText(getActivity(), R.string.primary_empty_amount_toast, Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }

    private void makePlay() {
        if (cash >= getTotalAmount() && !playsListAdapter.isEmpty()) {
            showPlaysConfirmed();
        } else if (cash < getTotalAmount()) {
            Toast.makeText(getActivity(), R.string.primary_insufficient_balance_toast, Toast.LENGTH_SHORT).show();
            btnPlay.setEnabled(true);
        } else if (playsListAdapter.isEmpty()) {
            Toast.makeText(getActivity(), R.string.primary_finish_play_toast, Toast.LENGTH_SHORT).show();
            btnPlay.setEnabled(true);
        }
    }

    public void cancel() {
        if (Global.currentGames != null) {
            setNewAmount("0");
            for (int i = 0; i < Global.rafflesByGames.size(); i++) {
                Global.rafflesByGames.get(i).setSelected(false);
            }
            RaffleLabel.clearRaffleSelection(rafflesList);
            raffleListAdapter.notifyDataSetChanged();
            myGames.clear();
            sellItems.clear();
            playsListAdapter.notifyDataSetChanged();
            Chip.clearChipSelection();
        }
    }

    private void deletePlay() {
        if (Global.mycheked.containsValue(true)) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea eliminar esta(s) jugada?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    Toast.makeText(getActivity(), R.string.primary_play_delete_toast, Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < playsListAdapter.getCount(); i++) {
                        if (Global.mycheked.get(i).equals(true)) {
                            if (myGames.containsValue(playsListAdapter.getItem(i))) {
                                myGames.remove(playsListAdapter.getString(i));
                                eliminateAmount(playsListAdapter.getAmount(i));
                            }
                        }
                    }
                    addHashMapToAdapter(false);
                }
            });
            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    playsListAdapter.updateDelete();
                }
            });
            dialogo1.show();
        }
    }

    public void setCash() {
        cash = Integer.parseInt(Global.cash);
        etBalance.setText(Format.getCashFormat(cash));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.putInt("cash", cash);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Global.currentGames == null) {
                DBHelper dbHelper = new DBHelper(getActivity());
                Cursor res = dbHelper.getToken();
                if (res.moveToFirst()) {
                    Global.token = res.getString(res.getColumnIndex("token"));
                    Global.userId = res.getString(res.getColumnIndex("userid"));
                    Global.agencyId = res.getString(res.getColumnIndex("agency"));
                    Global.timestamp = SharedPreferenceConstants.getSavedTimestamp(getActivity());
                }
                populateSavedJsonData(SharedPreferenceConstants.getSavedData(getActivity()));
                new ApplySynchronization().execute(Global.URL_SYNCRO_MOBILE);
            } else if (Global.currentGames != null && raffleListAdapter.isEmpty()) {
                getData();
            } else {
                if (Time.consultSyncronizationTime(getActivity(), Time.getCurrentTimeInSeconds())) {
                    getData();
                }
            }
            cash = Integer.valueOf(Global.cash);
            etBalance.setText(Format.getCashFormat(cash));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void populateSavedJsonData(String jsonData) {
        Response response = new Gson().fromJson(jsonData, Response.class);
        LoginMessage message = new Gson().fromJson(response.message, LoginMessage.class);
        Global.currentGames = message.games;
        Global.cash = String.valueOf(response.cash);
        cash = response.cash;
        getData();
    }

    private void saveIncomingCashPlayer(int amount){
        try {
            if (!SharedPreferenceConstants.getSavedData(getActivity()).equals("")) {
                String s = SharedPreferenceConstants.getSavedData(getActivity());
                Response res = new Gson().fromJson(s, Response.class);
                res.cash = amount;
                SharedPreferenceConstants.saveData(getActivity(), new Gson().toJson(res));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void refreshContent() {
        try {
            mSwipeRefreshLayout.setRefreshing(false);
            new ApplySynchronization().execute(Global.URL_SYNCRO_MOBILE);
        } catch (Exception e){
            e.getMessage();
        }
    }

    private void addPlay() {
        if (RaffleLabel.isRaffleSelected(rafflesList) &&
                Chip.isChipSelected()) {
            validateRafflesAndChips();
            isPlayAdded = true;
        } else if (amount < 50) {
            Toast.makeText(getActivity(), R.string.primary_empty_amount_toast, Toast.LENGTH_LONG).show();
        }  else if (!RaffleLabel.isRaffleSelected(rafflesList) || rafflesList.isEmpty()) {
            Toast.makeText(getActivity(), R.string.primary_empty_raffle, Toast.LENGTH_LONG).show();
        } else if (!Chip.isChipSelected()) {
            Toast.makeText(getActivity(), R.string.primary_empty_chip_toast, Toast.LENGTH_SHORT).show();
        }
    }

    private void validateRafflesAndChips() {
        filterRaffle();
        for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
            Game game = games.getValue(); // Objeto
            String gameName = games.getKey(); // nombre de juego\
            LinkedHashMap<Integer, Raffle> raffles = game.getRaffles();
            for (Map.Entry<Integer, Raffle> ra : raffles.entrySet()) {
                int raffleName = ra.getKey();
                Raffle raffle = ra.getValue();
                if (raffle.isSelected()) {
                    for (Chip chip : game.getSelectedChip()) {
                        SellItem sellItem = new SellItem();
                        sellItem.gameId = game.getId();
                        sellItem.raffleId = raffle.getId();
                        sellItem.chipId = chip.id;
                        sellItem.amount = amount;
                        sellItem.game = gameName;
                        sellItem.raffle = raffleName;
                        sellItem.chip = chip.chipLabel;
                        sellItem.codeName = game.getCodeName();
                        sellItem.description = chip.description;
                        String gameRaffleChip = sellItem.game + " " + Format.getRaffleHour(sellItem.raffle) +
                                " \n" + sellItem.chip + " " + chip.description;
                        if (myGames.containsKey(gameRaffleChip)) {
                            SellItem se = new SellItem();
                            se.amount = myGames.get(gameRaffleChip).amount;
                            se.amount += amount;
                            sellItem.amount = se.amount;
                            myGames.put(gameRaffleChip, sellItem);
                        } else {
                            myGames.put(gameRaffleChip, sellItem);
                        }
                        setTotalAmount();
                    }
                }
            }
        }
        addHashMapToAdapter(true);
    }

    private void addHashMapToAdapter(boolean flag) {
        sellItems.clear();
        Global.mycheked.clear();
        int j = 0;
        if (flag) {
            if ((myGames.size() - playsListView.getCount() == 0)) {
                Toast.makeText(getActivity(), "Se actualizo monto", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Se añadieron " + String.valueOf(myGames.size() - playsListView.getCount() + " jugada(s)"), Toast.LENGTH_SHORT).show();
            }
        }
        for (Map.Entry<String, SellItem> entry : myGames.entrySet()) {
            sellItems.add(0, entry.getValue());
            Global.mycheked.put(j, false);
            j++;
        }
        playsListAdapter.notifyDataSetChanged();
    }

    private void eliminateAmount(int amount) {
        setNewAmount(String.valueOf(getTotalAmount() - amount));
    }

    private int getTotalAmount() {
        return total;
    }

    private void setEditAmount() {
        int amount = 0;
        for (SellItem se : sellItems) {
            amount += se.amount;
        }
        setNewAmount(String.valueOf(amount));
    }

    private void setNewAmount(String amount) {
        etTotal.setText(formatter.format(Integer.valueOf(amount)));
        total = Integer.valueOf(amount);
    }

    private void setTotalAmount() {
        int totalTemp = amount;
        total += totalTemp;
        etTotal.setText(formatter.format(total));
    }

    private String getTotalPlays() {
        return String.valueOf(sellItems.size());
    }

    private void showPlaysConfirmed() {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            Collections.sort(sellItems, new Comparator<SellItem>() {
                @Override
                public int compare(SellItem o1, SellItem o2) {
                    return Integer.valueOf(o1.raffle).compareTo(o2.raffle);
                }
            });
            ListConfirmAdapter listConfirmAdapter = new ListConfirmAdapter(getActivity(), sellItems, true);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View ViewAlerta = inflater.inflate(R.layout.list_confirm_plays, null);
            ListView ListView = (ListView) ViewAlerta.findViewById(R.id.lv_confirm_pays);
            TextView lvTitulo = (TextView) ViewAlerta.findViewById(R.id.tv_confirm_title);
            String value = "Jugadas: <b>" + getTotalPlays() + "</b> | Total: <b>" + Format.getCashFormat(getTotalAmount()) + "</b>";
            lvTitulo.setText(Html.fromHtml(value));
            ListView.setAdapter(listConfirmAdapter);
            builder.setCancelable(false);
            builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    progressDialog.setTitleText("Validando jugada");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    btnPlay.setEnabled(true);
                    new ApplySell().execute(Global.ENDPOINT_SELL);
                }
            });
            builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    btnPlay.setEnabled(true);
                }
            });
            builder.setView(ViewAlerta);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.getMessage();
            btnPlay.setEnabled(true);
            Toast.makeText(getActivity(),"Algo salio mal, actualice los sorteos",Toast.LENGTH_SHORT).show();
        }
    }

    private void filterRaffle() {
        int tmpRaffle = 0;
        rafflesList.clear();
        for (int i = Global.rafflesByGames.size() - 1; i >= 0; i--) {
            if (Global.rafflesByGames.get(i).getHour() <= Time.getCurrentTime()) {
                Global.rafflesByGames.remove(i);
            }
        }
        for (int i = 0; i < Global.rafflesByGames.size(); i++) {
            if (tmpRaffle != Global.rafflesByGames.get(i).getHour()) {
                rafflesList.add(Global.rafflesByGames.get(i));
            }
            tmpRaffle = Global.rafflesByGames.get(i).getHour();
        }
        raffleListAdapter.notifyDataSetChanged();
    }

    private void showChipsAvailable() {
        AlertDialog iconDialog;
        if (isPlayAdded) {
            Chip.clearChipSelection();
            isPlayAdded = false;
        }
        populateArrayListChips(chipArrayList);
        populateArrayChips(chipArrayList);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.grid_play, null);
        final TextView tvNumberOfPlays = (TextView) view.findViewById(R.id.tv_play_number);
        ListView lvRoulette = (ListView)view.findViewById(R.id.lv_roulette);
        ListView lvFruit = (ListView)view.findViewById(R.id.lv_fruit);
        ListView lvLotto = (ListView)view.findViewById(R.id.lv_lotto);
        ImageView img1 = (ImageView) view.findViewById(R.id.imageView90);
        ImageView img2 = (ImageView) view.findViewById(R.id.imageView10);
        ImageView img3 = (ImageView) view.findViewById(R.id.imageView110);
        ImageButton btnCancel = (ImageButton)view.findViewById(R.id.imageButton10);
        ImageButton btnAdd = (ImageButton) view.findViewById(R.id.imageButton9);
        ImageButton plus = (ImageButton) view.findViewById(R.id.imageButton8);
        ImageButton sus = (ImageButton) view.findViewById(R.id.imageButton6);
        EditText etAmount = (EditText) view.findViewById(R.id.etAmount);
        etAmount.setGravity(Gravity.CENTER);
        etAmount.setHorizontallyScrolling(false);
        etAmount.setText("100");
        etAmount.setGravity(Gravity.CENTER);
        img1.setImageResource(getResources().getIdentifier(gamesNameLogo.get(0).split(" ")[0].toLowerCase(), "drawable", getActivity().getPackageName()));
        img2.setImageResource(getResources().getIdentifier(gamesNameLogo.get(1).split(" ")[0].toLowerCase(), "drawable", getActivity().getPackageName()));
        img3.setImageResource(getResources().getIdentifier(gamesNameLogo.get(2).split(" ")[0].toLowerCase(), "drawable", getActivity().getPackageName()));
        TextView tvRaffle = (TextView) view.findViewById(R.id.tv_raffle_chips);
        RaffleLabel raffleLabel = RaffleLabel.getSelectedRaffle(rafflesList);
        tvRaffle.setText("Sorteo: " + Format.getRaffleHour(raffleLabel.getHour()));
        ChipPlayListAdapter lottoAdapter = new ChipPlayListAdapter(getActivity(),lotto);
        ChipPlayListAdapter rouletteAdapter = new ChipPlayListAdapter(getActivity(),roulette);
        ChipPlayListAdapter fruitAdapter = new ChipPlayListAdapter(getActivity(),fruit);
        lvFruit.setAdapter(fruitAdapter);
        lvRoulette.setAdapter(rouletteAdapter);
        lvLotto.setAdapter(lottoAdapter);
        lvFruit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                fruit.get(i).setSelected(!fruit.get(i).isSelected());
                fruitAdapter.notifyDataSetChanged();
                tvNumberOfPlays.setText(String.valueOf(Chip.getNumberChipsSelected(Global.currentGames)));
            }
        });
        lvLotto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lotto.get(i).setSelected(!lotto.get(i).isSelected());
                lottoAdapter.notifyDataSetChanged();
                tvNumberOfPlays.setText(String.valueOf(Chip.getNumberChipsSelected(Global.currentGames)));
            }
        });
        lvRoulette.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                roulette.get(i).setSelected(!roulette.get(i).isSelected());
                rouletteAdapter.notifyDataSetChanged();
                tvNumberOfPlays.setText(String.valueOf(Chip.getNumberChipsSelected(Global.currentGames)));
            }
        });
        builder.setCancelable(false);
        builder.setView(view);
        iconDialog = builder.create();
        etAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    playsListView.setFocusable(false);
                    return true;
                }
                return false;
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) + 100));
            }
        });
        sus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etAmount.getText().toString().equals("0") && !etAmount.getText().toString().equals("")) {
                    etAmount.setText(String.valueOf(Integer.parseInt(etAmount.getText().toString()) - 100));
                    if (Integer.parseInt(etAmount.getText().toString()) <= 0 && !etAmount.getText().toString().equals("")) {
                        etAmount.setText("0");
                    }
                }
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Global.currentGames != null && Chip.isChipSelected() && Integer.valueOf(etAmount.getText().toString()) >= 50) {
                        amount = Integer.valueOf(etAmount.getText().toString());
                        addPlay();
                        Chip.clearChipSelection();
                        raffleGameListView.setEnabled(true);
                        iconDialog.dismiss();
                    } else if (!Chip.isChipSelected()){
                        Toast.makeText(getActivity(),"Debe seleccionar una ficha", Toast.LENGTH_SHORT).show();
                    } else if (Integer.valueOf(etAmount.getText().toString()) < 50) {
                        Toast.makeText(getActivity(),"Monto minimo 50",Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iconDialog.dismiss();
                Chip.clearChipSelection();
                raffleGameListView.setEnabled(true);
            }
        });
        iconDialog.show();
    }


    void init(View rootView) {
        formatter = new DecimalFormat("#,###");
        etTotal = (EditText) rootView.findViewById(R.id.etTotal);
        etTotal.setGravity(Gravity.CENTER);
        etTotal.setHorizontallyScrolling(false);
        etTotal.setHintTextColor(getResources().getColor(R.color.white));
        btnCancel = (ImageButton) rootView.findViewById(R.id.btn_cancel);
        etBalance = (TextView) rootView.findViewById(R.id.et_amount);
        etBalance.setHorizontallyScrolling(false);
        etBalance.setGravity(Gravity.CENTER);
        btnPlay = (ImageButton) rootView.findViewById(R.id.btn_play);
        raffleGameListView = (ListView) rootView.findViewById(R.id.lv_repeat_raffles);
        playsListView = (ListView) rootView.findViewById(R.id.PlaysListView);
        raffleListAdapter = new RaffleListAdapter(getActivity(), rafflesList);
        raffleGameListView.setAdapter(raffleListAdapter);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        playsListAdapter = new PlaysListAdapter(getActivity(), sellItems, PrimaryFragment.this);
        playsListView.setAdapter(playsListAdapter);
        playsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (playsListView == null || playsListView.getChildCount() == 0) ? 0 : playsListView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        raffleGameListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (raffleGameListView == null || raffleGameListView.getChildCount() == 0) ? 0 : raffleGameListView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void getData() {
        Global.rafflesByGames.clear();
        chipArrayList.clear();
        gamesNameLogo.clear();
        if (Global.currentGames != null) {
            for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
                Game sg = games.getValue(); // Objeto
                String gameName = games.getKey(); // nombre de juego\
                gamesNameLogo.add(OrderArrayListWithZeros.setLotteryImage(sg.getCodeName()));
                LinkedHashMap<Integer, Raffle> raffle = sg.getRaffles();
                LinkedHashMap<String, Chip> chips = sg.getChips();
                for (Map.Entry<Integer, Raffle> ra : raffle.entrySet()) {
                    ra.getValue().setGameId(games.getValue().getId());
                    if (ra.getKey() > Time.getCurrentTime()) {
                        RaffleLabel raffleLabel = new RaffleLabel(gameName, ra.getValue(), ra.getKey());
                        Global.rafflesByGames.add(raffleLabel);
                    }
                }
                for (Map.Entry<String, Chip> chip : chips.entrySet()) {
                    Chip c = chip.getValue();
                    c.chipLabel = chip.getKey();
                    c.gameId = sg.getId();
                    c.codeName = sg.getCodeName();
                    chipArrayList.add(c);
                }
            }
            Collections.sort(Global.rafflesByGames);
            OrderArrayListWithZeros.orderArrayListZeros(chipArrayList);
            filterRaffle();
        }
    }

    private void populateArrayListChips(ArrayList<Chip> chipArrayList){
        fruit.clear();
        lotto.clear();
        roulette.clear();
        for (Chip chip : chipArrayList) {
            chip.setActive(false);
                switch (chip.codeName) {
                    case Global.KEY_FA:
                        fruit.add(chip);
                        break;
                    case Global.KEY_LA:
                        lotto.add(chip);
                        break;
                    case Global.KEY_RA:
                        roulette.add(chip);
                        break;
                }
        }
    }

    private void populateArrayChips(ArrayList<Chip> chips) {
        for (Chip chip : chips){
            for (Map.Entry<String, Game> games : Global.currentGames.entrySet()) {
                Game game = games.getValue();
                LinkedHashMap<Integer, Raffle> raffles = game.getRaffles();
                for (Map.Entry<Integer, Raffle> ra : raffles.entrySet()) {
                    Raffle raffle = ra.getValue();
                    if (raffle.isSelected()) {
                        if (chip.gameId == raffle.getGameId()){
                            chip.setActive(true);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onItemDelete(int amount) {
        deletePlay();
    }

    private class ApplySynchronization extends AsyncTask<String, Integer, String> {
        String response;
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpRequest request = HttpRequest.post(Global.URL_SYNCRO_MOBILE).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .header(Global.KEY_TOKEN_FB, Global.refreshedToken)
                        .header(Global.KEY_TIMESTAMP,Global.timestamp)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = request.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "No se pueden actualizar los sorteos", Toast.LENGTH_SHORT);
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            Response response;
            progressBar.setVisibility(View.GONE);
            if (!s.isEmpty()) {
                try {
                    response = new Gson().fromJson(s, Response.class);
                    if (response.error.isEmpty()) {
                        LoginMessage message = gson.fromJson(response.message, LoginMessage.class);
                        Global.cash = String.valueOf(response.cash);
                        cash = response.cash;
                        etBalance.setText(Format.getCashFormat(cash));
                        saveIncomingCashPlayer(cash);
                        if (message.synchronization) {
                            SharedPreferenceConstants.saveTimestamp(getActivity(),Global.timestamp);
                            SharedPreferenceConstants.saveData(getActivity(),s);
                            Global.timestamp = response.timestamp;
                            Global.producerRating = message.producerRating;
                            Global.producerName = message.producerName;
                            Global.currentGames = message.games;
                            Global.producerId = message.producerId;
                            Global.userLogin = message.userlogin;
                            Global.features = message.producerFeatures;
                            Global.userName = message.username;
                            getData();
                        }
                    } else {
                        Global.Toaster.get().showToast(getContext(), response.message, Toast.LENGTH_SHORT);
                        if (response.message.equals("Problema de autorizacion") && Global.activityVisible) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Error al actualizar")
                                    .setContentText("Su sesion se encuentra activa en otro dispositivo")
                                    .setConfirmText("Cerrar Aplicacion")
                                    .showCancelButton(true)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.cancel();
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            LoginState.setLoginState(getActivity(), false);
                                            getActivity().finish();
                                        }
                                    })
                                    .show();
                        }
                    }
                } catch (HttpRequest.HttpRequestException ex) {
                    progressBar.setVisibility(View.GONE);
                    Global.Toaster.get().showToast(getActivity(), "No se pueden actualizar los sorteos", Toast.LENGTH_SHORT);
                }
            } else {
                progressBar.setVisibility(View.GONE);
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            }
        }
    }

    private class ApplySell extends AsyncTask<String, Integer, String> {
        AlertDialog.Builder builder = new AlertDialog.Builder(PrimaryFragment.this.getActivity());
        boolean flagResponse = false;
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            String jsonData;
            Sell sell = new Sell();
            sell.userId = Global.userId;
            sell.xSerial = new Date().getTime() + "";
            sell.sellItems = sellItems;
            jsonData = gson.toJson(sell);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(jsonData)
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getContext(), "Falla de red", Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (!s.isEmpty()) {
                Response response = gson.fromJson(s, Response.class);
                try {
                    if (response.error.equals("")) {
                        Sell sellResponse = gson.fromJson(response.message, Sell.class);
                        ArrayList<SellItem> se = new ArrayList<>();
                        cash = response.cash;
                        etBalance.setText(Format.getCashFormat(cash));
                        Global.cash = String.valueOf(cash);
                        saveIncomingCashPlayer(cash);
                        se.addAll(sellResponse.sellItems);
                        ListConfirmAdapter listConfirmAdapter = new ListConfirmAdapter(getActivity(), se, false);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View ViewAlerta = inflater.inflate(R.layout.list_view_general, null);
                        ListView xGrilla = (ListView) ViewAlerta.findViewById(R.id.listView1);
                        TextView xTitulo = (TextView) ViewAlerta.findViewById(R.id.lblTit);
                        String value = "Ticket: <b>" + sellResponse.xSerial + "&#160; &#160; &#160; &#160;" +
                                "<font color=\"#008000\">" + SellItem.getCountSellItem(se, true) + "</font>/" +
                                "<font color=\"red\">" + SellItem.getCountSellItem(se, false) + "</font></b>";
                        xTitulo.setText(Html.fromHtml(value));
                        xGrilla.setAdapter(listConfirmAdapter);
                        builder.setNegativeButton("Cerrar", null);
                        builder.setView(ViewAlerta);
                        builder.setCancelable(false);
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        flagResponse = true;
                        Global.Toaster.get().showToast(getActivity(), "Ticket Procesado", Toast.LENGTH_SHORT);
                    } else {
                        Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);//showToast("No se pudo completar la venta");
                    }
                } catch (HttpRequest.HttpRequestException e) {
                    Global.Toaster.get().showToast(getActivity(), "falla de red", Toast.LENGTH_SHORT);//showToast("Falla1 de red");
                }
            } else {
                Global.Toaster.get().showToast(getActivity(), "No se pudo completar la compra", Toast.LENGTH_SHORT);//showToast("No se pudo completar la venta");
            }
            if (flagResponse) {
                PrimaryFragment.this.cancel();
            }
        }
    }
}
