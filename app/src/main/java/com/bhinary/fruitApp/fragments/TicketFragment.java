package com.bhinary.fruitApp.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.BooleanResponse;
import com.bhinary.fruitApp.models.Details;
import com.bhinary.fruitApp.models.MyTickets;
import com.bhinary.fruitApp.models.SimpleDetail;
import com.bhinary.fruitApp.models.Ticket;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.adapters.TicketListAdapter;
import com.bhinary.fruitApp.adapters.TicketListDetailAdapter;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TicketFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private HashMap<String, Details> allTickets2 = new HashMap<>();
    private EditText etTotalAmount, etAward;
    private ProgressBar progressBar;
    private String dateTicket;
    private ArrayList<Ticket> tickets = new ArrayList<>();
    private TicketListAdapter ticketListAdapter;
    private ArrayList<SimpleDetail> simpleDetails = new ArrayList<>();
    private TicketListDetailAdapter ticketListDetailAdapter;
    private ListView lvTicket;
    private ImageButton btnCancel, btnCopy;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageButton btnSearchDate;

    public TicketFragment() {
        // Required empty public constructor
    }

    public static TicketFragment newInstance(String param1, String param2) {
        TicketFragment fragment = new TicketFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = (inflater.inflate(R.layout.fragment_ticket, container, false));
        lvTicket = (ListView) rootView.findViewById(R.id.lv_repeat_raffles);
        final ListView lvDetails = (ListView) rootView.findViewById(R.id.lv_detail);
        etTotalAmount = (EditText) rootView.findViewById(R.id.et_amount);
        etTotalAmount.setHorizontallyScrolling(false);
        etTotalAmount.setGravity(Gravity.CENTER);
        etAward = (EditText) rootView.findViewById(R.id.et_award);
        etAward.setHorizontallyScrolling(false);
        etAward.setGravity(Gravity.CENTER);
        EditText etPrice = (EditText) rootView.findViewById(R.id.et_award);
        etPrice.setHorizontallyScrolling(false);
        etPrice.setGravity(Gravity.CENTER);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.INVISIBLE);
        ticketListAdapter = new TicketListAdapter(getActivity(), tickets);
        lvTicket.setAdapter(ticketListAdapter);
        ticketListDetailAdapter = new TicketListDetailAdapter(getActivity(), simpleDetails);
        lvDetails.setAdapter(ticketListDetailAdapter);
        btnSearchDate = (ImageButton) rootView.findViewById(R.id.btn_search_ticket);
        ImageButton btnRefresh = (ImageButton) rootView.findViewById(R.id.btn_refresh_ticket);
        btnCancel = (ImageButton) rootView.findViewById(R.id.btn_dicard_ticket);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshContent();
            }
        });
        btnSearchDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSearchDate.setEnabled(false);
                alertDatePicker();
            }
        });
        lvTicket.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                populateDetailList(position);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ticketListAdapter.isEmpty()) {
                    if (tickets.get(Global.selectedPosition).details.getStatus() == 2) {
                        Toast.makeText(getContext(), "Ticket ya anulado, seleccione otro", Toast.LENGTH_SHORT).show();
                    } else {
                        btnCancel.setEnabled(false);
                        cancelTicket();
                    }
                } else {
                    Toast.makeText(getActivity(), "Debe Seleccionar un ticket", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent1();
            }
        });
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        OnFragmentInteractionListener mListener = null;
    }

    private void refreshContent1() {
        dateTicket = com.bhinary.fruitApp.common.Time.getCurrentDate();
        new GetTickets().execute(Global.URL_ALL_TICKET);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void copyTicketContentToPlayAgain() {
        if (!ticketListAdapter.isEmpty()) {
            Global.details = allTickets2.get(ticketListAdapter.getSerial(Global.selectedPosition));
            //startActivity(intent);
            btnCopy.setEnabled(true);
        } else {
            btnCopy.setEnabled(true);
            Toast.makeText(getActivity(), "Debe seleccionar un ticket", Toast.LENGTH_SHORT).show();
        }
    }

    public void refreshContent() {
        dateTicket = com.bhinary.fruitApp.common.Time.getCurrentDate();
        new GetTickets().execute(Global.URL_ALL_TICKET);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void cancelTicket() {
        if (!ticketListAdapter.isEmpty()) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Importante");
            dialogo1.setMessage("¿Desea anular este ticket?");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    new DiscardTicket().execute(Global.URL_DISCARD_TICKET);
                    btnCancel.setEnabled(true);
                }
            });
            dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    btnCancel.setEnabled(true);
                }
            });
            dialogo1.show();
        } else {
            btnCancel.setEnabled(true);
            Toast.makeText(getActivity(), "Debe seleccionar un ticket", Toast.LENGTH_SHORT).show();
        }
    }

    private void populateDetailList(int position) {
        Global.selectedPosition = position;
        ticketListAdapter.notifyDataSetChanged();
        lvTicket.smoothScrollToPosition(position);
        Details detail = allTickets2.get(ticketListAdapter.getSerial(position));
        etTotalAmount.setText(Format.getCashFormat(detail.getTotalAmount()));
        etAward.setText(Format.getCashFormat(detail.getAward()));
        simpleDetails.clear();
        for (SimpleDetail sd : detail.getItems()) {
            if (sd.getEffectiveSell() != 0) {
                simpleDetails.add(sd);
            }
        }
        ticketListDetailAdapter.notifyDataSetChanged();
    }

    private void checkTicketStatus(){
        int count = 0;
        for (Ticket ticket : tickets){
            Details details = ticket.getDetails();
            for (SimpleDetail simpleDetail : details.getItems()){
                if (simpleDetail.getProcessed() == 1 && simpleDetail.getStatus() == 0) {
                    count++;
                }
            }
            if (count == details.getItems().size()){
                details.setStatus(4);
                ticketListAdapter.notifyDataSetChanged();
                ticketListDetailAdapter.notifyDataSetChanged();
            }
            count = 0;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class GetTickets extends AsyncTask<String, String, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject ticketRequestJson = new JsonObject();
            ticketRequestJson.addProperty("userId", Global.userId);
            ticketRequestJson.addProperty("startDate", dateTicket);
            ticketRequestJson.addProperty("endDate", dateTicket);
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(ticketRequestJson.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
                tickets.clear();
                simpleDetails.clear();
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                tickets.clear();
                simpleDetails.clear();
                ticketListDetailAdapter.notifyDataSetChanged();
                ticketListAdapter.notifyDataSetChanged();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    MyTickets myTickets = gson.fromJson(response.message, MyTickets.class);
                    for (Map.Entry<String, Details> t : myTickets.tickets.entrySet()) {
                        tickets.add(new Ticket(t.getKey(), t.getValue()));
                        allTickets2.put(t.getKey(), t.getValue());
                    }
                    ticketListAdapter.notifyDataSetChanged();
                    if (ticketListAdapter.isEmpty()) {
                        Global.Toaster.get().showToast(getActivity(), "No se encontro tickets para esta fecha", Toast.LENGTH_SHORT);
                        etTotalAmount.setText("0");
                        etAward.setText("0");
                    }
                    populateDetailList(0);
                    checkTicketStatus();
                } else {
                    Global.Toaster.get().showToast(getActivity(), "No se encontro tickets para esta fecha", Toast.LENGTH_SHORT);
                    etTotalAmount.setText("0");
                    etAward.setText("0");
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (HttpRequest.HttpRequestException e) {
                progressBar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DiscardTicket extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject serialTicket = new JsonObject();
            serialTicket.addProperty("xSerial", ticketListAdapter.getSerial(Global.selectedPosition));
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .contentType("application/json")
                        .send(serialTicket.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    BooleanResponse booleanResponse = gson.fromJson(response.message, BooleanResponse.class);
                    if (booleanResponse.status) {
                        Toast.makeText(getActivity(), "Se elimino ticket", Toast.LENGTH_SHORT).show();
                        tickets.get(Global.selectedPosition).details.setStatus(2);
                        ticketListAdapter.notifyDataSetChanged();
                        Global.cash = String.valueOf(response.cash);
                        ((MainActivity) getActivity()).setCash();
                    } else {
                        Toast.makeText(getActivity(), "No se pudo eliminar ticket", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                System.out.println("error3 : " + e.getMessage());
            }
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = (DatePicker) view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -1);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(new Time(System.currentTimeMillis()).getTime());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        int month = myDatePicker.getMonth() + 1;
                        int day = myDatePicker.getDayOfMonth();
                        int year = myDatePicker.getYear();
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + day;
                        if (month < 10) {
                            formattedMonth = "0" + month;
                        }
                        if (day < 10) {
                            formattedDayOfMonth = "0" + day;
                        }
                        dateTicket = String.valueOf(formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                        new GetTickets().execute(Global.URL_ALL_TICKET);
                        progressBar.setVisibility(View.VISIBLE);
                        btnSearchDate.setEnabled(true);
                        dialog.cancel();
                    }
                }).show().setCancelable(false);
    }
}
