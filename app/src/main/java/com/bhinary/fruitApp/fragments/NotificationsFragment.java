package com.bhinary.fruitApp.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.bhinary.fruitApp.activities.MainActivity;
import com.bhinary.fruitApp.adapters.RecyclerNotificationListAdapter;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.MessageType;
import com.bhinary.fruitApp.models.Notification;
import com.bhinary.fruitApp.models.ResultNotification;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.util.SharedPreferenceConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by wpinango on 8/2/17.
 */

public class NotificationsFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ArrayList<Notification> notifications = new ArrayList<>();
    private ImageButton btnClean;
    private RecyclerNotificationListAdapter recyclerNotificationListAdapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifications, null);
        btnClean = (ImageButton) rootView.findViewById(R.id.btn_clean);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.listView);
        recyclerNotificationListAdapter = new RecyclerNotificationListAdapter(notifications);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(recyclerNotificationListAdapter);
        mRecyclerView.setHasFixedSize(true);
        NotificationManager notifManager= (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        setUpItemTouchHelper();
        setUpAnimationDecoratorHelper();
        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnClean.setEnabled(false);
                showDialogDelete();
            }
        });
        notifications.clear();
        try {
            ArrayList<Notification> a = new ArrayList<>(Arrays.asList(new Gson().fromJson(getSavedNotifications(getActivity()), Notification[].class)));
            notifications.addAll(a);
            recyclerNotificationListAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.getMessage();
        }
        return rootView;
    }

    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_clear_24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.activity_vertical_margin);
                initiated = true;
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                RecyclerNotificationListAdapter recyclerNotificationListAdapter = (RecyclerNotificationListAdapter) recyclerView.getAdapter();
                if (recyclerNotificationListAdapter.isUndoOn() && recyclerNotificationListAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                RecyclerNotificationListAdapter adapter = (RecyclerNotificationListAdapter) mRecyclerView.getAdapter();
                boolean undoOn = adapter.isUndoOn();
                if (undoOn) {
                    adapter.pendingRemoval(swipedPosition);
                } else {
                    adapter.remove(swipedPosition);
                    notifications.remove(swipedPosition);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;
                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }
                if (!initiated) {
                    init();
                }
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();
                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                xMark.draw(c);
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void setUpAnimationDecoratorHelper() {
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                if (!initiated) {
                    init();
                }
                if (parent.getItemAnimator().isRunning()) {
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;
                    int left = 0;
                    int right = parent.getWidth();
                    int top = 0;
                    int bottom = 0;
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            lastViewComingDown = child;
                        } else if (child.getTranslationY() > 0) {
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }
                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }
                    background.setBounds(left, top, right, bottom);
                    background.draw(c);
                }
                super.onDraw(c, parent, state);
            }
        });
    }

    private void showDialogDelete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea eliminar todas las notificaciones?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                notifications.clear();
                recyclerNotificationListAdapter.notifyDataSetChanged();
                saveNotifications(getActivity(), new Gson().toJson(notifications));
                btnClean.setEnabled(true);
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btnClean.setEnabled(true);
            }
        });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        for (Notification m : notifications) {
            m.setSelected(false);
        }
        recyclerNotificationListAdapter.notifyDataSetChanged();
        Global.isNotificationShow = true;
    }

    private void updateRecyclerAdapter(ArrayList<Notification> notifications) {
        try {
            recyclerNotificationListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Global.isNotificationShow = false;
    }

    public static void saveNotifications(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("savedNotifications1", value);
        edit.apply();
    }

    public static String getSavedNotifications(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
        return sharedPreferences.getString("savedNotifications1", "");
    }

    public void updateUIOnReceiverValue2(String value) {
        try {
            Map<String, String> data = new Gson().fromJson(value, Map.class);
            Notification notification = new Notification();
            if (data.containsKey("title") && !data.get("title").equals("")) {
                notification.setTitle(data.get("title"));
                notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                notifications.add(0, notification);
            }
            if (data.containsKey("result")) {
                Notification n = new Notification();
                ResultNotification resultNotification = new Gson().fromJson(data.get("result"), ResultNotification.class);
                n.setResult(resultNotification);
                n.setTitle("Ver repeticion de resultado de " + resultNotification.getRaffle() + " en la ruleta");
                n.setHour(Time.getNotificationDate(data.get("notificationDate")));
                notifications.add(0, n);
                ResultFragment.refresh = "refresh";
            }
            if (data.containsKey(MessageType.KEY_MESSAGE)) {
                String dataContains = data.get(MessageType.KEY_MESSAGE);
                if (dataContains.equals(MessageType.KEY_PAYMENT)) {
                    ((MainActivity) getActivity()).refreshPaymentFragment();
                }
                if (dataContains.equals(MessageType.KEY_RECHARGE)) {
                    RechargeFragment.refresh = "refresh";
                }
            }
            updateRecyclerAdapter(notifications);
        } catch (Exception e) {
            e.getMessage();
        }
    }

}