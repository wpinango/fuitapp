package com.bhinary.fruitApp.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.models.ResultList;
import com.bhinary.fruitApp.models.SimpleDetail;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.models.Response;
import com.bhinary.fruitApp.adapters.ResultListAdapter;
import com.bhinary.fruitApp.util.SharedPreferenceConstants;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ratan on 7/29/2015.
 */
public class ResultFragment extends Fragment {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private HashMap<String, ArrayList<SimpleDetail>> myResults = new HashMap<>();
    private ResultListAdapter resultListAdapter;
    private ProgressBar progressBar;
    private ArrayList<ResultList> resultLists = new ArrayList<>();
    private ImageButton refreshButton;
    private String dateResult;
    private ImageButton btnHist;
    public static String refresh = null;
    private String resultTag = "result";
    private String resultIdTag = "resultId";
    private String hashResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_result, null);
        refreshButton = (ImageButton)root.findViewById(R.id.btn_refresh_result);
        mSwipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.activity_main_swipe_refresh_layout);
        btnHist = (ImageButton) root.findViewById(R.id.btn_search_result);
        ListView lvResult = (ListView) root.findViewById(R.id.lv_result);
        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar_result);
        progressBar.setVisibility(View.INVISIBLE);
        resultListAdapter = new ResultListAdapter(getActivity(),resultLists);
        lvResult.setAdapter(resultListAdapter);
        if ((SharedPreferenceConstants.getGeneralData(getActivity(),resultTag) != "") && checkSavedDataDay() && resultLists.isEmpty()) {
            resultLists.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), resultTag), ResultList[].class)));
        }
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshButton.setEnabled(false);
                refreshContent();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        btnHist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnHist.setEnabled(false);
                alertDatePicker();
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (resultLists.isEmpty() || refresh != null) {
                refreshContent();
                refresh = null;
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void refreshContent() {
        dateResult = com.bhinary.fruitApp.common.Time.getCurrentDate();
        new GetResults().execute(Global.URL_GET_RESULT);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void alertDatePicker() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
        final DatePicker myDatePicker = (DatePicker) view.findViewById(R.id.myDatePicker);
        myDatePicker.setCalendarViewShown(true);
        myDatePicker.setSpinnersShown(false);
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -6);
        myDatePicker.setMinDate(c.getTime().getTime());
        myDatePicker.setMaxDate(System.currentTimeMillis());
        new AlertDialog.Builder(getActivity()).setView(view)
                .setTitle("Seleccione una fecha")
                .setPositiveButton("Ir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int month = myDatePicker.getMonth() + 1;
                        int day = myDatePicker.getDayOfMonth();
                        int year = myDatePicker.getYear();
                        String formattedMonth = "" + month;
                        String formattedDayOfMonth = "" + day;
                        if (month < 10) {
                            formattedMonth = "0" + month;
                        }
                        if (day < 10) {
                            formattedDayOfMonth = "0" + day;
                        }
                        dateResult = String.valueOf(formattedDayOfMonth + "-" + formattedMonth + "-" + String.valueOf(year).substring(2));
                        new GetResults().execute(Global.URL_GET_RESULT);
                        dialog.cancel();
                        btnHist.setEnabled(true);
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show().setCancelable(false);
    }

    private boolean checkSavedDataDay() {
        try {
            ArrayList<ResultList> r = new ArrayList<>();
            r.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), resultTag), ResultList[].class)));
            int day = Integer.valueOf(r.get(0).getDate().split("-")[0]);
            System.out.println("valores : " + day);
            if (day == Time.getCurrentDay()) {
                return true;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    public class GetResults extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            JsonObject dateToConsult = new JsonObject();
            dateToConsult.addProperty("startDate", dateResult);
            dateToConsult.addProperty("endDate", dateResult);
            try {
                if (!SharedPreferenceConstants.getGeneralData(getActivity(),resultIdTag).equals("")) {
                    hashResult = SharedPreferenceConstants.getGeneralData(getActivity(), resultIdTag);
                }
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .header(Global.KEY_HASH_RESULT, hashResult)
                        .contentType("application/json")
                        .send(dateToConsult.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = null;
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            refreshButton.setEnabled(true);
            try {
                myResults.clear();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    if (!hashResult.equals(response.getHashResult())) {
                        SharedPreferenceConstants.saveGeneralData(getActivity(),response.getHashResult(),resultIdTag);
                        populateList(response.message);
                        if (SharedPreferenceConstants.getGeneralData(getActivity(),resultTag) != "") {
                            resultLists.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), resultTag), ResultList[].class)));
                        }
                        SharedPreferenceConstants.saveGeneralData(getActivity(),new Gson().toJson(resultLists),resultTag);
                    } else if (!dateResult.equals(Time.getCurrentDate())){
                        populateList(response.message);
                    } else if (hashResult.equals(response.getHashResult()) && dateResult.equals(Time.getCurrentDate())) {
                        resultLists.clear();
                        resultLists.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), resultTag), ResultList[].class)));
                    }
                }
                resultListAdapter.notifyDataSetChanged();
                refreshButton.setEnabled(true);
                progressBar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void populateList(String message) {
        Type custom = new TypeToken<HashMap<String, ArrayList<SimpleDetail>>>() {
        }.getType();
        myResults = new Gson().fromJson(message, custom);
        resultLists.clear();
        for (Map.Entry<String, ArrayList<SimpleDetail>> si : myResults.entrySet()) {
            resultLists.add(new ResultList(si.getKey(), si.getValue(), dateResult));
        }
        Collections.sort(resultLists, new Comparator<ResultList>() {
            @Override
            public int compare(ResultList o1, ResultList o2) {
                return Integer.valueOf(o2.hour).compareTo(Integer.valueOf(o1.hour));
            }
        });
    }
}
