package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.RaffleLabel;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/16/17.
 */

public class RaffleListAdapter extends BaseAdapter {
    private Context Contexto;
    private ArrayList<RaffleLabel> raffleLabels;
    private LayoutInflater inflater;
    private CardView cardView;
    public RaffleListAdapter(Context cContexto, ArrayList<RaffleLabel> raffleLabels)   {
        this.Contexto = cContexto;
        this.raffleLabels = raffleLabels;
    }
    @Override
    public int getCount()
    {
        return raffleLabels.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent)    {
        TextView raffle;
        RaffleLabel raffleLabel = raffleLabels.get(position);
        if (itemView == null) {
            inflater = (LayoutInflater) Contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_list3, parent, false);
        }
        cardView = (CardView)itemView.findViewById(R.id.cv_ticket);
        raffle = (TextView) itemView.findViewById(R.id.tv_ticket);
        raffle.setText("Sorteo: " + Format.getRaffleHour(raffleLabel.getHour()));
        if (raffleLabel.isSelected()) {
            cardView.setCardBackgroundColor(Color.LTGRAY);
        } else {
            cardView.setCardBackgroundColor(Color.WHITE);
        }
        return itemView;
    }
}
