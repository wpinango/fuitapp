package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.Ticket;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/23/17.
 */

public class TicketListAdapter extends BaseAdapter {
    private Context Contexto;
    private ArrayList<Ticket> tickets;

    public TicketListAdapter(Context cContexto, ArrayList<Ticket> tickets) {
        this.Contexto = cContexto;
        this.tickets = tickets;
    }

    @Override
    public int getCount() {
        return tickets.size();
    }

    @Override
    public Object getItem(int position) {
        return tickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) Contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_ticket, parent, false);
        }
        CardView cardView = (CardView) itemView.findViewById(R.id.cv_ticket);
        TextView Game = (TextView) itemView.findViewById(R.id.tv_ticket);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_prize);
        try {
            Game.setText("Ticket: " + tickets.get(position).label + "\n" + tickets.get(position).details.getTicketDate()
            + " " + tickets.get(position).details.getTicketTime());
            cardView.setBackgroundColor(Color.WHITE);
            if (tickets.get(position).details.getStatus() == 0) {
                imageView.setImageResource(R.drawable.bill_black);
            }
            else if (tickets.get(position).details.getStatus() == 1) {
                imageView.setImageResource(R.drawable.prize);
            }
            else if (tickets.get(position).details.getStatus() == 2) {
                imageView.setImageResource(R.drawable.bill_cancel1);
            }
            else if (tickets.get(position).details.getStatus() == 3) {
                imageView.setImageResource(R.drawable.bill_check1);
            }
            else if (tickets.get(position).details.getStatus() == 4) {
                imageView.setImageResource(R.drawable.bill);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        if (Global.selectedPosition == position){
            cardView.setBackgroundResource(R.color.colorSelect);
        }else {
            cardView.setBackgroundColor(Color.WHITE);
        }
        return itemView;
    }

    public String getSerial(int position){
        return tickets.get(position).label;
    }

    public void setArrayListUpdate(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
        notifyDataSetChanged();
    }
}
