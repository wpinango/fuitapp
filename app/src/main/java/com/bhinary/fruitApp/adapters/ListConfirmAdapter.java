package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Time;
import com.bhinary.fruitApp.models.SellItem;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.common.Format;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/27/17.
 */

public class ListConfirmAdapter extends BaseAdapter{
    private ArrayList<SellItem> sellItems;
    private Context Contexto;
    private boolean isConfirmCalled;

    public ListConfirmAdapter (Context cContexto, ArrayList<SellItem> sellItems, boolean isConfirmCalled) {
        this.Contexto = cContexto;
        this.sellItems = sellItems;
        this.isConfirmCalled = isConfirmCalled;
    }
    @Override
    public int getCount() {
        return sellItems.size();
    }

    @Override
    public Object getItem(int position) {
        return sellItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        SellItem sellItem = sellItems.get(position);
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) Contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_list, parent, false);
        }
        TextView tvName = (TextView) itemView.findViewById(R.id.lblNombr);
        ImageView imageCheck = (ImageView) itemView.findViewById(R.id.imgFot);
        if (isConfirmCalled) {
            imageCheck.setImageResource(R.color.transparent);
        } else {
            if (sellItem.effectiveSell == sellItem.amount) {
                imageCheck.setImageResource(R.drawable.ok);
            } else if (sellItem.raffle <= Time.getCurrentTime() || sellItem.effectiveSell == 0) {
                imageCheck.setImageResource(R.drawable.no);
            }
        }
        tvName.setText(sellItem.game + " " + Format.getRaffleHour(sellItem.raffle) +
                " \n[ " + sellItem.chip + " ] " + sellItem.description + " x " + sellItem.amount);
        return itemView;
    }
}
