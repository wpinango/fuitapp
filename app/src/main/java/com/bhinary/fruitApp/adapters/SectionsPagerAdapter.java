package com.bhinary.fruitApp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhinary.fruitApp.fragments.NotificationsFragment;
import com.bhinary.fruitApp.fragments.PaymentFragment;
import com.bhinary.fruitApp.fragments.BalanceFragment;
import com.bhinary.fruitApp.fragments.PrimaryFragment;
import com.bhinary.fruitApp.fragments.RechargeFragment;
import com.bhinary.fruitApp.fragments.ResultFragment;
import com.bhinary.fruitApp.fragments.TicketFragment;

/**
 * Created by wpinango on 6/1/17.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private PrimaryFragment primaryFragment = new PrimaryFragment();
    private TicketFragment ticketFragment = new TicketFragment();
    private NotificationsFragment notificationsFragment = new NotificationsFragment();
    private BalanceFragment balanceFragment = new BalanceFragment();
    private ResultFragment resultFragment = new ResultFragment();
    private RechargeFragment rechargeFragment = new RechargeFragment();
    private PaymentFragment paymentFragment = new PaymentFragment();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return primaryFragment;
            case 1:
                return ticketFragment;
            case 2:
                return resultFragment;
            case 3:
                return balanceFragment;
            case 4:
                return rechargeFragment;
            case 5:
                return paymentFragment;
            case 6:
                return notificationsFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Jugar";
            case 1:
                return "Mis Jugadas";
            case 2:
                return "Resultados";
            case 3:
                return "Mi Balance";
            case 4:
                return "Recargar";
            case 5:
                return "Cobrar";
            case 6:
                return "Notificaciones";
        }
        return null;
    }

    public void setCash() {
        try {
            primaryFragment.setCash();
        }catch (Exception e ) {
            e.getMessage();
        }
    }

    public void requestResultList(){
        resultFragment.refreshContent();
    }

    public void requestPendingPayments(){
        paymentFragment.requestPendingTransaction();
    }

    public void refreshBalance(){
        balanceFragment.refreshContent();
    }

    public void refreshTickets(){
        ticketFragment.refreshContent();
    }

    public void refreshRafflesAndChips(){
        primaryFragment.refreshContent();
    }

    public void requestPendingRecharge(){
        rechargeFragment.requestPendingTransaction();
    }

    public void updateBalanceAmount(){
        balanceFragment.updateBalanceAmounts();
    }

    public void setRechargeCash(){
        rechargeFragment.setNewCash();
    }

    public void updateRechargeCash(){
        rechargeFragment.setNewCash();
    }

    public void updateNotificationReceive(String value){
        notificationsFragment.updateUIOnReceiverValue2(value);
    }
}
