package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.FourDigitBankAccountFormatWatcher;
import com.bhinary.fruitApp.models.Bank;
import com.bhinary.fruitApp.models.BankList;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/7/17.
 */

public class BankListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Bank> banks;
    private Context context;

    public BankListAdapter(Context context, ArrayList<Bank> banks){
        this.inflater = LayoutInflater.from(context);
        this.banks = banks;
        this.context = context;
    }

    @Override
    public int getCount() {
        return banks.size();
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_producer_bank, null);
        }
        CardView cvComment = (CardView) view.findViewById(R.id.cv_bank);
        TextView tvBankName = (TextView)view.findViewById(R.id.tv_name_producer);
        TextView tvAccountNumber = (TextView)view.findViewById(R.id.tv_comment_account_number);
        ImageView ivBank = (ImageView)view.findViewById(R.id.iv_bank);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvAccountNumber.setText(banks.get(position).getAccount());
            tvBankName.setText(banks.get(position).getName());
            cvComment.setCardBackgroundColor(Color.WHITE);
            ivBank.setImageResource(BankList.getBankImages(context, banks.get(position).getIcon()));
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
