package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.FourDigitBankAccountFormatWatcher;
import com.bhinary.fruitApp.models.AccountNumber;
import com.bhinary.fruitApp.models.BankList;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/22/17.
 */

public class AccountListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<AccountNumber> accountNumbers;
    private Context context;

    public AccountListAdapter(Context context, ArrayList<AccountNumber> accountNumbers){
        this.inflater = LayoutInflater.from(context);
        this.accountNumbers = accountNumbers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return accountNumbers.size();
    }

    @Override
    public Object getItem(int position) {
        return accountNumbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_producer_bank, null);
        }
        AccountNumber accountNumber = accountNumbers.get(position);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_bank);
        TextView tvBankName = (TextView)view.findViewById(R.id.tv_name_producer);
        TextView tvAccountNumber = (TextView)view.findViewById(R.id.tv_comment_account_number);
        TextView tvType = (TextView)view.findViewById(R.id.tv_account_type);
        ImageView ivBank = (ImageView)view.findViewById(R.id.iv_bank);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvAccountNumber.setText(accountNumber.getAccountNumber());
            tvBankName.setText(accountNumber.getBank());
            ivBank.setImageResource(BankList.getBankImages(context, accountNumber.getAccountNumber().substring(0,4)));
            tvType.setText(accountNumbers.get(position).getType());
            if (accountNumber.isSelected()) {
                cvComment.setBackgroundResource(R.color.colorSelect);
            } else {
                cvComment.setBackgroundResource(R.color.white);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
