package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.SimpleDetail;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/23/17.
 */

public class TicketListDetailAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SimpleDetail> simpleDetails;

    public TicketListDetailAdapter(Context cContexto, ArrayList<SimpleDetail> simpleDetails) {
        this.context = cContexto;
        this.simpleDetails = simpleDetails;
    }

    @Override
    public int getCount() {
        return simpleDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return simpleDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        if (itemView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_ticket_detail, parent, false);
        }
        SimpleDetail simpleDetail = simpleDetails.get(position);
        CardView cardView = (CardView) itemView.findViewById(R.id.cv_ticket);
        TextView Game = (TextView) itemView.findViewById(R.id.tv_ticket);
        ImageView imPlay = (ImageView) itemView.findViewById(R.id.img_play);
        try {
            String [] temp = simpleDetail.getLabel().split("] ");
            String[] chipDescription = temp[1].split(" ");
            simpleDetail.setChip(Format.replaceEspecialCharactersInChips(simpleDetail.getChip()));
            imPlay.setImageResource(context.getResources().getIdentifier(chipDescription[0].toLowerCase().trim() + "_" + simpleDetail.getCodeName().replace("-","_"),
                    "drawable",context.getPackageName()));
            Game.setText(simpleDetail.getLabel());
            if (simpleDetail.getStatus() == 0 && simpleDetail.getProcessed() == 0) {
                cardView.setBackgroundColor(Color.WHITE);
            } else if (simpleDetail.getStatus() == 1 && simpleDetail.getProcessed() == 1) {
                cardView.setBackgroundResource(R.color.colorAward);
            } else if (simpleDetail.getStatus() == 0 && simpleDetail.getProcessed() == 1) {
                cardView.setBackgroundResource(R.color.red_processed);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return itemView;
    }
}
