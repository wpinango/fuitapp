package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bhinary.fruitApp.models.Comment;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/28/17.
 */

public class CommentsListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Comment> comments;

    public CommentsListAdapter(Context context, ArrayList<Comment> comments){
        this.inflater = LayoutInflater.from(context);
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_comment, null);
        }
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rb_producer_comment);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_comment);
        TextView tvName = (TextView)view.findViewById(R.id.tv_name_comment);
        TextView tvComment = (TextView)view.findViewById(R.id.tv_comment);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_comment_date);
        try {
            tvComment.setText(comments.get(position).getComment());
            tvName.setText(comments.get(position).getPlayerName());
            tvDate.setText(comments.get(position).getDate());
            cvComment.setBackgroundResource(R.color.white);
            ratingBar.setRating(comments.get(position).getRating());
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
