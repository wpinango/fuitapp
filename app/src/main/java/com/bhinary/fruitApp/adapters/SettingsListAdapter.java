package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.bhinary.fruitApp.models.Settings;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.util.notification.NotificationsConfig;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/5/17.
 */

public class SettingsListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Settings> settings;
    private NotificationsConfig notificationsConfig = new NotificationsConfig();

    public SettingsListAdapter(Context context, ArrayList<Settings> settings){
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.settings = settings;
    }

    @Override
    public int getCount() {
        return settings.size();
    }

    @Override
    public Object getItem(int position) {
        return settings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        /*if (settings.get(position).getTAG().equals("TAG")){
            if (view == null) {
                view = inflater.inflate(R.layout.item_list_setting_sound, null);
            }
            TextView tvTitle = (TextView)view.findViewById(R.id.tv_title_setting);
            tvTitle.setText(settings.get(position).getTitle());
        } else {*/
            if (view == null) {
                view = inflater.inflate(R.layout.item_list_setting, null);
            }
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_title_setting);
            Switch s = (Switch) view.findViewById(R.id.switch1);
            try {
                tvTitle.setText(settings.get(position).getTitle());
                s.setChecked(settings.get(position).isStatus());
                s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        settings.get(position).setStatus(b);
                        notifyDataSetChanged();
                        setNewNotificationConfig();
                    }
                });
            } catch (Exception e) {
                e.getMessage();
            }
 //       }
        return view;
    }

    private void setNewNotificationConfig(){
        notificationsConfig.setNotificationConfig(context,new Gson().toJson(settings));
    }
}
