package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.ResultList;
import com.bhinary.fruitApp.models.SimpleDetail;
import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.common.Format;

import java.util.ArrayList;

/**
 * Created by wpinango on 6/22/17.
 */

public class ResultListAdapter extends BaseAdapter {
    private ArrayList<ResultList> resultLists;
    private LayoutInflater inflter;
    private Context context;

    public ResultListAdapter(Context context, ArrayList<ResultList> resultLists) {
        this.resultLists = resultLists;
        inflter = (LayoutInflater.from(context));
        this.context = context;
    }

    @Override
    public int getCount() {
        return resultLists.size();
    }

    @Override
    public Object getItem(int position) {
        return resultLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //if (view == null) {
        view = inflter.inflate(R.layout.item_list_result, null);
        //}
        TextView tvHour = (TextView) view.findViewById(R.id.tv_raffle_result);
        ImageView cvSafari = (ImageView) view.findViewById(R.id.cv_safari);
        ImageView cvTizana = (ImageView) view.findViewById(R.id.cv_tizana);
        ImageView cvLoto = (ImageView) view.findViewById(R.id.cv_loto);
        TextView tvSafari = (TextView) view.findViewById(R.id.tv_safari);
        TextView tvTizana = (TextView) view.findViewById(R.id.tv_tizana_result);
        TextView tvLoto = (TextView) view.findViewById(R.id.tv_loto_result);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date_result);
        TextView tvNumberResult = (TextView) view.findViewById(R.id.tv_number_result);
        CardView cardVSafari = (CardView) view.findViewById(R.id.cv_safari_result);
        CardView cardVTizana = (CardView) view.findViewById(R.id.cv_tizana_result);
        CardView cardVLoto = (CardView) view.findViewById(R.id.cv_loto_result);
        try {
            ResultList rs = resultLists.get(position);
            tvHour.setText(Format.getRaffleHour(Integer.parseInt(rs.hour)));
            tvDate.setText(rs.date);
            for (SimpleDetail simpleDetail : rs.simpleDetails) {
                tvNumberResult.setText("Sorteo: " + simpleDetail.getSerial());
                String[] temp = simpleDetail.getLabel().split(" - ");
                String chipDescription = temp[1];
                switch (simpleDetail.getCodeName()) {
                    case Global.KEY_RA:
                        tvLoto.setText(simpleDetail.getLabel());
                        cvLoto.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim() + "_" + simpleDetail.getCodeName().replace("-","_"),
                                "drawable",context.getPackageName()));
                        if (simpleDetail.getStatus() == 1) {
                            cardVLoto.setBackgroundResource(R.color.colorSelect);
                        } else {
                            cardVLoto.setBackgroundResource(R.color.white);
                        }
                        break;
                    case Global.KEY_FA:
                        tvTizana.setText(simpleDetail.getLabel());
                        simpleDetail.setChip(Format.replaceEspecialCharactersInChips(simpleDetail.getChip()));
                        cvTizana.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim() + "_" + simpleDetail.getCodeName().replace("-","_"),
                                "drawable",context.getPackageName()));
                        if (simpleDetail.getStatus() == 1) {
                            cardVTizana.setBackgroundResource(R.color.colorSelect);
                        } else {
                            cardVTizana.setBackgroundResource(R.color.white);
                        }
                        break;
                    case Global.KEY_LA:
                        tvSafari.setText(simpleDetail.getLabel());
                        cvSafari.setImageResource(context.getResources().getIdentifier(chipDescription.toLowerCase().trim() + "_" + simpleDetail.getCodeName().replace("-","_"),
                                "drawable",context.getPackageName()));
                        if (simpleDetail.getStatus() == 1) {
                            cardVSafari.setBackgroundResource(R.color.colorSelect);
                        } else {
                            cardVSafari.setBackgroundResource(R.color.white);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
