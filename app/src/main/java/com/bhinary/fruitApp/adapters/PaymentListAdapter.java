package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.RechargeItemPending;
import com.bhinary.fruitApp.R;

/**
 * Created by wpinango on 7/18/17.
 */

public class PaymentListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private RechargeItemPending rechargeItemPending;

    public PaymentListAdapter(Context context, RechargeItemPending rechargeItemPending){
        this.inflater = LayoutInflater.from(context);
        this.rechargeItemPending = rechargeItemPending;
    }

    @Override
    public int getCount() {
        return rechargeItemPending.getPending().size();
    }

    @Override
    public Object getItem(int position) {
        return rechargeItemPending.getPending().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_recharge, null);
        }
        CardView cvRecharge = (CardView) view.findViewById(R.id.cv_recharge);
        TextView tvRecharge = (TextView)view.findViewById(R.id.tv_recharge);
        ImageView ciRecharge = (ImageView)view.findViewById(R.id.ci_recharge);
        try {
            tvRecharge.setText(rechargeItemPending.getPending().get(position).getOperationDate() + "   "
                    + rechargeItemPending.getPending().get(position).getHour()  + "   "
                    + Format.getCashFormat(rechargeItemPending.getPending().get(position).getAmount()) + "\n"
                    + Format.getFormatValidationCode(rechargeItemPending.getPending().get(position).getRefNumber()));
            if (rechargeItemPending.getPending().get(position).getStatus() == 0 ) {
                ciRecharge.setImageResource(R.drawable.clock);
            }  else  if (rechargeItemPending.getPending().get(position).getStatus() == 2) {
                ciRecharge.setImageResource(R.drawable.no);
            } else  if (rechargeItemPending.getPending().get(position).getStatus() == 3) {
                ciRecharge.setImageResource(R.drawable.nul);
            }
            cvRecharge.setBackgroundResource(R.color.transparent);
            if (rechargeItemPending.getPending().get(position).isSelected()){
                view.setBackgroundResource(R.color.colorSelect);
            } else {
                view.setBackgroundResource(R.color.white);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
