package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.Bank;
import com.bhinary.fruitApp.models.BankList;
import com.bhinary.fruitApp.models.Producer;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 8/7/17.
 */

public class ProducerListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Producer> producers;
    private Context context;

    public ProducerListAdapter(Context context, ArrayList<Producer> producers) {
        this.inflater = LayoutInflater.from(context);
        this.producers = producers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return producers.size();
    }

    @Override
    public Object getItem(int position) {
        return producers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_producer, null);
        }
        final Producer producer = producers.get(position);
        TextView tvNameProducer = (TextView) view.findViewById(R.id.tv_name_producer);
        TextView tvRating = (TextView) view.findViewById(R.id.tv_rating_producer_signup);
        CardView cardView = (CardView) view.findViewById(R.id.cv_producer_signup);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.ly_bank);
        try {
            linearLayout.removeAllViews();
            for (int i = 0; i < producer.getBanks().size(); i++) {
                Bank bank = producer.getBanks().get(i);
                ImageView iv = new ImageView(context);
                iv.setLayoutParams(new LinearLayout.LayoutParams(90, 90));
                iv.setImageResource(BankList.getBankImages(context, bank.getIcon().trim()));
                linearLayout.addView(iv);
            }
            tvNameProducer.setText(producer.getName());
            tvRating.setText(Format.getFormattedStringOneDecimal(producer.getScore()));
            cardView.setCardBackgroundColor(Color.WHITE);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Producer.selectProducer(producers, position);
                    notifyDataSetChanged();
                }
            });
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Producer.selectProducer(producers, position);
                    notifyDataSetChanged();
                }
            });
            if (producer.isSelected()) {
                cardView.setCardBackgroundColor(Color.LTGRAY);
            } else {
                cardView.setCardBackgroundColor(Color.WHITE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
