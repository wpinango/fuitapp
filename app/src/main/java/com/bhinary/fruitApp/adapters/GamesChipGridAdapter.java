package com.bhinary.fruitApp.adapters;

/**
 * Created by willianpinango on 01/05/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.R;

public class GamesChipGridAdapter extends BaseAdapter {
    private Context context;
    private String[] Nombre;
    private int[] Foto;
    LayoutInflater inflater;
    private String codeName;

    public GamesChipGridAdapter(Context cContexto, String[] cNombre, String codeName)   {
        this.context = cContexto;
        this.Nombre = cNombre;
        this.codeName = codeName;
    }
    @Override
    public int getCount()
    {
        return Nombre.length;
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent)    {
        String[] split = Nombre[position].split("-");
        String number = split[0];
        String description = split[1];
        if (itemView ==  null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_grid_chips,parent,false);
        }
        TextView tvSafari = (TextView) itemView.findViewById(R.id.tv_chip_description);
        TextView tvChip = (TextView) itemView.findViewById(R.id.tv_chip);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView12);
        try {
            imageView.setImageResource(context.getResources().getIdentifier(Nombre[position].split("-")[1].toLowerCase() + "_" + codeName.replace("-","_"),
                    "drawable",context.getPackageName()));
            tvSafari.setText(description);
            tvChip.setText(number);
        } catch (Exception e) {
            e.getMessage();
        }
        return itemView;
    }
}
