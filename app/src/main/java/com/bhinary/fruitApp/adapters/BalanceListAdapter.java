package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.Balance;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/27/17.
 */

public class BalanceListAdapter extends BaseAdapter{
    private LayoutInflater inflter;
    private ArrayList<Balance> myBalances;
    private String operationType;

    public BalanceListAdapter(Context context, ArrayList<Balance> myBalances){
        inflter = LayoutInflater.from(context);
        this.myBalances = myBalances;
    }

    @Override
    public int getCount() {
        return myBalances.size();
    }

    @Override
    public Object getItem(int position) {
        return myBalances.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflter.inflate(R.layout.item_balance, null);
        }
        CardView cvRecharge = (CardView) view.findViewById(R.id.cv_balance);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_name_comment);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount_balance);
        TextView tvType = (TextView)view.findViewById(R.id.tv_type_balance);
        TextView tvReference = (TextView)view.findViewById(R.id.tv_reference_balance);
        try {
            switch (myBalances.get(position).type) {
                case 1:
                    operationType = "RB";
                    break;
                case 2:
                    operationType = "CT";
                    break;
                case 3:
                    operationType = "TJ";
                    break;
                case 4:
                    operationType = "TP";
                    break;
                case 5:
                    operationType = "TA";
                    break;
                case 6:
                    operationType = "SR";
                    break;
                case 7:
                    operationType = "ST";
                    break;
                case 8:
                    operationType = "RE";
                    break;
                case 9:
                    operationType = "CE";
                    break;
                case 10:
                    operationType = "3P";
                    break;
            }
            tvDate.setText(myBalances.get(position).operationDate);
            tvAmount.setText(Format.getCashFormat(myBalances.get(position).amount));
            tvType.setText(operationType);
            tvReference.setText(myBalances.get(position).reference);
            if (myBalances.get(position).reference == null) {
                tvReference.setText("0000" + (position));
            }
            cvRecharge.setBackgroundResource(R.color.white);
            if (position %2 == 0) {
                cvRecharge.setBackgroundResource(R.color.colorSelectBlueTransparen);
            }
        } catch (Exception e) {

        }
        return view;
    }
}
