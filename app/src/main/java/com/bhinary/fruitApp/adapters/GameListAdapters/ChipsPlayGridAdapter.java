package com.bhinary.fruitApp.adapters.GameListAdapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.models.Chip;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/7/17.
 */

public class ChipsPlayGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Chip> safariChips;
    private LayoutInflater inflater;

    public ChipsPlayGridAdapter(Context cContexto, ArrayList<Chip> safariChips) {
        this.context = cContexto;
        this.safariChips = safariChips;
    }

    @Override
    public int getCount() {
        return safariChips.size();
    }

    @Override
    public Object getItem(int position) {
        return safariChips.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rootView, ViewGroup parent) {
        if (rootView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rootView = inflater.inflate(R.layout.item_grid_chips, parent, false);
        }
        Chip chip = safariChips.get(position);
        TextView tvSafari = (TextView) rootView.findViewById(R.id.tv_chip_description);
        TextView tvChip = (TextView) rootView.findViewById(R.id.tv_chip);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView12);
        try {
            chip.description = Format.replaceEspecialCharactersInChips(chip.description);
            imageView.setImageResource(context.getResources().getIdentifier(chip.description.toLowerCase().trim() + "_" + chip.codeName.replace("-","_"),"drawable",context.getPackageName()));
            tvSafari.setText(chip.description);
            tvChip.setText(chip.chipLabel);
            if (chip.isSelected()) {
                imageView.setBackgroundColor(Color.LTGRAY);
            } else {
                imageView.setBackgroundColor(Color.WHITE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return rootView;
    }
}
