package com.bhinary.fruitApp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhinary.fruitApp.common.Format;
import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.SellItem;
import com.bhinary.fruitApp.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 7/19/17.
 */

public class PlaysListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SellItem> sellItems;
    private LayoutInflater inflater;
    private CardView cardView;
    private AdapterCallback callback;

    public interface AdapterCallback{
        void onItemDelete(int amount);
    }

    public PlaysListAdapter(Context cContexto, ArrayList<SellItem> sellItems, AdapterCallback callback) {
        this.sellItems = sellItems;
        this.context = cContexto;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return sellItems.size();
    }

    @Override
    public Object getItem(int position) {
        return sellItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        TextView tvPlay;
        ImageView imageView;
        SellItem sellItem = sellItems.get(position);
        if (itemView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_play, parent, false);
        }
        ImageButton btnDelete = (ImageButton)itemView.findViewById(R.id.imageButton11);
        cardView = (CardView) itemView.findViewById(R.id.cv_comment);
        tvPlay = (TextView) itemView.findViewById(R.id.tv_name_comment);
        imageView = (ImageView) itemView.findViewById(R.id.ci_comment);
        try {
            if (sellItem != null) {
                tvPlay.setText(sellItem.game + " " + Format.getRaffleHour(sellItem.raffle) +
                        " \n[ " + sellItem.chip + " ] " + sellItem.description + " x " + sellItem.amount);
                sellItem.chip = Format.replaceEspecialCharactersInChips(sellItem.chip);
                imageView.setImageResource(context.getResources().getIdentifier(sellItem.description.toLowerCase().trim() + "_" + sellItem.codeName.replace("-","_"),
                        "drawable",context.getPackageName()));
                if (Global.mycheked.get(position).equals(true)) {
                    cardView.setBackgroundResource(R.color.colorSelect);
                } else if (Global.mycheked.get(position).equals(false)) {
                    cardView.setBackgroundColor(Color.WHITE);
                }
            }
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleChecked(position);
                    callback.onItemDelete(sellItem.getAmount());
                    notifyDataSetChanged();
                }
            });
        }catch (Exception e) {
            e.getMessage();
        }
        return itemView;
    }

    public int getAmount (int position) {
        return sellItems.get(position).amount;
    }

    public void toggleChecked(int position) {
        if (Global.mycheked.get(position)) {
            Global.mycheked.put(position, false);
        } else {
            Global.mycheked.put(position, true);
        }
        notifyDataSetChanged();
    }

    public boolean isSelected() {
        for (int i = 0; i < Global.mycheked.size(); i++) {
            if (Global.mycheked.get(i)) {
                return true;
            }
        }
        return false;
    }

    public void updateChecked() {
        for (int i = 0; i < Global.mycheked.size(); i++) {
            if (Global.mycheked.get(i).equals(true)) {
                cardView.setBackgroundResource(R.color.colorSelect);
            } else if (Global.mycheked.get(i).equals(false)) {
                cardView.setBackgroundColor(Color.WHITE);
            }
        }
        notifyDataSetChanged();
    }

    public void updateDelete() {
        for (int i = 0; i < Global.mycheked.size(); i++) {
            Global.mycheked.put(i, false);
        }
        notifyDataSetChanged();
    }

    public String getString(int position) {
        String key = sellItems.get(position).game + " " + Format.getRaffleHour(sellItems.get(position).raffle) +
                " \n" + sellItems.get(position).chip + " " + sellItems.get(position).description;
        return key;
    }

}
