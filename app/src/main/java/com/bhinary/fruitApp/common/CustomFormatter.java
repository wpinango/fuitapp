package com.bhinary.fruitApp.common;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by wpinango on 8/7/17.
 */
public class CustomFormatter implements IValueFormatter, IAxisValueFormatter {

    private DecimalFormat mFormat;

    public CustomFormatter() {
        mFormat = new DecimalFormat("#.#");
    }

    // data
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return mFormat.format(Math.abs(value));
    }

    // YAxis
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(Math.abs(value));
    }
}
