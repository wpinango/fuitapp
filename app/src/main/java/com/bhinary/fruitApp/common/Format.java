package com.bhinary.fruitApp.common;

import android.util.Base64;

import com.bhinary.fruitApp.Global;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jcabrera on 05/06/17.
 */

public class Format {

    public static String getRaffleHour(int raffle) {
        int h = raffle / 60;
        int m = raffle % 60;

        String hl = String.valueOf(h);
        if (hl.length() == 1) {
            hl = "0" + hl;
        }
        String ml = String.valueOf(m);

        if (ml.length() == 1) {
            ml = "0" + ml;
        }
        Date d = new Date();
        d.setMinutes(m);
        d.setHours(h);
        DateFormat df = new SimpleDateFormat("hh:mm a");
        return df.format(d).toUpperCase();
    }

    public static String getMinutesAndSeconds(long time) {
        int h = (int) time / 60;
        int m = (int) time % 60;
        BigDecimal secondsPrecision = new BigDecimal((time - Math.floor(time)) * 100).setScale(2, RoundingMode.HALF_UP);
        int s = secondsPrecision.intValue();

        String hl = String.valueOf(h);
        if (hl.length() == 1) {
            hl = "0" + hl;
        }
        String ml = String.valueOf(m);

        if (ml.length() == 1) {
            ml = "0" + ml;
        }

        String sl = String.valueOf(s);
        if (sl.length() == 1) {
            sl = "0" + sl;
        }
        Date d = new Date();
        d.setMinutes(m);
        //d.setHours(h);
        d.setSeconds(s);
        DateFormat df = new SimpleDateFormat("mm:ss");
        return df.format(d).toUpperCase();
    }

    public static String getCashFormat(int cash) {
        String pattern = "#,###";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash);
    }

    public static String getFormattedStringOneDecimal(float value){
        return String.format("%.1f",value);
    }

    public static String getFormatValidationCode(String value) {
        return value.replaceFirst("(\\d{4})", "$1-");
    }

    public static String getFormatBankAccount(String value){
        return value.replaceFirst("(\\d{4}), (\\d{4})", "$1 ");
    }

    public static String getInvertString(String p) {
        String invertP = "";
        for (int x = p.length()-1; x >= 0; x--) {
            invertP = invertP + p.charAt(x);
        }
        return invertP;
    }

    private static String getEncryptStringP(String s) throws UnsupportedEncodingException {
        byte[] data = s.getBytes("UTF-8");
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    private static void desencrypStringP( String s){
        byte[] decodedBytes = Base64.decode(s, Base64.DEFAULT);
        String decodedString = new String(decodedBytes);
    }

    public static String getPEncrypted(String p) throws UnsupportedEncodingException {
        return getEncryptStringP(getInvertString(p));
    }

    public static String replaceEspecialCharactersInChips(String c) {
        String pineapple = "Pina";
        String mamon = "Mamon";
        String castana = "Castana";
        if (c.equals(Global.KEY_PINEAPPLE)) {
            c = pineapple;
        }
        if (c.equals("Mamón")) {
            c = mamon;
        }
        if (c.equals("Castaña")){
            c = castana;
        }
        return c;
    }
}
