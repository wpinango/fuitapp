package com.bhinary.fruitApp.common;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by wpinango on 8/4/17.
 */

public class Time {


    public static int toMins(String s) {
        String[] hourMin = s.split(":");
        int hour = Integer.parseInt(hourMin[0]);
        int mins = Integer.parseInt(hourMin[1]);
        int hoursInMins = hour * 60;
        return hoursInMins + mins;
    }

    public static int getCurrentYear() {
        SimpleDateFormat sfd = new SimpleDateFormat("yy");
        return Integer.valueOf(sfd.format(getCurrentCalendar().getTime()));
    }

    public static String getNotificationDate(String timeStamp) {
        long stamp = Long.valueOf(timeStamp);
        Date date = new Date(stamp);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy hh:mm a");
        return sdf.format(date).toUpperCase();
    }

    public static int getCompleteCurrentYear(){
        SimpleDateFormat sfd = new SimpleDateFormat("yyyy");
        return Integer.valueOf(sfd.format(getCurrentCalendar().getTime()));
    }

    public static Calendar getCurrentCalendar() {
        Calendar cal = Calendar.getInstance();
        return cal;
    }

    public static String getCurrentTimeInHours(){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return (sdf.format(getCurrentCalendar().getTime()));
    }

    public static String getCurrentDateAndTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy hh:mm a");
        return sdf.format(getCurrentCalendar().getTime()).toUpperCase();
    }

    public static int getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return toMins(sdf.format(getCurrentCalendar().getTime()));
    }

    public static String getCurrentDate(){
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yy");
        return sfd.format(getCurrentCalendar().getTime());
    }

    public static int getCurrentMonth() {
        SimpleDateFormat sfd = new SimpleDateFormat("MM");
        return Integer.valueOf(sfd.format(getCurrentCalendar().getTime())) - 1;
    }

    public static int getCurrentDay() {
        SimpleDateFormat sfd = new SimpleDateFormat("dd");
        return Integer.valueOf(sfd.format(getCurrentCalendar().getTime()));
    }

    public static long getCurrentTimeInSeconds() {
        return System.currentTimeMillis()/1000;
    }

    public static long getLastSyncronizationTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Timestamp", Context.MODE_PRIVATE);
        return sharedPreferences.getLong("time",0);
    }

    public static void setLastSyncronizationTime(Context context, long timestamp) {
        SharedPreferences preferences = context.getSharedPreferences("Timestamp", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong("time", timestamp);
        edit.apply();
    }

    public static boolean consultSyncronizationTime(Context context, long timestamp) {
        if ((getCurrentTimeInSeconds() - getLastSyncronizationTime(context)) >= 1800 ) {
            setLastSyncronizationTime(context,timestamp);
            return true;
        }
        return false;
    }
}
