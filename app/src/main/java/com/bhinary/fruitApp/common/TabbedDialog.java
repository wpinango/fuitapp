package com.bhinary.fruitApp.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhinary.fruitApp.R;
import com.bhinary.fruitApp.adapters.DialogFragmentPagerAdapter;
import com.bhinary.fruitApp.fragments.CustomFragment;
import com.bhinary.fruitApp.fragments.LotosuiteChipsFragment;

/**
 * Created by wpinango on 11/13/17.
 */

public class TabbedDialog extends DialogFragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.dialog_sample,container,false);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        //tabLayout = (TabLayout) rootview.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) rootview.findViewById(R.id.masterViewPager);
        DialogFragmentPagerAdapter adapter = new DialogFragmentPagerAdapter(getChildFragmentManager());
        //adapter.addFragment("Boy", CustomFragment.createInstance("John"));
        adapter.addFragment("tal", new LotosuiteChipsFragment());
        adapter.addFragment("Girl",CustomFragment.createInstance("Stacy"));
        //adapter.addFragment("Robot",CustomFragment.createInstance("Aeon"));
        viewPager.setAdapter(adapter);
        //tabLayout.setupWithViewPager(viewPager);
        return rootview;
    }
}
