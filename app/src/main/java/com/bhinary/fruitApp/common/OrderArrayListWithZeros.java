package com.bhinary.fruitApp.common;


import com.bhinary.fruitApp.Global;
import com.bhinary.fruitApp.models.Chip;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Created by wpinango on 9/7/17.
 */

public class OrderArrayListWithZeros implements Comparator<Chip>{

    public static void orderArrayListZeros(ArrayList<Chip> chips){
        ArrayList<Chip> arrayList = new ArrayList<>();
        Collections.sort(chips);
        for (Chip c: chips) {
            if (c.chipLabel.equals("0")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (c.chipLabel.equals("00")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (c.chipLabel.equals("000")){
                arrayList.add(c);
            }
        }
        for (Chip c: chips) {
            if (!c.chipLabel.equals("0") && !c.chipLabel.equals("00") && !c.chipLabel.equals("000")){
                arrayList.add(c);
            }
        }
        chips.clear();
        chips.addAll(arrayList);
    }

    public static String setLotteryImage(String codeName){
        switch (codeName){
            case Global.KEY_LA:
                return "lotto";
            case Global.KEY_FA:
                return "tufrutactiva";
            case Global.KEY_RA:
                return "ruleta";
        }
        return "";
    }

    @Override
    public int compare(Chip chip, Chip t1) {
        return 0;
    }

    @Override
    public Comparator<Chip> reversed() {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparing(Comparator<? super Chip> other) {
        return null;
    }

    @Override
    public <U> Comparator<Chip> thenComparing(Function<? super Chip, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<Chip> thenComparing(Function<? super Chip, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingInt(ToIntFunction<? super Chip> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingLong(ToLongFunction<? super Chip> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Chip> thenComparingDouble(ToDoubleFunction<? super Chip> keyExtractor) {
        return null;
    }
}
