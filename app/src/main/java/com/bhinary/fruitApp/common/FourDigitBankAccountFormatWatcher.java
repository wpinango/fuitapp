package com.bhinary.fruitApp.common;

import android.text.Editable;
import android.text.TextWatcher;

import com.bhinary.fruitApp.Global;

/**
 * Created by wpinango on 6/20/17.
 */

public class FourDigitBankAccountFormatWatcher implements TextWatcher {
    private boolean lock;

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (lock || s.length() > 20) {
            return;
        }
        lock = true;
        for (int i = 4; i < s.length(); i += 5) {
            if (s.toString().charAt(i) != '-') {
                s.insert(i, Global.KEY_VALUE_ACCOUNT);
            }
        }
        lock = false;
    }
}
