package com.bhinary.fruitApp.common;

import android.app.Activity;

import com.bhinary.fruitApp.R;

/**
 * Created by wpinango on 8/15/17.
 */

public class TransitionAnimation {
    public static void setInActivityTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void setOutActivityTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public static void setInDownActivityTrantition(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    public static void setInUpActiviTrantition(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }
}
