package com.bhinary.fruitApp.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wpinango on 8/28/17.
 */

public class LoginState {

    public static void setLoginState(Context context,boolean state){
        SharedPreferences preferences = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean("islogin", state);
        edit.apply();
    }

    public static boolean isLogin(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("islogin", false);
    }
}
